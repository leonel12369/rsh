package com.his.dao;

import java.util.List;

import com.his.model.MaeRol;
import com.his.model.MaeUsuario;



public interface UsuarioDao {

	public void save(MaeUsuario  usuario);
	
	public void saveRol(MaeRol rol);	
	
	public void deleteRol(int idUsuario);
	
	public void deleteUsuario(int idUsuario);
	
	public int findUsername(String username);
	
	public List<MaeRol> adminOrUser(String nombreRol);
	
	public List<MaeRol> adminAndUser();
	
	public List<String> listaStringRol(int id);
	
	public MaeUsuario findUsuario(int id);
	
	public List<MaeRol> findRolUsuario(int id);
	
	public void deleteRolEspecifico(String rol,int idUsuario);
	
	public MaeRol findRolUsuarioEspecifico(String rol, int idUsuario) ;
	
	
}
