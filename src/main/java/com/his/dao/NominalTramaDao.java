package com.his.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.his.model.NominalTramaNuevo;

public interface NominalTramaDao {

	public Page<NominalTramaNuevo> listar(int idEstablecimiento,String  anio,String mes,Pageable pageable);
	
}
