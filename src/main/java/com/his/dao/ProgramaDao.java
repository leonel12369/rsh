package com.his.dao;

import java.util.List;

import com.his.model.MaeEstrategiaSanitaria;

public interface ProgramaDao {
	
	public List<MaeEstrategiaSanitaria> listar();

}
