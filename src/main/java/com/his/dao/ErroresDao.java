package com.his.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.his.model.MaeErrores;
import com.his.model.ResErrores;

public interface ErroresDao {

	public Page<ResErrores> listarTodo(Pageable pageable);
	
	public Page<ResErrores> erroresFiltrados(Boolean estado, int tipoRegistrador,int idPrograma,int idEstablecimiento,String  anio,String mes,Pageable pageable);
	
	public List<ResErrores> listaErrores(String idCita);
	
	public Page<ResErrores> erroresFiltradosUser(Boolean estado,int tipoRegistrador,int idPrograma,String  anio,String mes,String dni,Pageable pageable);
	
	public Page<ResErrores> erroresFiltradosPersonal(Boolean estado,int idPrograma,String  anio,String mes,String dni,Pageable pageable);
	
	public Page<ResErrores> erroresFiltradosPaciente(Boolean estado,String  anio,String mes,String dni,Pageable pageable);
	
	
	//------------------------------------------- documentos--------------------------------------
	public List<ResErrores> listaErroresFiltrados(Boolean estado,int tipoRegistrador,int idPrograma,int idEstablecimiento,String  anio,String mes);
	
	public List<ResErrores> listaErroresFiltradosUser(Boolean estado,int tipoRegistrador,int idPrograma,String  anio,String mes,String dni);
	
	public List<ResErrores> listaErroresFiltradosPersonal(Boolean estado,int idPrograma,String  anio,String mes,String dni);
	
	public List<ResErrores> listaErroresFiltradosPaciente(Boolean estado,String  anio,String mes,String dni);
	
	//-------------------------------------------fin documentos--------------------------------------
	
	public int editar(ResErrores resErrores);
	
	public ResErrores buscar(int idResErrores);
}
