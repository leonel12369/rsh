package com.his.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.ProgramaDao;
import com.his.model.MaeEstrategiaSanitaria;

@Repository
public class ProgramaDaoImpl implements ProgramaDao{

private final Logger log= LoggerFactory.getLogger(ProgramaDaoImpl.class);

	
	@PersistenceContext
	private EntityManager em;


	@Override
	public List<MaeEstrategiaSanitaria> listar() {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeEstrategiaSanitaria",MaeEstrategiaSanitaria.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS AL MOMENTO DE LISTAR MAESTRATEGIA");
			log.info(""+e);
			return null;
		}
	}
	
	
}
