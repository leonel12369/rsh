package com.his.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.his.dao.UsuarioDao;
import com.his.model.MaeRol;
import com.his.model.MaeUsuario;


@Repository
public class UsuarioDaoImpl implements UsuarioDao{

	private final Logger log= LoggerFactory.getLogger(UsuarioDaoImpl.class);
	
	@Autowired
	private EntityManager em;
	
	@Override
	public void save(MaeUsuario usuario) {
		// TODO Auto-generated method stub
		try {
			if(usuario.getIdUsuario()>0) {
				em.merge(usuario);
			}
			else {
				em.persist(usuario);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL GUARDAR AL USUARIO");
			log.info(""+e);
		}
	}

	@Override
	public void saveRol(MaeRol rol) {
		// TODO Auto-generated method stub
		try {
			if(rol.getIdRol()>0) {
				em.merge(rol);
			}
			else {
				em.persist(rol);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL GUARDAR LOS ERRORES");
			log.info(""+e);
		}
	}

	@Override
	public int findUsername(String username) {
		// TODO Auto-generated method stub
		try {
			MaeUsuario usuario=(MaeUsuario) em.createQuery("from MaeUsuario where nombreUsuario =: username",MaeUsuario.class).setParameter("username", username).getSingleResult();
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
	}

	@Override
	public void deleteRol(int idUsuario) {
		// TODO Auto-generated method stub
		List<MaeRol> lista= findRolUsuario(idUsuario);
		for (MaeRol i: lista) {
			//System.out.println(i.getIdRol()+""+i.getUsuario().getIdUsuario());
			em.remove(i);
		}
	}

	@Override
	public void deleteUsuario(int idUsuario) {
		// TODO Auto-generated method stub
		MaeUsuario objeto= findUsuario(idUsuario);
		//System.out.println(objeto.getNombre());
		em.remove(objeto);
	}

	@Override
	public List<MaeRol> adminOrUser(String nombreRol) {
		// TODO Auto-generated method stub
		try {
			return em.createNativeQuery("select t1.* from mae_rol t1 inner join (select id_usuario from mae_rol group by id_usuario having COUNT(id_usuario)<2)t2 on t1.id_usuario=t2.id_usuario  where t1.nombre=:nombre_rol",MaeRol.class).setParameter("nombre_rol", nombreRol).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ADMIN O USER ");
			log.info(e+"");
			return null;
		}
	}

	@Override
	public List<MaeRol> adminAndUser() {
		// TODO Auto-generated method stub
		try {
			return em.createNativeQuery("select id_rol,nombre,id_usuario from( select row_number() over(PARTITION BY t1.id_usuario order by t1.id_usuario) as nro, t1.* from mae_rol t1 inner join (select id_usuario from mae_rol group by id_usuario having COUNT(id_usuario)>1)t2 on t1.id_usuario=t2.id_usuario)X where nro=1",MaeRol.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ADMIN AND USER ");
			log.info(e+"");
			return null;
		}
		
	}

	@Override
	public List<String> listaStringRol(int id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("select nombre from MaeRol where id_usuario =: id").setParameter("id", id).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LISTA STRING ROL");
			log.info(e+"");
			return null;
		}
	}

	@Override
	public MaeUsuario findUsuario(int id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeUsuario where idUsuario =: id",MaeUsuario.class).setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ENCONTRAR AL USUARIO");
			log.info(e+"");
			return null;
		}
	}

	@Override
	public List<MaeRol> findRolUsuario(int id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeRol where id_usuario =: id",MaeRol.class).setParameter("id", id).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ENCONTRAR AL ENCONTRAR ROL");
			log.info(e+"");
			return null;		
		}
	}

	@Override
	public void deleteRolEspecifico(String rol, int idUsuario) {
		// TODO Auto-generated method stub
		try {
			MaeRol objeto=findRolUsuarioEspecifico( rol,  idUsuario);
			em.remove(objeto);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ELIMINAR EL ROL ESPEFICICO");
			log.info(e+"");
			
		}
	}

	@Override
	public MaeRol findRolUsuarioEspecifico(String rol, int idUsuario) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeRol where nombre =: rol and id_usuario=: idUsuario",MaeRol.class).setParameter("rol", rol).setParameter("idUsuario", idUsuario).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ELIMINAR EL ROL ESPEFICICO");
			log.info(e+"");
			return null;
		}
	}

	

}
