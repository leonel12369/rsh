package com.his.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.GraficoDao;
import com.his.model.Grafico;


@Repository
public class GraficoDaoImpl implements GraficoDao{

	private final Logger log= LoggerFactory.getLogger(GraficoDaoImpl.class);

	
	@PersistenceContext
	private EntityManager em;
	
	
	public void queryWithAuthorBookCountHibernateMapping() {
		  Session session = (Session)this.em.getDelegate();//hibernate
	}
	

	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Grafico> mesError() {
		// TODO Auto-generated method stub
		try {
			List<Grafico> lista_mes_error = ((Session)this.em.getDelegate()).createSQLQuery("select e.mes AS nombre, count(e.id) as  cantidad  from res_errores e group by e.mes order by e.mes;")
					.addScalar("nombre").addScalar("cantidad",StandardBasicTypes.INTEGER)
			        .setResultTransformer(new AliasToBeanResultTransformer(Grafico.class))
			        .list();

			return lista_mes_error;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS SACAR LOS MES - ERROR");
			log.info(""+e);
			return null;
		}
	}


	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Grafico> erroresComunes() {
		// TODO Auto-generated method stub
		try {
			List<Grafico> lista_error_comun = ((Session)this.em.getDelegate()).createSQLQuery("select me.descripcionCorta AS nombre, count(e.id) as  cantidad  from res_errores e inner join mae_errores me on me.idError=e.idError group by me.descripcionCorta ORDER BY count(e.id) desc OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY")
					.addScalar("nombre").addScalar("cantidad",StandardBasicTypes.INTEGER)
			        .setResultTransformer(new AliasToBeanResultTransformer(Grafico.class))
			        .list();

			return lista_error_comun;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS SACAR ERRORES COMUNES");
			log.info(""+e);
			return null;
		}
	}



	@Override
	public List<Object[]> errorMicroRed(String anio, int mes,  int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		try {
			String consulta=" select count(e.id),mhe.Codigo_MicroRed,mhe.MicroRed from res_errores e ";
			String joinCita=" inner join mae_citas mc on mc.Id_Cita = e.idCita ";
			String joinEstablecimiento=" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento ";
			String joinErrores= " inner join mae_errores me on me.idError=e.idError ";
			String where=" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' ";
			if(!anio.equals("0")) {
				where =where+ " and e.año='"+anio+"'";
			}
			if(mes!=0) {
				where =where+ " and e.mes between "+mes+" and "+fmes+" ";
			}
			if(idEstrategia!=0) {
				where =where+ " and me.idEstrategia="+idEstrategia;
			}
			
			String groupBy=" group by mhe.Codigo_MicroRed,mhe.MicroRed ORDER BY mhe.Codigo_MicroRed ";
			String sql=consulta+joinCita+joinEstablecimiento+joinErrores+where+groupBy;
			System.out.println(sql);
			List<Object[]> listaErrorMicroRed= new ArrayList<Object[]>();
			Query resultados_consulta= em.createNativeQuery(sql);
			listaErrorMicroRed = resultados_consulta.getResultList();
			/*for (Object[] a : authors) {
			    System.out.println("Author "
			            + a[0]
			            + " "
			            + a[1]);
			}
			System.out.println(authors);*/
						
			return listaErrorMicroRed;
		} catch (Exception e) {
			log.info("ERROR EN LA BASE DE DATOS SACAR CANTIDAD DE ERRORES POR MICRORED");
			log.info(""+e);
			return null;
		}
	}


	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Grafico> errorEstablecimiento(String codMicroRed,String anio, int mes,int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		try {
			String consulta=" select mhe.Nombre_Establecimiento as nombre, count(e.id) as cantidad from res_errores e ";
			String joinCita=" inner join mae_citas mc on mc.Id_Cita = e.idCita ";
			String joinEstablecimiento=" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento ";
			String joinErrores= " inner join mae_errores me on me.idError=e.idError ";
			String where=" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01'and Codigo_MicroRed =:codMicroRed ";
			if(!anio.equals("0")) {
				where =where+ " and e.año='"+anio+"'";
			}
			if(mes!=0) {
				where =where+ " and e.mes between "+mes+" "+"and "+fmes+" ";
			}
			if(idEstrategia!=0) {
				where =where+ " and me.idEstrategia="+idEstrategia;
			}
			
			String groupBy=" group by mhe.Id_Establecimiento,mhe.Nombre_Establecimiento ORDER BY count(e.id) desc ";
			String sql=consulta+joinCita+joinEstablecimiento+joinErrores+where+groupBy;
			
			System.out.println(sql);
			List<Grafico> listaErrorEstablecimiento = ((Session)this.em.getDelegate()).createSQLQuery(sql)
					.addScalar("nombre").addScalar("cantidad",StandardBasicTypes.INTEGER)
					.setResultTransformer(new AliasToBeanResultTransformer(Grafico.class))
					.setParameter("codMicroRed", codMicroRed).list();

			return listaErrorEstablecimiento;
		} catch (Exception e) {
			log.info("ERROR EN LA BASE DE DATOS SACAR CANTIDAD DE ERRORES POR ESTABLECIMIENTO");
			log.info(""+e);
			return null;
		}
	}


	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Grafico> errorMayorEstablecimiento() {
		// TODO Auto-generated method stub
		try{
			List<Grafico> listaErrorMayorEstablecimiento = ((Session)this.em.getDelegate()).createSQLQuery("select mhe.Nombre_Establecimiento as nombre, count(e.id) as cantidad from res_errores e "
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' "
					+ " group by mhe.Nombre_Establecimiento ORDER BY count(e.id) desc "
					+ " OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY ")
					.addScalar("nombre").addScalar("cantidad",StandardBasicTypes.INTEGER)
					.setResultTransformer(new AliasToBeanResultTransformer(Grafico.class)).list();

			return listaErrorMayorEstablecimiento;
		} catch (Exception e) {
			log.info("ERROR EN LA BASE DE DATOS OBTENER MAYOR CANTIDAD DE ERRORES POR ESTABLEICMIENTO");
			log.info(""+e);
			return null;
		}
	}



	@Override
	public List<Object[]> cantidadErrorMicrored(String codMicroRed,String anio, int mes, int fmes,int idEstrategia) {
		// TODO Auto-generated method stub
		
		try {
			String consulta=" select e.mes as nombre, count(e.id) as  cantidad  from res_errores e " ;
			String joinCita=" inner join mae_citas mc on mc.Id_Cita = e.idCita ";
			String joinEstablecimiento=" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento ";
			String joinErrores= " inner join mae_errores me on me.idError=e.idError ";
			String where=" where mhe.Codigo_Disa=5 and mhe.Codigo_Red='01' and Codigo_MicroRed =:codMicroRed ";
			if(!anio.equals("0")) {
				where =where+ " and e.año='"+anio+"'";
			}
			if(mes!=0) {
				where =where+ " and e.mes between "+mes+" "+"and "+fmes+" ";
			}
			if(idEstrategia!=0) {
				where =where+ " and me.idEstrategia="+idEstrategia;
			}
			
			String groupBy=" group by e.mes order by e.mes asc ";
			String sql=consulta+joinCita+joinEstablecimiento+joinErrores+where+groupBy;
			System.out.println(sql);
			List<Object[]> listaCantidadErrorMicrored= new ArrayList<Object[]>();
			Query resultados_consulta= em.createNativeQuery(sql)
					.setParameter("codMicroRed", codMicroRed);
			
			listaCantidadErrorMicrored = resultados_consulta.getResultList();
			
			return listaCantidadErrorMicrored;
			
		} catch (Exception e) {
			log.info("ERROR EN LA BASE DE DATOS SACAR CANTIDAD DE ERRORES POR MICRORED");
			log.info(""+e);
			return null;
		}
	}



	@Override
	public List<Object[]> errorComunMicrored(String codMicroRed,String anio, int mes,int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		try {
			
			String consulta=" select me.descripcionCorta as nombre, count(e.id) as  cantidad  from res_errores e " ;
			String joinCita=" inner join mae_citas mc on mc.Id_Cita = e.idCita ";
			String joinEstablecimiento=" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento ";
			String joinErrores=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where mhe.Codigo_Disa=5 and mhe.Codigo_Red='01' and Codigo_MicroRed =:codMicroRed  ";
			if(!anio.equals("0")) {
				where =where+ " and e.año='"+anio+"'";
			}
			if(mes!=0) {
				where =where+ " and e.mes between "+mes+" "+"and "+fmes+" ";
			}
			if(idEstrategia!=0) {
				where =where+ " and me.idEstrategia="+idEstrategia;
			}
			
			String groupBy=" group by me.descripcionCorta ";
			String sql=consulta+joinCita+joinEstablecimiento+joinErrores+where+groupBy;
			System.out.println(sql);
			
			List<Object[]> listaErrorComunMicrored= new ArrayList<Object[]>();
			Query resultados_consulta= em.createNativeQuery(sql)
					.setParameter("codMicroRed", codMicroRed).setMaxResults(10);
			listaErrorComunMicrored = resultados_consulta.getResultList();
		
			return listaErrorComunMicrored;
		} catch (Exception e) {
			log.info("ERROR EN LA BASE DE DATOS SACAR CANTIDAD DE ERRORES POR MICRORED");
			log.info(""+e);
			return null;
		}
	}



	@Override
	public List<Object[]> avanceGraficoDetalleEstablecimiento(String codMicroRed,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			List<Object[]> lista= new ArrayList<Object[]>();
			String sql=" select count(e.id) as cantidad,mhe.Id_Establecimiento, mhe.Nombre_Establecimiento from res_errores e "
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and mhe.Codigo_MicroRed=:codMicroRed and e.año=:anio and e.mes between :mesI and :mesF and estado='1' "
					+ " group by mhe.Id_Establecimiento,mhe.Nombre_Establecimiento ORDER BY count(e.id) ";
			Query resultados_consulta= em.createNativeQuery(sql).setParameter("codMicroRed", codMicroRed)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF);
			lista = resultados_consulta.getResultList();
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS AVANCE GRAFICO DETALLE ESTABLEICMIENTO");
			log.info(""+e);
			return null;
		}
	}
	
	
	@Override
	public int cantidadAvance_RestanteMicrored(String codMicroRed, Boolean estado,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			int cantidad=0;
			String sql=" select count(e.id) as cantidad from res_errores e "//,mhe.Id_Establecimiento, mhe.Nombre_Establecimiento
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and mhe.Codigo_MicroRed=:codMicroRed and e.año=:anio and e.mes between :mesI and :mesF and estado=:estado "
					+ " group by mhe.Codigo_MicroRed,mhe.MicroRed ";
			
			String resultados_consulta= em.createNativeQuery(sql)
					.setParameter("codMicroRed", codMicroRed)
					.setParameter("estado", estado)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.getSingleResult().toString();

			System.out.println("resultados_consulta: "+resultados_consulta);
			cantidad= Integer.parseInt(resultados_consulta);
	
			return cantidad;
		} catch (Exception e) {
			// TODO: handle exception
			//log.info("ERROR EN LA BASE DE DATOS CANTIDAD AVANCE Y RESTANTE DETALLE MICRORED");
			//log.info(""+e);
			return 0;
		}
	}



	@Override
	public int cantidadAvance_RestanteEstablecimiento(String establecimiento, Boolean estado,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			int cantidad=0;
			String sql=" select count(e.id) as cantidad from res_errores e "//,mhe.Id_Establecimiento, mhe.Nombre_Establecimiento
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5 and mhe.Id_Establecimiento=:establecimiento and e.año=:anio and e.mes between :mesI and :mesF and estado=:estado "
					+ " group by mhe.Id_Establecimiento,mhe.Nombre_Establecimiento ";
			
			String resultados_consulta= em.createNativeQuery(sql)
					.setParameter("establecimiento", establecimiento)
					.setParameter("estado", estado)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.getSingleResult().toString();

			cantidad= Integer.parseInt(resultados_consulta);
	
			return cantidad;
		} catch (Exception e) {
			// TODO: handle exception
			//log.info("ERROR EN LA BASE DE DATOS CANTIDAD AVANCE Y RESTANTE DETALLE ESTABLECIMIENTO");
			//log.info(""+e);
			return 0;
		}
	}



	@Override
	public int cantidadAvance_RestanteMicrored2(String codMicroRed, Boolean estado, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			int cantidad=0;
			String sql=" select count(e.id) as cantidad from res_errores e "//,mhe.Id_Establecimiento, mhe.Nombre_Establecimiento
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5 and mhe.Codigo_Red=:codMicroRed and e.año=:anio and e.mes between :mesI and :mesF and estado=:estado "
					+ " group by mhe.Codigo_Red,mhe.Red  ";
			
			String resultados_consulta= em.createNativeQuery(sql)
					.setParameter("codMicroRed", codMicroRed)
					.setParameter("estado", estado)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.getSingleResult().toString();

			cantidad= Integer.parseInt(resultados_consulta);
	
			return cantidad;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN cantidadAvance_RestanteMicrored2");
			log.info(""+e);
			return 0;
		}
	}



	@Override
	public int cantidadAvance_RestanteRed(String red, Boolean estado, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			int cantidad=0;
			String sql=" select count(e.id) as cantidad from res_errores e "//,mhe.Id_Establecimiento, mhe.Nombre_Establecimiento
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5  and e.año=:anio and e.mes between :mesI and :mesF and estado=:estado "
					+ " group by mhe.Codigo_Disa,mhe.Disa ";
			
			String resultados_consulta= em.createNativeQuery(sql)
					.setParameter("estado", estado)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.getSingleResult().toString();

			cantidad= Integer.parseInt(resultados_consulta);
	
			return cantidad;
		} catch (Exception e) {
			// TODO: handle exception
			//log.info("ERROR EN LA BASE DE DATOS CANTIDAD AVANCE Y RESTANTE DETALLE ESTABLECIMIENTO");
			//log.info(""+e);
			return 0;
		}
	}



	@Override
	public List<Object[]> avanceGraficoDetalleMicroRed(String codMicroRed, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			List<Object[]> lista= new ArrayList<Object[]>();
			String sql=" select count(e.id) as cantidad,mhe.Codigo_MicroRed, mhe.MicroRed from res_errores e  "
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5  and  mhe.Codigo_Red=:codMicroRed and e.año=:anio and e.mes between :mesI and :mesF and estado='1' "
					+ " group by mhe.Codigo_MicroRed, mhe.MicroRed ORDER BY count(e.id) ";
			Query resultados_consulta= em.createNativeQuery(sql).setParameter("codMicroRed", codMicroRed)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF);
			lista = resultados_consulta.getResultList();
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN avanceGraficoDetalleMicroRed");
			log.info(""+e);
			return null;
		}
	}



	@Override
	public List<Object[]> avanceGraficoDetalleREd(String codRed, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			List<Object[]> lista= new ArrayList<Object[]>();
			String sql=" select count(e.id) as cantidad,mhe.Codigo_Red, mhe.Red from res_errores e "
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5 and e.año=:anio and e.mes between :mesI and :mesF and estado='1' "
					+ " group by mhe.Codigo_Red, mhe.Red ORDER BY count(e.id)  ";
			Query resultados_consulta= em.createNativeQuery(sql)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF);
			lista = resultados_consulta.getResultList();
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN avanceGraficoDetalleREd");
			log.info(""+e);
			return null;
		}
	}
}
