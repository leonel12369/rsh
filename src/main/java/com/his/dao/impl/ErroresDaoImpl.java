package com.his.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.his.dao.ErroresDao;

import com.his.model.MaeErrores;
import com.his.model.ResErrores;


@Repository
public class ErroresDaoImpl implements ErroresDao{

	private final Logger log= LoggerFactory.getLogger(ErroresDaoImpl.class);
	
	@PersistenceContext
	private EntityManager em;

	
	@Override
	public Page<ResErrores> listarTodo(Pageable pageable) {
		// TODO Auto-generated method stub
		int inicio=pageable.getPageSize()*pageable.getPageNumber();
		int tamanio=pageable.getPageSize();
		try {
			int cantidad=(int) em.createNativeQuery("select count(*) from res_errores").getSingleResult();
			
			List<ResErrores> lista = em.createNativeQuery("select * from res_errores ORDER BY id OFFSET :inicio ROWS FETCH NEXT :tamanio ROWS ONLY",ResErrores.class)
					.setParameter("inicio", inicio)
					.setParameter("tamanio", tamanio).getResultList();
			
			Page<ResErrores> page = new PageImpl<>(lista,pageable,cantidad);
			return page;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR TODOS LOS ERRORES");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public Page<ResErrores> erroresFiltrados(Boolean estado,int tipoRegistrador,int idPrograma,int idEstablecimiento, String anio, String mes, Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			int inicio=pageable.getPageSize()*pageable.getPageNumber();
			int tamanio=pageable.getPageSize();
			String consulta_Cantidad="";
			String consulta_lista="";
			//String consulta_Cantidad=" select COUNT(*) from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores ) e inner join mae_citas m on m.Id_Cita= e.idCita ";
			//String consulta_lista=" select  e.id,e.idCita,e.idError,e.[año],e.mes from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores ) e inner join mae_citas m on m.Id_Cita= e.idCita ";
			
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				consulta_Cantidad="select count(*) from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita ";
				consulta_lista="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita ";
			}
			if(estado==true) {
				consulta_Cantidad="select count(*) from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita ";
				
				consulta_lista="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita ";
			}
			//------------------------------------------------------------------------------------------
			
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año =:anio  and e.mes =:mes and m.Id_Establecimiento =:idEstablecimiento ";
			String paginador=" ORDER BY e.id OFFSET :inicio ROWS FETCH NEXT :tamanio ROWS ONLY ";
			//String sql=;
			String rigthEqhali="";
			
			if(idPrograma!=0) {
				consulta_lista=consulta_lista+joinError;
				consulta_Cantidad=consulta_Cantidad+joinError;
				where=where+" and me.idEstrategia="+idPrograma;
			}
			if(tipoRegistrador!=0) {
				if(tipoRegistrador==1) {
					rigthEqhali=" right join mae_eqhali meq on meq.nombre_lote=m.Lote ";
				}
				if(tipoRegistrador==2) {
					where=where+" and m.Lote not in (select nombre_lote from mae_eqhali) ";
				}
			}
			System.out.println("cantidad: "+consulta_Cantidad+rigthEqhali+where);
			System.out.println("consulta: "+consulta_lista+rigthEqhali+where+paginador);
			
			int cantidad=(int) em.createNativeQuery(consulta_Cantidad+rigthEqhali+where)
					.setParameter("idEstablecimiento", idEstablecimiento)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.getSingleResult();
			
			List<ResErrores> lista = em.createNativeQuery(consulta_lista+rigthEqhali+where+paginador,ResErrores.class)
					.setParameter("idEstablecimiento", idEstablecimiento)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.setParameter("inicio", inicio)
					.setParameter("tamanio", tamanio).getResultList();
			
			Page<ResErrores> page = new PageImpl<>(lista,pageable,cantidad);
			return page;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR TODOS LOS ERRORES FILTRADOS");
			log.info(""+e);
			return null;
		}

	}


	@Override
	public List<ResErrores> listaErrores(String idCita) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from ResErrores where idCita.idCita =:idCita ",ResErrores.class).setParameter("idCita", idCita).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES DE UNA CITA");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public Page<ResErrores> erroresFiltradosUser(Boolean estado,int tipoRegistrador,int idPrograma,String anio, String mes, String dni, Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			int inicio=pageable.getPageSize()*pageable.getPageNumber();
			int tamanio=pageable.getPageSize();
			
			//String consulta_Cantidad="select COUNT(*) from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador ";
			//String consulta="select  e.id,e.idCita,e.idError,e.[año],e.mes from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador ";
			String consulta_Cantidad="";
			String consulta="";
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				consulta_Cantidad="select count(*) from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador";
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador";
			}
			if(estado==true) {
				consulta_Cantidad="select count(*) from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador ";
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador ";
			}
			//------------------------------------------------------------------------------------------
			
			
			
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año=:anio and e.mes=:mes and mr.Numero_Documento_Registrador=:dni ";
			String paginador=" ORDER BY e.id OFFSET :inicio ROWS FETCH NEXT :tamanio ROWS ONLY ";
			String rigthEqhali="";
			if(idPrograma!=0) {
				consulta=consulta+joinError;
				consulta_Cantidad=consulta_Cantidad+joinError;
				where=where+" and me.idEstrategia="+idPrograma;
			}
			if(tipoRegistrador!=0) {
				if(tipoRegistrador==1) {
					rigthEqhali=" right join mae_eqhali meq on meq.nombre_lote=m.Lote ";
				}
				if(tipoRegistrador==2) {
					where=where+" and m.Lote not in (select nombre_lote from mae_eqhali) ";
				}
			}
			
			System.out.println("cantidad: "+consulta_Cantidad+rigthEqhali+where);
			System.out.println("consulta: "+consulta+rigthEqhali+where+paginador);
			
			int cantidad=(int) em.createNativeQuery(consulta_Cantidad+rigthEqhali+where)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.getSingleResult();
			
			
			List<ResErrores> lista = em.createNativeQuery(consulta+rigthEqhali+where+paginador,ResErrores.class)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.setParameter("inicio", inicio)
					.setParameter("tamanio", tamanio).getResultList();
			
			Page<ResErrores> page = new PageImpl<>(lista,pageable,cantidad);
			
			return page;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR USER");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public Page<ResErrores> erroresFiltradosPersonal(Boolean estado,int idPrograma, String anio, String mes, String dni,
			Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			int inicio=pageable.getPageSize()*pageable.getPageNumber();
			int tamanio=pageable.getPageSize();
			//String consulta_Cantidad="select COUNT(*) from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			//String consulta="select  e.id,e.idCita,e.idError,e.[año],e.mes from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			
			String consulta_Cantidad="";
			String consulta="";
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				consulta_Cantidad="select count(*) from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			}
			if(estado==true) {
				consulta_Cantidad="select count(*) from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			}
			//------------------------------------------------------------------------------------------
			
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año=:anio and e.mes=:mes and mp.Numero_Documento_Personal=:dni ";
			String paginador=" ORDER BY e.id OFFSET :inicio ROWS FETCH NEXT :tamanio ROWS ONLY ";
			
			if(idPrograma!=0) {
				consulta=consulta+joinError;
				consulta_Cantidad=consulta_Cantidad+joinError;
				where=where+" and me.idEstrategia="+idPrograma;
			}
			
			int cantidad=(int) em.createNativeQuery(consulta_Cantidad+where)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.getSingleResult();
			
			
			List<ResErrores> lista = em.createNativeQuery(consulta+where+paginador,ResErrores.class)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.setParameter("inicio", inicio)
					.setParameter("tamanio", tamanio).getResultList();
			
			Page<ResErrores> page = new PageImpl<>(lista,pageable,cantidad);
			
			return page;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR PERSONAL");
			log.info(""+e);
			return null;
		}
	}

	//--------------------------------------- documentos --------------------------------------------

	@Override
	public List<ResErrores> listaErroresFiltrados(Boolean estado,int tipoRegistrador, int idPrograma, int idEstablecimiento,
			String anio, String mes) {
		// TODO Auto-generated method stub
		try {
			//String consulta_lista=" select * from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores ) e inner join mae_citas m on m.Id_Cita= e.idCita ";
			String consulta_lista="";
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				consulta_lista="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita ";
			}
			if(estado==true) {
				consulta_lista="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita ";
			}
			//------------------------------------------------------------------------------------------
			
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año =:anio  and e.mes =:mes and m.Id_Establecimiento =:idEstablecimiento ";
			String order=" ORDER BY e.id OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY";
			String rigthEqhali="";
			
			if(idPrograma!=0) {
				consulta_lista=consulta_lista+joinError;
				where=where+" and me.idEstrategia="+idPrograma;
			}
			if(tipoRegistrador!=0) {
				if(tipoRegistrador==1) {
					rigthEqhali=" right join mae_eqhali meq on meq.nombre_lote=m.Lote ";
				}
				if(tipoRegistrador==2) {
					where=where+" and m.Lote not in (select nombre_lote from mae_eqhali) ";
				}
			}
			
			System.out.println("consulta: "+consulta_lista+rigthEqhali+where+order);

			@SuppressWarnings("unchecked")
			List<ResErrores> lista = em.createNativeQuery(consulta_lista+rigthEqhali+where,ResErrores.class)
					.setParameter("idEstablecimiento", idEstablecimiento)
					.setParameter("anio", anio)
					.setParameter("mes", mes).getResultList();

			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR ESTABLECIMIENTOS PARA DOCUMENTOS");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public List<ResErrores> listaErroresFiltradosUser(Boolean estado,int tipoRegistrador, int idPrograma, String anio, String mes,
			String dni) {
		// TODO Auto-generated method stub
		try {
			//String consulta="select * from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador ";
			String consulta="";
			
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador";
			}
			if(estado==true) {

				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_REGISTRADOR mr on mr.Id_Registrador= m.Id_Registrador ";
			}
			//------------------------------------------------------------------------------------------
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año=:anio and e.mes=:mes and mr.Numero_Documento_Registrador=:dni ";
			String rigthEqhali="";
			String order=" ORDER BY e.id OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY";
			
			if(idPrograma!=0) {
				consulta=consulta+joinError;
				where=where+" and me.idEstrategia="+idPrograma;
			}
			if(tipoRegistrador!=0) {
				if(tipoRegistrador==1) {
					rigthEqhali=" right join mae_eqhali meq on meq.nombre_lote=m.Lote ";
				}
				if(tipoRegistrador==2) {
					where=where+" and m.Lote not in (select nombre_lote from mae_eqhali) ";
				}
			}
			

			System.out.println("consulta: "+consulta+rigthEqhali+where+order);

			@SuppressWarnings("unchecked")
			List<ResErrores> lista = em.createNativeQuery(consulta+rigthEqhali+where,ResErrores.class)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes).getResultList();
			
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR RESGISTRADOR PARA DOCUMENTOS");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public List<ResErrores> listaErroresFiltradosPersonal(Boolean estado,int idPrograma, String anio, String mes, String dni) {
		// TODO Auto-generated method stub
		try {

			//String consulta="select * from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			String consulta="";
			
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			}
			if(estado==true) {
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			}
			//------------------------------------------------------------------------------------------
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año=:anio and e.mes=:mes and mp.Numero_Documento_Personal=:dni ";
			String order=" ORDER BY e.id OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY";
			
			if(idPrograma!=0) {
				consulta=consulta+joinError;
				where=where+" and me.idEstrategia="+idPrograma;
			}
			
			System.out.println("consulta: "+consulta+where+order);
			
			@SuppressWarnings("unchecked")
			List<ResErrores> lista = em.createNativeQuery(consulta+where,ResErrores.class)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes).getResultList();
			
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR PERSONAL PARA DOCUMENTOS");
			log.info(""+e);
			return null;
		}
	}

	//--------------------------------------- fin de documentos --------------------------------------------
	@Override
	public int editar(ResErrores resErrores) {
		// TODO Auto-generated method stub
		try {
			if (resErrores.getId()>0) {
				 em.merge(resErrores);
			} else {
				em.persist(resErrores);
			}
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL EDITAR LA CITA");
			log.info(""+e);
			return 0;
		}
	}


	@Override
	public ResErrores buscar(int idResErrores) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from ResErrores where  id=:idResErrores",ResErrores.class).setParameter("idResErrores", idResErrores).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL BUSCAR EL ID DE LA TABLA RESERRORES");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public Page<ResErrores> erroresFiltradosPaciente(Boolean estado, String anio, String mes, String dni,
			Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			int inicio=pageable.getPageSize()*pageable.getPageNumber();
			int tamanio=pageable.getPageSize();
			//String consulta_Cantidad="select COUNT(*) from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			//String consulta="select  e.id,e.idCita,e.idError,e.[año],e.mes from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			
			String consulta_Cantidad="";
			String consulta="";
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				consulta_Cantidad="select count(*) from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PACIENTE mp on mp.Id_Paciente= m.Id_Paciente ";
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PACIENTE mp on mp.Id_Paciente= m.Id_Paciente ";
			}
			if(estado==true) {
				consulta_Cantidad="select count(*) from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PACIENTE mp on mp.Id_Paciente= m.Id_Paciente  ";
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PACIENTE mp on mp.Id_Paciente= m.Id_Paciente  ";
			}
			//------------------------------------------------------------------------------------------
			
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año=:anio and e.mes=:mes and mp.Numero_Documento_Paciente=:dni ";
			String paginador=" ORDER BY e.id OFFSET :inicio ROWS FETCH NEXT :tamanio ROWS ONLY ";
			
			
			int cantidad=(int) em.createNativeQuery(consulta_Cantidad+where)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.getSingleResult();
			
			
			List<ResErrores> lista = em.createNativeQuery(consulta+where+paginador,ResErrores.class)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes)
					.setParameter("inicio", inicio)
					.setParameter("tamanio", tamanio).getResultList();
			
			Page<ResErrores> page = new PageImpl<>(lista,pageable,cantidad);
			
			return page;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR PACIENTE");
			log.info(""+e);
			return null;
		}
	}


	@Override
	public List<ResErrores> listaErroresFiltradosPaciente(Boolean estado, String anio, String mes, String dni) {
		// TODO Auto-generated method stub
		try {

			//String consulta="select * from (select id,idCita,idError,[año],mes,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from res_errores) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PERSONAL mp on mp.Id_Personal= m.Id_Personal ";
			String consulta="";
			
			//------------------------------- PRUEBA ESTADO-------------------------------
			if(estado==false) {
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from (/*x*/select id,idCita,idError,[año],mes,estado from res_errores where estado='0')x ) e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PACIENTE mp on mp.Id_Paciente= m.Id_Paciente ";
			}
			if(estado==true) {
				
				consulta="select e.id,e.idCita,e.idError,e.[año],e.mes,e.estado from "
						+ " (/*e*/select id,idCita,idError,[año],mes,estado,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as i from "
						+ " (/*e1*/select id,idCita,idError,[año],mes,estado from res_errores where idCita not in(select idCita from "
						+ " (/*e2*/select idCita,ROW_NUMBER() over(PARTITION BY idCita order by id asc) as j from "
						+ " (/*e3*/select id,idCita from res_errores where estado='0')e3 )e2 where j=1))e1)e inner join mae_citas m on m.Id_Cita= e.idCita inner join MAESTRO_PACIENTE mp on mp.Id_Paciente= m.Id_Paciente  ";
			}
			//------------------------------------------------------------------------------------------
			
			String joinError=" inner join mae_errores me on me.idError=e.idError ";
			String where=" where i=1 and e.año=:anio and e.mes=:mes and mp.Numero_Documento_Paciente=:dni ";
			String order=" ORDER BY e.id OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY";
			
			
			System.out.println("consulta: "+consulta+where+order);
			
			@SuppressWarnings("unchecked")
			List<ResErrores> lista = em.createNativeQuery(consulta+where,ResErrores.class)
					.setParameter("dni", dni)
					.setParameter("anio", anio)
					.setParameter("mes", mes).getResultList();
			
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL LISTAR LOS ERRORES FILTRADOS POR PACIENTES PARA DOCUMENTOS");
			log.info(""+e);
			return null;
		}
	}
	
	
}
