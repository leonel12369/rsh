package com.his.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.GenericDao;
import com.his.model.Generic;


@Repository
public class GenericDaoImpl implements GenericDao{

	@PersistenceContext
	private EntityManager em;
	
	private final Logger log= LoggerFactory.getLogger(GenericDaoImpl.class);
	
	public void queryWithAuthorBookCountHibernateMapping() {
		  Session session = (Session)this.em.getDelegate();//hibernate
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> avance_microred(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(""
					+ " select count(e.id) as campo1,mhe.Codigo_MicroRed as campo2 ,mhe.MicroRed as campo3 from res_errores e "
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and e.año=:anio and e.mes between :mesI and :mesF and estado='1'"
					+ " group by mhe.Codigo_MicroRed,mhe.MicroRed ORDER BY mhe.Codigo_MicroRed ")
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .list();
			//System.out.println(lista);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS AVANCE - MICRORED");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Object[]> total_microred(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String sql=" select count(e.id) as cantidad,mhe.Codigo_MicroRed,mhe.MicroRed from res_errores e " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento " + 
					" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and e.año=:anio and e.mes between :mesI and :mesF " + 
					" group by mhe.Codigo_MicroRed,mhe.MicroRed ORDER BY mhe.Codigo_MicroRed ";
			List<Object[]> lista= new ArrayList<Object[]>();
			Query resultados_consulta= em.createNativeQuery(sql).setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF);
			lista = resultados_consulta.getResultList();
			System.out.println(sql);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS AVANCE - MICRORED");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public Generic datos_microred(String codigoMicrored) {
		// TODO Auto-generated method stub
		try {
			
			Generic objeto = (Generic) ((Session)this.em.getDelegate()).createSQLQuery(""
					+ " select  distinct(MicroRed) as campo1,Departamento as campo3 from MAESTRO_HIS_ESTABLECIMIENTO "
					+ " where Codigo_Disa=5 and Codigo_Red='01' and Codigo_MicroRed=:codigoMicrored")
					.setParameter("codigoMicrored", codigoMicrored)
					.addScalar("campo1",StandardBasicTypes.STRING)
					//.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					//.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class)).getSingleResult();
			        
			return objeto;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN DATOS MICRORED");
			log.info(""+e);
			return null;
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> avance_personal(String establecimiento,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select=" SELECT q1.Numero_Documento_Personal as campo1, q1.Nombres_Personal as campo2, " + 
					" concat (q1.Apellido_Paterno_Personal ,' ',q1.Apellido_Materno_Personal) as campo3, " + 
					" q1.cantidad as campo4 ,q2.cantidad as campo5 "+
					" from ";
			String from="(select count(e.id) as  cantidad, mp.Id_Personal,mp.Numero_Documento_Personal, mp.Nombres_Personal, mp.Apellido_Paterno_Personal, mp.Apellido_Materno_Personal from res_errores e " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_PERSONAL mp on mp.Id_Personal =mc.Id_Personal " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where mhe.Id_Establecimiento=:establecimiento  and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mp.Id_Personal,mp.Nombres_Personal,mp.Apellido_Paterno_Personal, mp.Apellido_Materno_Personal,mp.Numero_Documento_Personal " + 
					") as ";//q1//q2
			String where=" WHERE q1.Id_Personal = q2.Id_Personal ";
			
			String unido=select+from+estado1+group_by+" q1 "+" , "+from+estado2+group_by+" q2 "+where;
			
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("establecimiento", establecimiento)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER AVANCE_PERSONAL");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public Generic datos_Establecimiento(String Establecimiento) {
		// TODO Auto-generated method stub
		try {
			
			Generic objeto = (Generic) ((Session)this.em.getDelegate()).createSQLQuery(""
					+ " select  distinct(Id_Establecimiento) as campo1,Red as campo2 ,MicroRed as campo3,Nombre_Establecimiento as campo4, "
					+ " Descripcion_Sector as campo5,Departamento as campo6,Provincia as campo7,Distrito as campo8 from MAESTRO_HIS_ESTABLECIMIENTO "
					+ " where Codigo_Disa=5 and Codigo_Red='01' and Id_Establecimiento=:establecimiento")
					.setParameter("establecimiento", Establecimiento)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					.addScalar("campo5",StandardBasicTypes.STRING)
					.addScalar("campo6",StandardBasicTypes.STRING)
					.addScalar("campo7",StandardBasicTypes.STRING)
					.addScalar("campo8",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class)).getSingleResult();
			        
			return objeto;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN BD EN INFORMACION ESTABLECIMIENTO");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Object[]> ranking(String establecimiento, Boolean estado,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			
			String consulta=" select count(e.id),me.descripcionCorta  from res_errores e  "
					+ " inner join mae_citas mc on mc.Id_Cita = e.idCita "
					+ " inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento "
					+ " inner join mae_errores me on me.idError=e.idError "
					+ " where mhe.Id_Establecimiento=:establecimiento  and e.año=:anio and e.mes between :mesI and :mesF  and  estado=:estado "
					+ " group by me.descripcionCorta order by count(e.id) desc ";
			
			
			
			List<Object[]> lista= new ArrayList<Object[]>();
			Query resultados_consulta= em.createNativeQuery(consulta)
					.setParameter("establecimiento", establecimiento)
					.setParameter("estado", estado)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF);
			
			lista = resultados_consulta.getResultList();
			
			return lista;
			
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS ranking");
			log.info(""+e);
			return null;
		}
	}

	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> detalleMicroRed_Establecimientos(String establecimiento,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select=" SELECT q1.Id_Establecimiento as campo1, q1.Nombre_Establecimiento as campo2, " + 
					" q1.cantidad as campo3 ,q2.cantidad as campo4 " + 
					" from ";
			String from="(select count(e.id) as cantidad,mhe.Id_Establecimiento,mhe.Nombre_Establecimiento from res_errores e " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento " + 
					" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and mhe.Codigo_MicroRed=:establecimiento and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Id_Establecimiento,mhe.Nombre_Establecimiento  " + 
					") as ";//q1//q2
			String where=" WHERE q1.Id_Establecimiento = q2.Id_Establecimiento ";
			
			String unido=select+from+estado1+group_by+" q1 "+" , "+from+estado2+group_by+" q2 "+where;
			
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("establecimiento", establecimiento)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)

			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER detalleMicroRed_Establecimientos");
			log.info(""+e);
			return null;
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public Generic nombres_resultados(String tabla, String dni) {
		// TODO Auto-generated method stub
		try {
			String sql="";
			
			switch(tabla) {
			
				case "paciente":
					sql=" select concat(mpa.Apellido_Paterno_Paciente,' ',mpa.Apellido_Materno_Paciente,' ',mpa.Nombres_Paciente) as campo1 "
							+ " from MAESTRO_PACIENTE mpa where mpa.Numero_Documento_Paciente=:dni "
							+ " group by mpa.Apellido_Paterno_Paciente,mpa.Apellido_Materno_Paciente,mpa.Nombres_Paciente ";
					break;
					
				case "registrador":
					sql=" select concat(mr.Apellido_Paterno_Registrador,' ',mr.Apellido_Materno_Registrador,' ',mr.Nombres_Registrador)  as campo1 "
							+ " from MAESTRO_REGISTRADOR mr where mr.Numero_Documento_Registrador=:dni "
							+ " group by mr.Apellido_Paterno_Registrador,mr.Apellido_Materno_Registrador,mr.Nombres_Registrador ";
					break;
					
				case "personal":
					sql=" select concat(mpe.Apellido_Paterno_Personal,' ',mpe.Apellido_Materno_Personal,' ',mpe.Nombres_Personal)  as campo1 "
							+ " from MAESTRO_PERSONAL mpe where mpe.Numero_Documento_Personal=:dni "
							+ " group by mpe.Apellido_Paterno_Personal,mpe.Apellido_Materno_Personal,mpe.Nombres_Personal ";
					break;
			}
			
			Generic lista = (Generic) ((Session)this.em.getDelegate()).createSQLQuery(sql)
					.setParameter("dni", dni)
					.addScalar("campo1",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getSingleResult();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER nombres_resultados");
			log.info(""+e);
			Generic obG= new Generic();
			obG.setCampo1("No existe esta persona en la BD");
			return obG;
			//return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> no_avance(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select_p=" select 0 as campo1, mhe.Codigo_MicroRed as campo2 ,mhe.MicroRed as campo3 from res_errores e  ";
			String select_s=" select mhe.Codigo_MicroRed  from res_errores e ";
			String inner_cita=" inner join mae_citas mc on mc.Id_Cita = e.idCita ";
			String inner_establecimiento="inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento ";
			String where_p=" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and e.año=:anio and e.mes between :mesI and :mesF and estado='0' and mhe.Codigo_MicroRed not in ";
			String where_s=" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and e.año=:anio and e.mes between :mesI and :mesF and estado='1'";
			String group_by=" group by mhe.Codigo_MicroRed,mhe.MicroRed ";
			
			String sql=select_p+inner_cita+inner_establecimiento
					+where_p+" ( "+select_s+inner_cita+inner_establecimiento+where_s+group_by+" ) "+group_by;
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(sql)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			      
			
			System.out.println("no_avance"+sql);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER nombres_resultados");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> avance_est_detMicrocred(String idMicrored, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select=" SELECT q2.Id_Establecimiento as campo1, q2.Nombre_Establecimiento as campo2, " + 
					" 0 as campo3 ,q2.cantidad as campo4 from ";
			String from="(select count(e.id) as cantidad,mhe.Id_Establecimiento as Id_Establecimiento,mhe.Nombre_Establecimiento from res_errores e  " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento " + 
					" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and mhe.Codigo_MicroRed=:idMicrored and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Id_Establecimiento,mhe.Nombre_Establecimiento  " + 
					")  ";//q1//q2
			String rigth_join=" right join  ";
			String on=" on q2.Id_Establecimiento=q1.Id_Establecimiento ";
			String where=" where q1.Id_Establecimiento is null ";
			
			String unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
						
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("idMicrored", idMicrored)
					.setParameter("anio", anio)
					.setParameter("mesI", mesI)
					.setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			      
			
			//System.out.println("no_avance"+sql);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER avance_est_detMicrocred");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> no_avance_personal(String establecimiento, String anio, int mesI, int mesF,int terminado) {
		// TODO Auto-generated method stub
		try {
			
			String select=" SELECT q2.Numero_Documento_Personal as campo1, q2.Nombres_Personal as campo2, " + 
					" concat (q2.Apellido_Paterno_Personal ,' ',q2.Apellido_Materno_Personal) as campo3, " + 
					" 0 as campo4 ,q2.cantidad as campo5 "+
					" from ";
			String from="(select count(e.id) as  cantidad,mp.Id_Personal,mp.Numero_Documento_Personal, mp.Nombres_Personal, mp.Apellido_Paterno_Personal, mp.Apellido_Materno_Personal from res_errores e  " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_PERSONAL mp on mp.Id_Personal =mc.Id_Personal " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where mhe.Id_Establecimiento=:establecimiento  and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mp.Id_Personal,mp.Nombres_Personal,mp.Apellido_Paterno_Personal, mp.Apellido_Materno_Personal,mp.Numero_Documento_Personal  " + 
					") ";//q1//q2
			
			String rigth_join=" right join  ";
			
			String on=" on q2.Id_Personal=q1.Id_Personal  ";
			
			String where=" WHERE q1.Id_Personal is null ";
			String unido="";
			if(terminado==1) {
				 unido=select+from+estado2+group_by+" q1 "+rigth_join+from+estado1+group_by+" q2 "+on+where;
			}
			else {
				 unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
			}
			//String unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
			
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("establecimiento", establecimiento)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER no_avance_personal");
			log.info(""+e);
			return null;
		}
		
		
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Generic> no_avance_microred(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select=" SELECT q2.Codigo_MicroRed as campo1,q2.MicroRed as campo2,0 as campo3 ,q2.cantidad as campo4 from "; 
		
			String from=" (select count(e.id) as cantidad,mhe.Codigo_MicroRed as Codigo_MicroRed ,mhe.MicroRed as MicroRed from res_errores e  " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where mhe.Codigo_Disa=5  and mhe.Codigo_Red='01' and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Codigo_MicroRed,mhe.MicroRed " + 
					") ";//q1//q2
			
			String right="right join ";
			String on=" on q1.Codigo_MicroRed=q2.Codigo_MicroRed ";
			
			String where=" WHERE q1.Codigo_MicroRed is null ";
			
			String unido=select+from+estado1+group_by+" q1 "+ right +from+estado2+group_by+" q2 "+on+where;
			System.out.println("unido: "+unido);
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER no_avance_microred");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public Generic datos_Redes() {
		// TODO Auto-generated method stub
		try {
			
			Generic objeto = (Generic) ((Session)this.em.getDelegate()).createSQLQuery(""
					+ " select  distinct(Disa) as campo1,Departamento as campo3 from MAESTRO_HIS_ESTABLECIMIENTO "
					+ " where Codigo_Disa=5 ")
					.addScalar("campo1",StandardBasicTypes.STRING)
					//.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					//.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class)).getSingleResult();
			        
			return objeto;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN DATOS datos_Redes");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public Generic datos_microredes(String codigoRed) {
		// TODO Auto-generated method stub
		try {
			
			Generic objeto = (Generic) ((Session)this.em.getDelegate()).createSQLQuery(""
					+ " select  distinct(Red) as campo1,Departamento as campo3 from MAESTRO_HIS_ESTABLECIMIENTO "
					+ " where Codigo_Disa=5 and Codigo_Red=:codigoRed")
					.setParameter("codigoRed", codigoRed)
					.addScalar("campo1",StandardBasicTypes.STRING)
					//.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					//.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class)).getSingleResult();
			        
			return objeto;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN datos_microredes");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public List<Generic> avance_redes(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select=" SELECT q1.Codigo_Red as campo1, q1.red as campo2, " + 
					" q1.cantidad as campo3 ,q2.cantidad as campo4  " + 
					" from ";
			String from=" (select count(e.id) as cantidad,mhe.Codigo_Red,mhe.red from res_errores e  " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where mhe.Codigo_Disa=5  and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Codigo_Red,mhe.red  " + 
					") as ";//q1//q2
			String where=" WHERE q1.Codigo_Red = q2.Codigo_Red ";
			
			String unido=select+from+estado1+group_by+" q1 "+" , "+from+estado2+group_by+" q2 "+where;
			
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER AVANCE_PERSONAL");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public List<Generic> no_avance_redes(String anio, int mesI, int mesF, int terminado) {
		// TODO Auto-generated method stub
try {
			
			String select=" SELECT q2.Codigo_Red as campo1, q2.red as campo2, 0 as campo3 ,q2.cantidad as campo4 from " ;

			String from="(select count(e.id) as cantidad,mhe.Codigo_Red as Codigo_Red,mhe.red from res_errores e  " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " +  
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where  mhe.Codigo_Disa=5  and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Codigo_Red,mhe.red " + 
					") ";//q1//q2
			
			String rigth_join=" right join  ";
			
			String on=" on q2.Codigo_Red=q1.Codigo_Red ";
			
			String where="  where q1.Codigo_Red is null ";
			String unido="";
			if(terminado==1) {
				 unido=select+from+estado2+group_by+" q1 "+rigth_join+from+estado1+group_by+" q2 "+on+where;
			}
			else {
				 unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
			}
			//String unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
			System.out.println(unido);
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER no_avance_redes");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public List<Generic> avance_microRedes(String Codigo_red, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		try {
			String select=" SELECT q1.Codigo_MicroRed as campo1, q1.MicroRed as campo2, " + 
					" q1.cantidad as campo3 ,q2.cantidad as campo4  " + 
					" from ";
			String from=" (select count(e.id) as cantidad,mhe.Codigo_MicroRed,mhe.MicroRed from res_errores e  " + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where mhe.Codigo_Disa=5 and mhe.Codigo_Red=:Codigo_red  and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Codigo_MicroRed,mhe.MicroRed  " + 
					") as ";//q1//q2
			String where=" WHERE q1.Codigo_MicroRed = q2.Codigo_MicroRed ";
			
			String unido=select+from+estado1+group_by+" q1 "+" , "+from+estado2+group_by+" q2 "+where;
			
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("Codigo_red", Codigo_red)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN avance_microRedes");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public List<Generic> no_avance_microRedes(String Codigo_red, String anio, int mesI, int mesF, int terminado) {
		// TODO Auto-generated method stub
		try {
			
			String select=" SELECT q2.Codigo_MicroRed as campo1, q2.MicroRed as campo2, 0 as campo3 ,q2.cantidad as campo4 from " ;

			String from="(select count(e.id) as cantidad,mhe.Codigo_MicroRed as Codigo_MicroRed,mhe.MicroRed from res_errores e" + 
					" inner join mae_citas mc on mc.Id_Cita = e.idCita " + 
					" inner join MAESTRO_HIS_ESTABLECIMIENTO mhe on mhe.Id_Establecimiento=mc.Id_Establecimiento  " + 
					" where mhe.Codigo_Disa=5  and mhe.Codigo_Red=:Codigo_red and e.año=:anio and e.mes between :mesI and :mesF ";
			
			String estado1=" and  estado='1' ";
			String estado2=" and  estado='0' ";
			
			String group_by	=" group by mhe.Codigo_MicroRed,mhe.MicroRed " + 
					") ";//q1//q2
			
			String rigth_join=" right join  ";
			
			String on=" on q2.Codigo_MicroRed=q1.Codigo_MicroRed  ";
			
			String where="  where q1.Codigo_MicroRed is null ";
			String unido="";
			if(terminado==1) {
				 unido=select+from+estado2+group_by+" q1 "+rigth_join+from+estado1+group_by+" q2 "+on+where;
			}
			else {
				 unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
			}
			//String unido=select+from+estado1+group_by+" q1 "+rigth_join+from+estado2+group_by+" q2 "+on+where;
			System.out.println(unido);
			List<Generic> lista = ((Session)this.em.getDelegate()).createSQLQuery(unido)
					.setParameter("Codigo_red", Codigo_red)
					.setParameter("anio", anio).setParameter("mesI", mesI).setParameter("mesF", mesF)
					.addScalar("campo1",StandardBasicTypes.STRING)
					.addScalar("campo2",StandardBasicTypes.STRING)
					.addScalar("campo3",StandardBasicTypes.STRING)
					.addScalar("campo4",StandardBasicTypes.STRING)
					//.addScalar("campo5",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Generic.class))
			        .getResultList();
			        
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS TRAER no_avance_microRedes");
			log.info(""+e);
			return null;
		}
	}
}
