package com.his.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.CitaDao;
import com.his.model.MaeCitas;

@Repository
public class CitaDaoImpl  implements CitaDao{

	private final Logger log= LoggerFactory.getLogger(CitaDaoImpl.class);
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public MaeCitas busquedaCita(String idCita) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeCitas where idCita=:idCita",MaeCitas.class).setParameter("idCita", idCita).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL BUSCAR LA CITA");
			log.info(""+e);
			return null;
		}
	}
	


}
