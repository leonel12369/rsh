package com.his.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.CalendarioDao;
import com.his.model.MaeCalendario;
import com.his.model.MaeCitas;
import com.his.model.MaeEqhali;

@Repository
public class CalendarioDaoImpl implements CalendarioDao{
	
private final Logger log= LoggerFactory.getLogger(CalendarioDaoImpl.class);
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<MaeCalendario> listar() {
		// TODO Auto-generated method stub
		try {
			return em.createNativeQuery("select * from mae_calendario ",MaeCalendario.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL TRAER LISTAR CALENDARIO");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public void agregar(MaeCalendario calendario) {
		// TODO Auto-generated method stub
		try {
			if(calendario.getId()>0) {
				em.merge(calendario);
			}
			else {
				em.persist(calendario);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN agregar calendario");
			log.info(""+e);
		}
	}

	@Override
	public void eliminar(MaeCalendario calendario) {
		// TODO Auto-generated method stub
		try {
			em.remove(calendario);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ELIMINAR calendario");
			log.info(""+e);
		}
	}

	@Override
	public MaeCalendario buscar(int id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeCalendario where id =:id ",MaeCalendario.class).setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN buscar calendario");
			log.info(""+ e);
			return null;
		}
	}

	@Override
	public List<MaeCalendario> buscar_mes_anio(int mes, int anio) {
		// TODO Auto-generated method stub
		try {
			return em.createNativeQuery("select * from mae_calendario where MONTH(f_comienzo)=:mes and YEAR(f_comienzo)=:anio ",MaeCalendario.class).setParameter("mes", mes).setParameter("anio", anio).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN buscar calendario");
			log.info(""+ e);
			return null;
		}
	}

}
