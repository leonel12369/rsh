package com.his.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.MaestroDao;
import com.his.model.MaeEqhali;
import com.his.model.MaeErrores;
import com.his.model.MaeEstrategiaSanitaria;


@Repository
public class MaestroDaoImpl implements MaestroDao{

private final Logger log= LoggerFactory.getLogger(MaestroDaoImpl.class);
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<MaeErrores> listarError() {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeErrores where estado=true ",MaeErrores.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL listarError");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public List<MaeEstrategiaSanitaria> listarEstrategia() {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeEstrategiaSanitaria where estado=true",MaeEstrategiaSanitaria.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL BUSCAR LA listarEstrategia");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public List<MaeEqhali> listarEqhali() {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeEqhali where estado=true",MaeEqhali.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL BUSCAR LA listarEqhali");
			log.info(""+e);
			return null;
		}
	}

	@Override
	public void save_maeEqhali(MaeEqhali maeEqhali) {
		// TODO Auto-generated method stub
		try {
			if(maeEqhali.getId()>0) {
				em.merge(maeEqhali);
			}
			else {
				em.persist(maeEqhali);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN save_maeEqhali");
			log.info(""+e);
		}
	}

	@Override
	public void eliminar_maeEqhali(MaeEqhali maeEqhali) {
		// TODO Auto-generated method stub
		try {
			em.merge(maeEqhali);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ELIMINAR eliminar_maeEqhali");
			log.info(""+e);
		}
	}

	@Override
	public MaeEqhali buscar_maeEqhali(int id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeEqhali where id =:id ",MaeEqhali.class).setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN buscar_maeEqhali");
			log.info(""+ e);
			return null;
		}
	}

	@Override
	public void save_estrategia(MaeEstrategiaSanitaria estrategia) {
		// TODO Auto-generated method stub
		try {
			if(estrategia.getIdEstrategia()>0) {
				em.merge(estrategia);
			}
			else {
				em.persist(estrategia);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN save_estrategia");
			log.info(""+e);
		}
	}

	@Override
	public void eliminar_estrategia(MaeEstrategiaSanitaria estrategia) {
		// TODO Auto-generated method stub
		try {
			em.merge(estrategia);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ELIMINAR eliminar_estrategia");
			log.info(""+e);
		}
	}

	@Override
	public MaeEstrategiaSanitaria buscar_estrategia(int id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeEstrategiaSanitaria where idEstrategia =:id ",MaeEstrategiaSanitaria.class).setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN buscar_estrategia");
			log.info(""+ e);
			return null;
		}
	}

	@Override
	public void save_maeError(MaeErrores error) {
		// TODO Auto-generated method stub
		try {
			em.persist(error);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN save_maeEqhali");
			log.info(""+e);
		}
	}

	@Override
	public void eliminar_maeError(MaeErrores error) {
		// TODO Auto-generated method stub
		try {
			em.merge(error);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ELIMINAR eliminar_maeError");
			log.info(""+e);
		}
	}

	@Override
	public MaeErrores buscar_maeError(String id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("from MaeErrores where idError =:id ",MaeErrores.class).setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN buscar_maeError");
			log.info(""+ e);
			return null;
		}
	}

	@Override
	public void editar_maeError(MaeErrores error) {
		// TODO Auto-generated method stub
		try {
			em.merge(error);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN editar_maeError");
			log.info(""+e);
		}
	}

	@Override
	public String idError(int idEstrategia) {
		// TODO Auto-generated method stub
		try {
			String lenEstrategia=String.valueOf(idEstrategia);
			int cantidad=lenEstrategia.length();
			int inicio=0;
			int fin=0;
			switch(cantidad) {
			case 1:
				inicio=2;
				fin=3;
				break;
			case 2:
				inicio=3;
				fin=4;
				break;
			}
			
			return em.createNativeQuery("select COALESCE(max(SUBSTRING(idError,:inicio,:fin)),0) from mae_errores where idEstrategia=:idEstrategia")
					.setParameter("idEstrategia", idEstrategia)
					.setParameter("inicio", inicio)
					.setParameter("fin", fin)
					.getSingleResult().toString(); 
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN idError");
			log.info(""+e);
			return "-1";
		}
	}
}
