package com.his.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.his.dao.UbigeoDao;
import com.his.model.MaestroHisEstablecimiento;
import com.his.model.MaestroHisUbigeoIneiReniec;
import com.his.model.Ubigeo;



@Repository
public class UbigeoDaoImpl implements UbigeoDao {

	@PersistenceContext
	private EntityManager em;
	
	private final Logger log= LoggerFactory.getLogger(UbigeoDaoImpl.class);
	
	public void queryWithAuthorBookCountHibernateMapping() {
		  Session session = (Session)this.em.getDelegate();//hibernate
	}
	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> departamento() {
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> departamento = ((Session)this.em.getDelegate()).createSQLQuery("SELECT CodDepInei as value, Departamento as nombre from MAESTRO_HIS_UBIGEO_INEI_RENIEC GROUP BY CodDepInei, Departamento order by Departamento")
					//.addScalar("CodDepInei").addScalar("Departamento")
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return departamento;
			
		} catch (Exception e) {
			// TODO: handle exception
				log.info("ERROR EN LA BASE DE DATOS LISTAR DEPARTAMENTOS");
				log.info(""+e);
			return null;
		}
		
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> provincia(String codDepartamento) {
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> provincia = ((Session)this.em.getDelegate()).createSQLQuery("SELECT CodProvInei as value, Provincia as nombre from MAESTRO_HIS_UBIGEO_INEI_RENIEC where CodDepInei =:codDepartamento GROUP BY CodProvInei, Provincia order by Provincia").setParameter("codDepartamento", codDepartamento)
					//.addScalar("CodDepInei").addScalar("Departamento")
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return provincia;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS LISTAR PROVINCIA");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> distrito(String codDepa,String codProv) {
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> distrito = ((Session)this.em.getDelegate()).createSQLQuery("SELECT CodDistInei as value, Distrito as nombre from MAESTRO_HIS_UBIGEO_INEI_RENIEC where CodDepInei =:codDepa and CodProvInei =:codProv GROUP BY CodDistInei, Distrito order by Distrito")
					.setParameter("codDepa", codDepa).setParameter("codProv", codProv)
					//.addScalar("CodDepInei").addScalar("Departamento")
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return distrito;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS LISTAR DISTRITO");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> red(int codDisa) {
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> red = ((Session)this.em.getDelegate()).createSQLQuery("select Codigo_Red as value ,Red as nombre from MAESTRO_HIS_ESTABLECIMIENTO WHERE Codigo_Disa =:codDisa GROUP BY Codigo_Red,Red ORDER BY Codigo_Red")
					.setParameter("codDisa", codDisa)
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return red;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS LISTAR RED");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> microRed(int codDisa,String codRed) {
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> microRed = ((Session)this.em.getDelegate()).createSQLQuery("select Codigo_MicroRed as value ,MicroRed as nombre from MAESTRO_HIS_ESTABLECIMIENTO WHERE Codigo_Red=:codRed and Codigo_Disa=:codDisa GROUP BY Codigo_MicroRed,MicroRed ORDER BY Codigo_MicroRed")
					.setParameter("codRed", codRed)
					.setParameter("codDisa", codDisa)
					//.addScalar("CodDepInei").addScalar("Departamento")
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return microRed;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS LISTAR MICRO RED");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> establecimiento(int codDisa,String codRed, String codMicroRed){
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> establecimiento = ((Session)this.em.getDelegate()).createSQLQuery("select Id_Establecimiento as value ,Nombre_Establecimiento as nombre from MAESTRO_HIS_ESTABLECIMIENTO WHERE Codigo_Disa=:codDisa and Codigo_Red=:codRed and Codigo_MicroRed=:codMicroRed GROUP BY Id_Establecimiento,Nombre_Establecimiento ORDER BY Id_Establecimiento")
					.setParameter("codRed", codRed).setParameter("codMicroRed", codMicroRed).setParameter("codDisa", codDisa)
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return establecimiento;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA BASE DE DATOS LISTAR DISTRITO");
			log.info(""+e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Ubigeo> disa() {
		// TODO Auto-generated method stub
		try {
			List<Ubigeo> disa = ((Session)this.em.getDelegate()).createSQLQuery("select Codigo_Disa as value ,Disa as nombre from MAESTRO_HIS_ESTABLECIMIENTO WHERE Codigo_Disa IS NOT NULL GROUP BY Codigo_Disa,Disa ORDER BY Codigo_Disa")
					.addScalar("value",StandardBasicTypes.STRING).addScalar("nombre",StandardBasicTypes.STRING)
			        .setResultTransformer(new AliasToBeanResultTransformer(Ubigeo.class))
			        .list();
			return disa;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR EN LA LISTAR DISA");
			log.info(""+e);
			return null;
		}
	}
	
}
