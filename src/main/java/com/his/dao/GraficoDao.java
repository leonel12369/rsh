package com.his.dao;

import java.util.List;

import com.his.model.Grafico;

public interface GraficoDao {
	
	public List<Grafico> mesError();

	public List<Grafico> erroresComunes();
	
	public List<Object[]> errorMicroRed(String anio, int mes,  int fmes, int idEstrategia);
	
	public List<Grafico> errorEstablecimiento(String codMicroRed,String anio, int mes,int fmes, int idEstrategia);
	
	public List<Grafico> errorMayorEstablecimiento();//no
	
	public List<Object[]> cantidadErrorMicrored(String codMicroRed,String anio, int mes,int fmes, int idEstrategia);
	
	public List<Object[]> errorComunMicrored(String codMicroRed,String anio, int mes,int fmes, int idEstrategia);
	
	public List<Object[]> avanceGraficoDetalleEstablecimiento(String codMicroRed,String anio, int mesI, int mesF);
	
	public int cantidadAvance_RestanteMicrored(String codMicroRed,Boolean estado,String anio, int mesI, int mesF);
	
	public int cantidadAvance_RestanteEstablecimiento(String establecimiento,Boolean estado,String anio, int mesI, int mesF);
	
	
	public int cantidadAvance_RestanteMicrored2(String codMicroRed,Boolean estado,String anio, int mesI, int mesF);
	
	public int cantidadAvance_RestanteRed(String red,Boolean estado,String anio, int mesI, int mesF);
	
	public List<Object[]> avanceGraficoDetalleMicroRed(String codMicroRed,String anio, int mesI, int mesF);
	
	public List<Object[]> avanceGraficoDetalleREd(String codRed,String anio, int mesI, int mesF);
}

