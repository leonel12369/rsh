package com.his.security.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.his.dao.UsuarioDao;
import com.his.model.MaeRol;
import com.his.model.MaeUsuario;
import com.his.security.dao.UsuarioSecurityDao;

@Service
public class UsuarioSecurityServiceImpl implements UserDetailsService{

	@Autowired
	private UsuarioSecurityDao usuarioSecurityDao;  
	
	@Autowired
	private UsuarioDao usuarioDao;  

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		MaeUsuario usuario= usuarioSecurityDao.findUser(username);
		
		List<GrantedAuthority> roles= new ArrayList<GrantedAuthority>();
		List<MaeRol> roles_user= usuarioDao.findRolUsuario(usuario.getIdUsuario());
		for(MaeRol rol : roles_user) {
			roles.add(new SimpleGrantedAuthority(rol.getNombre()));
		}
		
		return new User(username, usuario.getContrasenia(), true, true, true, true, roles);
	}

}
