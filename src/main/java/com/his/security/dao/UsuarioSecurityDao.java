package com.his.security.dao;

import com.his.model.MaeUsuario;

public interface UsuarioSecurityDao {

	public MaeUsuario findUser(String username);
}
