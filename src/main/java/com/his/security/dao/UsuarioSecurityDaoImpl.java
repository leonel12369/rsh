package com.his.security.dao;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.his.dao.impl.UsuarioDaoImpl;
import com.his.model.MaeUsuario;

@Repository
public class UsuarioSecurityDaoImpl implements UsuarioSecurityDao{

	private final Logger log= LoggerFactory.getLogger(UsuarioDaoImpl.class);
	
	@Autowired
	private EntityManager em;
	
	@Override
	public MaeUsuario findUser(String username) {
		// TODO Auto-generated method stub
		try {
			MaeUsuario usuario=(MaeUsuario) em.createQuery("from MaeUsuario where nombreUsuario =: username",MaeUsuario.class).setParameter("username", username).getSingleResult();
			return usuario;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR AL ENONTRAR USUARIO	");
			log.info(""+e);
			return null;
		}
	}
}
