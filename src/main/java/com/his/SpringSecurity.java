package com.his;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SpringSecurity  extends WebSecurityConfigurerAdapter{

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private com.his.security.service.UsuarioSecurityServiceImpl UsuarioSecurityServiceImpl;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/build/**","/images/**","/vendors/**","/views/**").permitAll()
		.antMatchers("/usuario/**").hasAnyRole("ADMIN")
		.antMatchers("/avance/**").hasAnyRole("ADMIN")
		.antMatchers("/maestro/**").hasAnyRole("ADMIN")
		.anyRequest().authenticated()
		.and()
		  .formLogin()
		        //.successHandler(successHandler)
		        .loginPage("/login")
		        .loginProcessingUrl("/login").defaultSuccessUrl("/calendario/index",true)
		    .permitAll()
		.and()
		.logout().permitAll()
		.and()
		.exceptionHandling().accessDeniedPage("/error_403");
		http.sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
	}
	
	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception
	{
		
		build
		.jdbcAuthentication()
		//.userDetailsService(UsuarioSecurityServiceImpl)
		.dataSource(dataSource)
		.passwordEncoder(passwordEncoder)
		.usersByUsernameQuery("select nombre_usuario, contrasenia, id_usuario  from mae_usuario where nombre_usuario=?")
		.authoritiesByUsernameQuery("select u.nombre_usuario, r.nombre from mae_rol r inner join mae_usuario u on (r.id_usuario=u.id_usuario) where u.nombre_usuario=?");
		
	}
}
