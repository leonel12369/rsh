package com.his.reporte;

import java.io.PrintWriter;
import java.util.List;

import com.his.model.ResErrores;

public interface CSVService {

	public void downloadCsv(PrintWriter writer, List<ResErrores> lista);
}
