package com.his.reporte;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.his.model.ResErrores;
import com.his.service.ErroresService;

@Service
public class ExcelServiceImpl implements ExcelService{

	@Autowired
	private ErroresService errorService; 
	
	@Override
	public ByteArrayInputStream exportExcelFiltros(List<ResErrores> lista) {
		// TODO Auto-generated method stub
		
		String [] columns= {"N#","Nombre del Personal","Apellido Paterno del Personal","Apellido Materno del Personal","Fecha Atencion","Lote","Num Pag","Num Reg","ERROR descripcion corta","ERROR descripcion completa","Programa de estrategia","Descripcion de estrategia","Deber ser "};
		Workbook workbook =new HSSFWorkbook();
		ByteArrayOutputStream stream =new ByteArrayOutputStream();
		Sheet sheet =workbook.createSheet("Atenciones");
		
		Row row=sheet.createRow(0);
		
		for(int i=0;i<columns.length;i++) {
			Cell cell =row.createCell(i);
			cell.setCellValue(columns[i]);
			
		}
		int initRow =1;
		int indice=1;
		for(ResErrores i: lista) {
			row=sheet.createRow(initRow);
			row.createCell(0).setCellValue(indice);
			row.createCell(1).setCellValue(i.getIdCita().getIdPersonal().getNombresPersonal());
			row.createCell(2).setCellValue(i.getIdCita().getIdPersonal().getApellidoPaternoPersonal());
			row.createCell(3).setCellValue(i.getIdCita().getIdPersonal().getApellidoMaternoPersonal());
			row.createCell(4).setCellValue(i.getIdCita().getFechaAtencion());
			row.createCell(5).setCellValue(i.getIdCita().getLote());
			row.createCell(6).setCellValue(i.getIdCita().getNumPag());
			row.createCell(7).setCellValue(i.getIdCita().getNumReg());
			
			List<ResErrores> lista_errores = errorService.listaErrores(i.idCita.getIdCita());
			int contador=0;
			for(ResErrores j: lista_errores) {
				row.createCell(0).setCellValue(indice++);
				row.createCell(1).setCellValue(i.getIdCita().getIdPersonal().getNombresPersonal());
				row.createCell(2).setCellValue(i.getIdCita().getIdPersonal().getApellidoPaternoPersonal());
				row.createCell(3).setCellValue(i.getIdCita().getIdPersonal().getApellidoMaternoPersonal());
				row.createCell(4).setCellValue(i.getIdCita().getFechaAtencion());
				row.createCell(5).setCellValue(i.getIdCita().getLote());
				row.createCell(6).setCellValue(i.getIdCita().getNumPag());
				row.createCell(7).setCellValue(i.getIdCita().getNumReg());
				row.createCell(8).setCellValue(j.getIdError().getDescripcionCorta());
				row.createCell(9).setCellValue(j.getIdError().getDescripcionLarga());
				row.createCell(10).setCellValue(j.getIdError().getIdEstrategia().getPrograma());
				row.createCell(11).setCellValue(j.getIdError().getIdEstrategia().getDescripcionEstrategia());
				row.createCell(12).setCellValue(j.getIdError().getSintaxis());
				//row.createCell(13).setCellValue(initRow);
				if(contador<lista_errores.size()-1) {
					initRow++;
					row=sheet.createRow(initRow);
					contador++;
				}
				
				
			}
			initRow++;
		}
		try {
			workbook.write(stream);
			workbook.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public ByteArrayInputStream exportExcelFiltrosUser(List<ResErrores> lista) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ByteArrayInputStream exportExcelFiltrosRegistrador(List<ResErrores> lista) {
		// TODO Auto-generated method stub
		return null;
	}

}
