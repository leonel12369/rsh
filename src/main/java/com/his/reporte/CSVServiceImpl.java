package com.his.reporte;

import java.io.PrintWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.model.ResErrores;
import com.his.service.ErroresService;

@Service
public class CSVServiceImpl implements CSVService{

	@Autowired
	private ErroresService errorService; 
	
	@Override
	public void downloadCsv(PrintWriter writer, List<ResErrores> lista) {
		// TODO Auto-generated method stub
		 writer.write("N#,Nombre del Personal,Apellido Paterno del Personal,Apellido Materno del Personal,Fecha Atencion,Lote,Num Pag,Num Reg,ERROR descripcion corta,ERROR descripcion completa,Programa de estrategia,Descripcion de estrategia,Deber ser \n");
		 int indice=1;
		 for(ResErrores i: lista) {
			 writer.write(indice +","+i.getIdCita().getIdPersonal().getNombresPersonal()+ "," + i.getIdCita().getIdPersonal().getApellidoPaternoPersonal() + "," +i.getIdCita().getIdPersonal().getApellidoMaternoPersonal() + "," 
		     + i.getIdCita().getFechaAtencion()+","+i.getIdCita().getLote()+","+i.getIdCita().getNumPag()+","+i.getIdCita().getNumReg()+",");
			 
			 List<ResErrores> lista_errores = errorService.listaErrores(i.idCita.getIdCita());
			 String coma="";
			 int contador=0;
			 for(ResErrores j: lista_errores) {
				 
				 if(contador!=0) {
					 	coma=","+","+","+","+","+","+","+",";
				 }
				 writer.write(coma+j.getIdError().getDescripcionCorta()+","+j.getIdError().getDescripcionLarga()+","+
						 j.getIdError().getIdEstrategia().getPrograma()+","+
						 j.getIdError().getIdEstrategia().getDescripcionEstrategia()+","+
						 j.getIdError().getSintaxis()+"\n");
				 contador++;
			 }
			 indice=indice+1;
			 //writer.write("\n");
		 }
	}

}
