package com.his.reporte;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.his.model.ResErrores;


public interface ExcelService {

	ByteArrayInputStream exportExcelFiltros(List<ResErrores> lista);
	
	ByteArrayInputStream exportExcelFiltrosUser(List<ResErrores> lista);
	
	ByteArrayInputStream exportExcelFiltrosRegistrador(List<ResErrores> lista);
}
