package com.his.service;

import java.util.List;

import com.his.model.MaeEqhali;
import com.his.model.MaeErrores;
import com.his.model.MaeEstrategiaSanitaria;

public interface MaestroService {
	
	public List<MaeEqhali> listarEqhali();

	public List<MaeErrores> listarError();
	
	public List<MaeEstrategiaSanitaria> listarEstrategia();
	
	public void save_maeEqhali(MaeEqhali maeEqhali);
	
	public void eliminar_maeEqhali(MaeEqhali maeEqhali);
	
	public MaeEqhali buscar_maeEqhali(int id);
	
	public void save_estrategia(MaeEstrategiaSanitaria estrategia);
	
	public void eliminar_estrategia(MaeEstrategiaSanitaria estrategia);
	
	public MaeEstrategiaSanitaria buscar_estrategia(int id);
	
	public void save_maeError(MaeErrores error);
	
	public void eliminar_maeError(MaeErrores error);
	
	public MaeErrores buscar_maeError(String id);
	
	public void editar_maeError(MaeErrores error);
	
	public String idError(int idEstrategia);
}
