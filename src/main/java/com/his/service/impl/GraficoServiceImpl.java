package com.his.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.GraficoDao;
import com.his.model.Grafico;
import com.his.service.GraficoService;

@Service
public class GraficoServiceImpl implements GraficoService{
	
	@Autowired
	private GraficoDao graficoDao;

	@Override
	public List<Grafico> mesError() {
		// TODO Auto-generated method stub
		return graficoDao.mesError();
	}

	@Override
	public List<Grafico> erroresComunes() {
		// TODO Auto-generated method stub
		return graficoDao.erroresComunes();
	}

	@Override
	public List<Object[]> errorMicroRed(String anio, int mes,int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		return graficoDao.errorMicroRed(anio, mes,fmes ,idEstrategia);
	}

	@Override
	public List<Grafico> errorEstablecimiento(String codMicroRed,String anio, int mes,int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		return graficoDao.errorEstablecimiento(codMicroRed, anio, mes,fmes, idEstrategia);
	}

	@Override
	public List<Grafico> errorMayorEstablecimiento() {
		// TODO Auto-generated method stub
		return graficoDao.errorMayorEstablecimiento();
	}

	@Override
	public List<Object[]> cantidadErrorMicrored(String codMicroRed,String anio, int mes,int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		return graficoDao.cantidadErrorMicrored(codMicroRed, anio, mes,fmes, idEstrategia);
	}

	@Override
	public List<Object[]> errorComunMicrored(String codMicroRed,String anio, int mes,int fmes, int idEstrategia) {
		// TODO Auto-generated method stub
		return graficoDao.errorComunMicrored(codMicroRed, anio, mes,fmes, idEstrategia);
	}

	@Override
	public List<Object[]> avanceGraficoDetalleEstablecimiento(String codMicroRed,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.avanceGraficoDetalleEstablecimiento(codMicroRed, anio, mesI, mesF);
	}

	@Override
	public int cantidadAvance_RestanteMicrored(String codMicroRed,Boolean estado,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.cantidadAvance_RestanteMicrored(codMicroRed, estado, anio, mesI, mesF);
	}

	@Override
	public int cantidadAvance_RestanteEstablecimiento(String establecimiento, Boolean estado,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.cantidadAvance_RestanteEstablecimiento(establecimiento, estado, anio, mesI, mesF);
	}

	@Override
	public int cantidadAvance_RestanteMicrored2(String codMicroRed, Boolean estado, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.cantidadAvance_RestanteMicrored2(codMicroRed, estado, anio, mesI, mesF);
	}

	@Override
	public int cantidadAvance_RestanteRed(String red, Boolean estado, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.cantidadAvance_RestanteRed(red, estado, anio, mesI, mesF);
	}

	@Override
	public List<Object[]> avanceGraficoDetalleMicroRed(String codMicroRed, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.avanceGraficoDetalleMicroRed(codMicroRed, anio, mesI, mesF);
	}

	@Override
	public List<Object[]> avanceGraficoDetalleREd(String codRed, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return graficoDao.avanceGraficoDetalleREd(codRed, anio, mesI, mesF);
	}
	
	

}
