package com.his.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.CitaDao;
import com.his.model.MaeCitas;
import com.his.service.CitaService;

@Service
public class CitaServiceImpl implements CitaService{

	@Autowired
	private CitaDao citaDao;
	
	
	@Override
	public MaeCitas busquedaCita(String idCita) {
		// TODO Auto-generated method stub
		return citaDao.busquedaCita(idCita);
	}

	
}
