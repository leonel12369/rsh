package com.his.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.GenericDao;
import com.his.model.Generic;
import com.his.service.GenericService;

@Service
public class GenericServiceImpl implements GenericService{

	@Autowired
	private GenericDao genericDao;
	
	
	@Override
	public List<Generic> avance_microred(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.avance_microred(anio, mesI, mesF);
	}


	@Override
	public List<Object[]> total_microred(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.total_microred(anio, mesI, mesF);
	}


	@Override
	public Generic datos_microred(String codigoMicrored) {
		// TODO Auto-generated method stub
		return genericDao.datos_microred(codigoMicrored);
	}


	@Override
	public List<Generic> avance_personal(String establecimiento,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.avance_personal(establecimiento, anio, mesI, mesF);
	}


	@Override
	public Generic datos_Establecimiento(String Establecimiento) {
		// TODO Auto-generated method stub
		return genericDao.datos_Establecimiento(Establecimiento);
	}


	@Override
	public List<Object[]> ranking(String establecimiento, Boolean estado,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.ranking(establecimiento, estado, anio, mesI, mesF);
	}


	@Override
	public List<Generic> detalleMicroRed_Establecimientos(String establecimiento,String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.detalleMicroRed_Establecimientos(establecimiento, anio, mesI, mesF);
	}


	@Override
	public Generic nombres_resultados(String tabla, String dni) {
		// TODO Auto-generated method stub
		return genericDao.nombres_resultados(tabla, dni);
	}


	@Override
	public List<Generic> no_avance(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.no_avance(anio, mesI, mesF);
	}


	@Override
	public List<Generic> avance_est_detMicrocred(String idMicrored, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.avance_est_detMicrocred(idMicrored, anio, mesI, mesF);
	}


	@Override
	public List<Generic> no_avance_personal(String establecimiento, String anio, int mesI, int mesF,int terminado) {
		// TODO Auto-generated method stub
		return genericDao.no_avance_personal(establecimiento, anio, mesI, mesF, terminado);
	}


	@Override
	public List<Generic> no_avance_microred(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.no_avance_microred(anio, mesI, mesF);
	}


	@Override
	public Generic datos_Redes() {
		// TODO Auto-generated method stub
		return genericDao.datos_Redes();
	}


	@Override
	public Generic datos_microredes(String codigoRed) {
		// TODO Auto-generated method stub
		return genericDao.datos_microredes(codigoRed);
	}


	@Override
	public List<Generic> avance_redes(String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.avance_redes(anio, mesI, mesF);
	}


	@Override
	public List<Generic> no_avance_redes(String anio, int mesI, int mesF, int terminado) {
		// TODO Auto-generated method stub
		return genericDao.no_avance_redes(anio, mesI, mesF, terminado);
	}


	@Override
	public List<Generic> avance_microRedes(String Codigo_red, String anio, int mesI, int mesF) {
		// TODO Auto-generated method stub
		return genericDao.avance_microRedes(Codigo_red, anio, mesI, mesF);
	}


	@Override
	public List<Generic> no_avance_microRedes(String Codigo_red, String anio, int mesI, int mesF, int terminado) {
		// TODO Auto-generated method stub
		return genericDao.no_avance_microRedes(Codigo_red, anio, mesI, mesF, terminado);
	}

}
