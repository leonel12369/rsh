package com.his.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.his.dao.NominalTramaDao;
import com.his.model.NominalTramaNuevo;
import com.his.service.NominalTramaService;

@Service
public class NominlaTramaServiceImpl implements NominalTramaService{

	@Autowired
	private NominalTramaDao nominalTramaDao;
	
	
	@Override
	public Page<NominalTramaNuevo> listar(int idEstablecimiento, String anio, String mes, Pageable pageable) {
		// TODO Auto-generated method stub
		return nominalTramaDao.listar(idEstablecimiento, anio, mes, pageable);
	}
	
	

}
