package com.his.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.MaestroDao;
import com.his.model.MaeEqhali;
import com.his.model.MaeErrores;
import com.his.model.MaeEstrategiaSanitaria;
import com.his.service.MaestroService;

@Service
public class MaestroServiceImpl implements MaestroService {

	@Autowired
	private MaestroDao maestroDao;

	@Override
	public List<MaeErrores> listarError() {
		// TODO Auto-generated method stub
		return maestroDao.listarError();
	}

	@Override
	public List<MaeEstrategiaSanitaria> listarEstrategia() {
		// TODO Auto-generated method stub
		return maestroDao.listarEstrategia();
	}

	@Override
	public List<MaeEqhali> listarEqhali() {
		// TODO Auto-generated method stub
		return maestroDao.listarEqhali();
	}

	@Transactional
	@Override
	public void save_maeEqhali(MaeEqhali maeEqhali) {
		// TODO Auto-generated method stub
		maestroDao.save_maeEqhali(maeEqhali);
	}

	@Transactional
	@Override
	public void eliminar_maeEqhali(MaeEqhali maeEqhali) {
		// TODO Auto-generated method stub
		maestroDao.eliminar_maeEqhali(maeEqhali);
	}

	@Override
	public MaeEqhali buscar_maeEqhali(int id) {
		// TODO Auto-generated method stub
		return maestroDao.buscar_maeEqhali(id);
	}

	@Transactional
	@Override
	public void save_estrategia(MaeEstrategiaSanitaria estrategia) {
		// TODO Auto-generated method stub
		maestroDao.save_estrategia(estrategia);
	}

	@Transactional
	@Override
	public void eliminar_estrategia(MaeEstrategiaSanitaria estrategia) {
		// TODO Auto-generated method stub
		maestroDao.eliminar_estrategia(estrategia);
	}

	@Override
	public MaeEstrategiaSanitaria buscar_estrategia(int id) {
		// TODO Auto-generated method stub
		return maestroDao.buscar_estrategia(id);
	}

	@Transactional
	@Override
	public void save_maeError(MaeErrores error) {
		// TODO Auto-generated method stub
		maestroDao.save_maeError(error);
	}
	
	@Transactional
	@Override
	public void eliminar_maeError(MaeErrores error) {
		// TODO Auto-generated method stub
		maestroDao.eliminar_maeError(error);
	}

	@Override
	public MaeErrores buscar_maeError(String id) {
		// TODO Auto-generated method stub
		return maestroDao.buscar_maeError(id);
	}

	@Transactional
	@Override
	public void editar_maeError(MaeErrores error) {
		// TODO Auto-generated method stub
		maestroDao.editar_maeError(error);
	}

	@Override
	public String idError(int idEstrategia) {
		// TODO Auto-generated method stub
		return maestroDao.idError(idEstrategia);
	}
	
	
	
}
