package com.his.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.his.dao.ErroresDao;
import com.his.model.MaeErrores;
import com.his.model.ResErrores;
import com.his.service.ErroresService;

@Service
public class ErroresServiceImpl implements ErroresService{

	@Autowired
	private ErroresDao erroresDao;
	
	@Override
	public Page<ResErrores> listarTodo(Pageable pageable) {
		// TODO Auto-generated method stub
		return erroresDao.listarTodo(pageable);
	}

	@Override
	public Page<ResErrores> erroresFiltrados(Boolean estado,int tipoRegistrador,int idPrograma,int idEstablecimiento, String anio, String mes, Pageable pageable) {
		// TODO Auto-generated method stub
		return erroresDao.erroresFiltrados(estado,tipoRegistrador,idPrograma,idEstablecimiento, anio, mes, pageable);
	}

	@Override
	public List<ResErrores> listaErrores(String idCita) {
		// TODO Auto-generated method stub
		return erroresDao.listaErrores(idCita);
	}

	@Override
	public Page<ResErrores> erroresFiltradosUser(Boolean estado,int tipoRegistrador,int idPrograma,String anio, String mes, String dni, Pageable pageable) {
		// TODO Auto-generated method stub
		return erroresDao.erroresFiltradosUser(estado,tipoRegistrador,idPrograma,anio, mes, dni, pageable);
	}

	@Override
	public Page<ResErrores> erroresFiltradosPersonal(Boolean estado,int idPrograma, String anio, String mes, String dni,
			Pageable pageable) {
		// TODO Auto-generated method stub
		return erroresDao.erroresFiltradosPersonal(estado,idPrograma, anio, mes, dni, pageable);
	}

	@Override
	public List<ResErrores> listaErroresFiltrados(Boolean estado,int tipoRegistrador, int idPrograma, int idEstablecimiento,
			String anio, String mes) {
		// TODO Auto-generated method stub
		return erroresDao.listaErroresFiltrados(estado,tipoRegistrador, idPrograma, idEstablecimiento, anio, mes);
	}

	@Override
	public List<ResErrores> listaErroresFiltradosUser(Boolean estado,int tipoRegistrador, int idPrograma, String anio, String mes,
			String dni) {
		// TODO Auto-generated method stub
		return erroresDao.listaErroresFiltradosUser(estado,tipoRegistrador, idPrograma, anio, mes, dni);
	}

	@Override
	public List<ResErrores> listaErroresFiltradosPersonal(Boolean estado,int idPrograma, String anio, String mes, String dni) {
		// TODO Auto-generated method stub
		return erroresDao.listaErroresFiltradosPersonal(estado,idPrograma, anio, mes, dni);
	}

	@Transactional
	@Override
	public int editar(ResErrores resErrores) {
		// TODO Auto-generated method stub
		return erroresDao.editar(resErrores);
	}

	@Override
	public ResErrores buscar(int idResErrores) {
		// TODO Auto-generated method stub
		return erroresDao.buscar(idResErrores);
	}

	@Override
	public Page<ResErrores> erroresFiltradosPaciente(Boolean estado, String anio, String mes, String dni,
			Pageable pageable) {
		// TODO Auto-generated method stub
		return erroresDao.erroresFiltradosPaciente(estado, anio, mes, dni, pageable);
	}

	@Override
	public List<ResErrores> listaErroresFiltradosPaciente(Boolean estado, String anio, String mes, String dni) {
		// TODO Auto-generated method stub
		return erroresDao.listaErroresFiltradosPaciente(estado, anio, mes, dni);
	}


}
