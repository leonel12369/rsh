package com.his.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.UsuarioDao;
import com.his.model.MaeRol;
import com.his.model.MaeUsuario;
import com.his.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao; 
	
	@Transactional
	@Override
	public void save(MaeUsuario usuario) {
		// TODO Auto-generated method stub
		usuarioDao.save(usuario);
	}

	@Transactional
	@Override
	public void saveRol(MaeRol rol) {
		// TODO Auto-generated method stub
		usuarioDao.saveRol(rol);
	}

	@Override
	public int findUsername(String username) {
		// TODO Auto-generated method stub
		return usuarioDao.findUsername(username);
	}

	@Transactional
	@Override
	public void deleteRol(int idUsuario) {
		// TODO Auto-generated method stub
		usuarioDao.deleteRol(idUsuario);
	}

	@Transactional
	@Override
	public void deleteUsuario(int idUsuario) {
		// TODO Auto-generated method stub
		usuarioDao.deleteUsuario(idUsuario);
	}

	@Override
	public List<MaeRol> adminOrUser(String nombreRol) {
		// TODO Auto-generated method stub
		return usuarioDao.adminOrUser(nombreRol);
	}

	@Override
	public List<MaeRol> adminAndUser() {
		// TODO Auto-generated method stub
		return usuarioDao.adminAndUser();
	}

	@Override
	public List<String> listaStringRol(int id) {
		// TODO Auto-generated method stub
		return usuarioDao.listaStringRol(id);
	}

	@Override
	public MaeUsuario findUsuario(int id) {
		// TODO Auto-generated method stub
		return usuarioDao.findUsuario(id);
	}

	@Override
	public List<MaeRol> findRolUsuario(int id) {
		// TODO Auto-generated method stub
		return usuarioDao.findRolUsuario(id);
	}

	@Transactional
	@Override
	public void deleteRolEspecifico(String rol, int idUsuario) {
		// TODO Auto-generated method stub
		usuarioDao.deleteRolEspecifico(rol, idUsuario);
	}

	@Override
	public MaeRol findRolUsuarioEspecifico(String rol, int idUsuario) {
		// TODO Auto-generated method stub
		return usuarioDao.findRolUsuarioEspecifico(rol, idUsuario);
	}

}
