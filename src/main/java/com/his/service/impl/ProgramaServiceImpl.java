package com.his.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.ProgramaDao;
import com.his.model.MaeEstrategiaSanitaria;
import com.his.service.ProgramaService;

@Service
public class ProgramaServiceImpl implements ProgramaService{

	@Autowired
	public ProgramaDao programaDao;

	@Override
	public List<MaeEstrategiaSanitaria> listar() {
		// TODO Auto-generated method stub
		return programaDao.listar();
	}

}
