package com.his.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.UbigeoDao;
import com.his.model.MaestroHisUbigeoIneiReniec;
import com.his.model.Ubigeo;
import com.his.service.UbigeoService;

@Service
public class UbigeoServiceImpl implements UbigeoService{

	@Autowired
	private UbigeoDao ubigeoDao;
	
	@Override
	public List<Ubigeo> departamento() {
		// TODO Auto-generated method stub
		return ubigeoDao.departamento();
	}

	@Override
	public List<Ubigeo> provincia(String codDepartamento) {
		// TODO Auto-generated method stub
		return ubigeoDao.provincia(codDepartamento);
	}

	@Override
	public List<Ubigeo> distrito(String codDepa,String codProv) {
		// TODO Auto-generated method stub
		return ubigeoDao.distrito(codDepa, codProv);
	}

	@Override
	public List<Ubigeo> red(int codDisa) {
		// TODO Auto-generated method stub
		return ubigeoDao.red(codDisa);
	}

	@Override
	public List<Ubigeo> microRed(int codDisa,String codRed) {
		// TODO Auto-generated method stub
		return ubigeoDao.microRed(codDisa, codRed);
	}

	@Override
	public List<Ubigeo> establecimiento(int codDisa,String codRed, String codMicroRed) {
		// TODO Auto-generated method stub
		return ubigeoDao.establecimiento(codDisa, codRed, codMicroRed);
	}

	@Override
	public List<Ubigeo> disa() {
		// TODO Auto-generated method stub
		return ubigeoDao.disa();
	}

	
}
