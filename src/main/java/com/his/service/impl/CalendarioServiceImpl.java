package com.his.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dao.CalendarioDao;
import com.his.model.MaeCalendario;
import com.his.service.CalendarioService;

@Service
public class CalendarioServiceImpl implements CalendarioService{

	@Autowired
	private CalendarioDao calendarioDao;

	@Override
	public List<MaeCalendario> listar() {
		// TODO Auto-generated method stub
		return calendarioDao.listar();
	}

	@Transactional
	@Override
	public void agregar(MaeCalendario calendario) {
		// TODO Auto-generated method stub
		calendarioDao.agregar(calendario);
	}

	@Transactional
	@Override
	public void eliminar(MaeCalendario calendario) {
		// TODO Auto-generated method stub
		calendarioDao.eliminar(calendario);
	}

	@Override
	public MaeCalendario buscar(int id) {
		// TODO Auto-generated method stub
		return calendarioDao.buscar(id);
	}

	@Override
	public List<MaeCalendario> buscar_mes_anio(int mes,int anio) {
		// TODO Auto-generated method stub
		return calendarioDao.buscar_mes_anio(mes, anio);
	}
	
	
}
