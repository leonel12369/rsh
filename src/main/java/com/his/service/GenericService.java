package com.his.service;

import java.util.List;

import com.his.model.Generic;

public interface GenericService {

	public List<Generic> avance_microred(String anio, int mesI, int mesF);
	
	public List<Object[]> total_microred(String anio, int mesI, int mesF);
	
	public Generic datos_microred(String codigoMicrored);
	
	public List<Generic> avance_personal(String establecimiento,String anio, int mesI, int mesF);
	
	public List<Object[]> ranking(String establecimiento,Boolean estado,String anio, int mesI, int mesF);
	
	public Generic datos_Establecimiento(String Establecimiento);
	
	public List<Generic> detalleMicroRed_Establecimientos(String establecimiento,String anio, int mesI, int mesF);
	
	public Generic nombres_resultados(String tabla,String dni);
	
	public List<Generic> no_avance(String anio, int mesI, int mesF);
	
	public List<Generic> avance_est_detMicrocred(String idMicrored,String anio, int mesI, int mesF);
	
	public List<Generic> no_avance_personal(String establecimiento,String anio, int mesI, int mesF,int terminado);
	
	public List<Generic> no_avance_microred(String anio, int mesI, int mesF);
	
	public Generic datos_Redes();
	
	public Generic datos_microredes(String codigoRed);
	
	
	public List<Generic> avance_redes(String anio, int mesI, int mesF);
	
	public List<Generic> no_avance_redes(String anio, int mesI, int mesF,int terminado);
	
	
	public List<Generic> avance_microRedes(String Codigo_red,String anio, int mesI, int mesF);
	
	public List<Generic> no_avance_microRedes(String Codigo_red,String anio, int mesI, int mesF,int terminado);
}
