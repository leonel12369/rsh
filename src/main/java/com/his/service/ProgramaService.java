package com.his.service;

import java.util.List;

import com.his.model.MaeEstrategiaSanitaria;

public interface ProgramaService {

	public List<MaeEstrategiaSanitaria> listar();
}
