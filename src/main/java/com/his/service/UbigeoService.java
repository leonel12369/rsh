package com.his.service;

import java.util.List;

import com.his.model.MaestroHisUbigeoIneiReniec;
import com.his.model.Ubigeo;

public interface UbigeoService {

	public List<Ubigeo> departamento();
	
	public List<Ubigeo> provincia(String codDepartamento);
	
	public List<Ubigeo> distrito(String codDepa,String codProv);
	
	//--------------------------------------------------------
	public List<Ubigeo> disa();
	
	public List<Ubigeo> red(int codDisa);
	
	public List<Ubigeo> microRed(int codDisa,String codRed);	
	
	public List<Ubigeo> establecimiento(int codDisa,String codRed, String codMicroRed);	
}
