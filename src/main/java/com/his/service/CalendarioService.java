package com.his.service;

import java.util.List;

import com.his.model.MaeCalendario;

public interface CalendarioService {

	public List<MaeCalendario> listar ();
	
	public void agregar(MaeCalendario calendario);
	
	public void eliminar(MaeCalendario calendario);
	
	public MaeCalendario buscar(int id);
	
	public List<MaeCalendario> buscar_mes_anio(int mes,int anio);
}
