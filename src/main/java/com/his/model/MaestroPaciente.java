package com.his.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MAESTRO_PACIENTE")
public class MaestroPaciente  implements Serializable{
	
	private String idPaciente;
	private MaestroHisTipoDoc idTipoDocumentoPaciente;
	private String numeroDocumentoPaciente;
	private String apellidoPaternoPaciente;
	private String apellidoMaternoPaciente;
	private String nombresPaciente;
	private Date fechaNacimientoPaciente;
	private String genero;
	private MaestroHisEtnia idEtnia;//datos desigual
	private String historiaClinica;
	private String fichaFamiliar;
	private String ubigeoNacimiento;
	private String ubigeoReniec;
	private String domicilioReniec;
	private MaestroHisUbigeoIneiReniec ubigeoDeclarado;
	private String domicilioDeclarado;
	private String referenciaDomicilio;
	private MaestroHisPais idPais;
	private MaestroHisEstablecimiento idEstablecimiento;
	private Date fechaAlta;
	private Date fechaModificacion;
	
	public MaestroPaciente(String idPaciente, MaestroHisTipoDoc idTipoDocumentoPaciente, String numeroDocumentoPaciente,
			String apellidoPaternoPaciente, String apellidoMaternoPaciente, String nombresPaciente,
			Date fechaNacimientoPaciente, String genero, MaestroHisEtnia idEtnia, String historiaClinica, String fichaFamiliar,
			String ubigeoNacimiento, String ubigeoReniec, String domicilioReniec, MaestroHisUbigeoIneiReniec ubigeoDeclarado,
			String domicilioDeclarado, String referenciaDomicilio, MaestroHisPais idPais, MaestroHisEstablecimiento idEstablecimiento, Date fechaAlta,
			Date fechaModificacion) {
		super();
		this.idPaciente = idPaciente;
		this.idTipoDocumentoPaciente = idTipoDocumentoPaciente;
		this.numeroDocumentoPaciente = numeroDocumentoPaciente;
		this.apellidoPaternoPaciente = apellidoPaternoPaciente;
		this.apellidoMaternoPaciente = apellidoMaternoPaciente;
		this.nombresPaciente = nombresPaciente;
		this.fechaNacimientoPaciente = fechaNacimientoPaciente;
		this.genero = genero;
		this.idEtnia = idEtnia;
		this.historiaClinica = historiaClinica;
		this.fichaFamiliar = fichaFamiliar;
		this.ubigeoNacimiento = ubigeoNacimiento;
		this.ubigeoReniec = ubigeoReniec;
		this.domicilioReniec = domicilioReniec;
		this.ubigeoDeclarado = ubigeoDeclarado;
		this.domicilioDeclarado = domicilioDeclarado;
		this.referenciaDomicilio = referenciaDomicilio;
		this.idPais = idPais;
		this.idEstablecimiento = idEstablecimiento;
		this.fechaAlta = fechaAlta;
		this.fechaModificacion = fechaModificacion;
	}
	
	public MaestroPaciente() {}

	@Id
	@Column(name="Id_Paciente",length = 50)
	public String getIdPaciente() {
		return idPaciente;
	}

	//@Column(name="Id_Tipo_Documento_Paciente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Tipo_Documento_Paciente")
	public MaestroHisTipoDoc getIdTipoDocumentoPaciente() {
		return idTipoDocumentoPaciente;
	}

	@Column(name="Numero_Documento_Paciente",length = 15)
	public String getNumeroDocumentoPaciente() {
		return numeroDocumentoPaciente;
	}

	@Column(name="Apellido_Paterno_Paciente",length = 50)
	public String getApellidoPaternoPaciente() {
		return apellidoPaternoPaciente;
	}

	@Column(name="Apellido_Materno_Paciente",length = 50)
	public String getApellidoMaternoPaciente() {
		return apellidoMaternoPaciente;
	}

	@Column(name="Nombres_Paciente",length = 150)
	public String getNombresPaciente() {
		return nombresPaciente;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Nacimiento_Paciente", length = 23)
	public Date getFechaNacimientoPaciente() {
		return fechaNacimientoPaciente;
	}

	@Column(name="Genero",length = 1)
	public String getGenero() {
		return genero;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Etnia")
	//@Column(name="Id_Etnia",length = 2)
	public MaestroHisEtnia getIdEtnia() {
		return idEtnia;
	}

	@Column(name="Historia_Clinica",length = 15)
	public String getHistoriaClinica() {
		return historiaClinica;
	}

	@Column(name="Ficha_Familiar",length = 15)
	public String getFichaFamiliar() {
		return fichaFamiliar;
	}

	@Column(name="Ubigeo_Nacimiento",length = 6)
	public String getUbigeoNacimiento() {
		return ubigeoNacimiento;
	}

	@Column(name="Ubigeo_Reniec",length = 6)
	public String getUbigeoReniec() {
		return ubigeoReniec;
	}

	@Column(name="Domicilio_Reniec",length = 250)
	public String getDomicilioReniec() {
		return domicilioReniec;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Ubigeo_Declarado")
	//@Column(name="Ubigeo_Declarado",length = 6)
	public MaestroHisUbigeoIneiReniec getUbigeoDeclarado() {
		return ubigeoDeclarado;
	}

	@Column(name="Domicilio_Declarado",length = 250)
	public String getDomicilioDeclarado() {
		return domicilioDeclarado;
	}

	@Column(name="Referencia_Domicilio",length = 500)
	public String getReferenciaDomicilio() {
		return referenciaDomicilio;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Pais")
	//@Column(name="Id_Pais",length = 3)
	public MaestroHisPais getIdPais() {
		return idPais;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Establecimiento")
	//@Column(name="Id_Establecimiento")
	public MaestroHisEstablecimiento getIdEstablecimiento() {
		return idEstablecimiento;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Alta", length = 23)
	public Date getFechaAlta() {
		return fechaAlta;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Modificacion", length = 23)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setIdPaciente(String idPaciente) {
		this.idPaciente = idPaciente;
	}

	public void setIdTipoDocumentoPaciente(MaestroHisTipoDoc idTipoDocumentoPaciente) {
		this.idTipoDocumentoPaciente = idTipoDocumentoPaciente;
	}

	public void setNumeroDocumentoPaciente(String numeroDocumentoPaciente) {
		this.numeroDocumentoPaciente = numeroDocumentoPaciente;
	}

	public void setApellidoPaternoPaciente(String apellidoPaternoPaciente) {
		this.apellidoPaternoPaciente = apellidoPaternoPaciente;
	}

	public void setApellidoMaternoPaciente(String apellidoMaternoPaciente) {
		this.apellidoMaternoPaciente = apellidoMaternoPaciente;
	}

	public void setNombresPaciente(String nombresPaciente) {
		this.nombresPaciente = nombresPaciente;
	}

	public void setFechaNacimientoPaciente(Date fechaNacimientoPaciente) {
		this.fechaNacimientoPaciente = fechaNacimientoPaciente;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setIdEtnia(MaestroHisEtnia idEtnia) {
		this.idEtnia = idEtnia;
	}

	public void setHistoriaClinica(String historiaClinica) {
		this.historiaClinica = historiaClinica;
	}

	public void setFichaFamiliar(String fichaFamiliar) {
		this.fichaFamiliar = fichaFamiliar;
	}

	public void setUbigeoNacimiento(String ubigeoNacimiento) {
		this.ubigeoNacimiento = ubigeoNacimiento;
	}

	public void setUbigeoReniec(String ubigeoReniec) {
		this.ubigeoReniec = ubigeoReniec;
	}

	public void setDomicilioReniec(String domicilioReniec) {
		this.domicilioReniec = domicilioReniec;
	}

	public void setUbigeoDeclarado(MaestroHisUbigeoIneiReniec ubigeoDeclarado) {
		this.ubigeoDeclarado = ubigeoDeclarado;
	}

	public void setDomicilioDeclarado(String domicilioDeclarado) {
		this.domicilioDeclarado = domicilioDeclarado;
	}

	public void setReferenciaDomicilio(String referenciaDomicilio) {
		this.referenciaDomicilio = referenciaDomicilio;
	}

	public void setIdPais(MaestroHisPais idPais) {
		this.idPais = idPais;
	}

	public void setIdEstablecimiento(MaestroHisEstablecimiento idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	
}

