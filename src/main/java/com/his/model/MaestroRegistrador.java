package com.his.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MAESTRO_REGISTRADOR")
public class MaestroRegistrador implements Serializable{

	private String idRegistrador;
	private MaestroHisTipoDoc idTipoDocumentoRegistrador;
	private String numeroDocumentoRegistrador;
	private String apellidoPaternoRegistrador;
	private String apellidoMaternoRegistrador;
	private String nombresRegistrador;
	private Date fechaNacimientoRegistrador;
	
	public MaestroRegistrador(String idRegistrador, MaestroHisTipoDoc idTipoDocumentoRegistrador, String numeroDocumentoRegistrador,
			String apellidoPaternoRegistrador, String apellidoMaternoRegistrador, String nombresRegistrador,
			Date fechaNacimientoRegistrador) {
		super();
		this.idRegistrador = idRegistrador;
		this.idTipoDocumentoRegistrador = idTipoDocumentoRegistrador;
		this.numeroDocumentoRegistrador = numeroDocumentoRegistrador;
		this.apellidoPaternoRegistrador = apellidoPaternoRegistrador;
		this.apellidoMaternoRegistrador = apellidoMaternoRegistrador;
		this.nombresRegistrador = nombresRegistrador;
		this.fechaNacimientoRegistrador = fechaNacimientoRegistrador;
	}
	
	public MaestroRegistrador() {}

	@Id
	@Column(name="Id_Registrador",length = 50)
	public String getIdRegistrador() {
		return idRegistrador;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Tipo_Documento_Registrador")
	//@Column(name="Id_Tipo_Documento_Registrador")
	public MaestroHisTipoDoc getIdTipoDocumentoRegistrador() {
		return idTipoDocumentoRegistrador;
	}

	@Column(name="Numero_Documento_Registrador",length = 15)
	public String getNumeroDocumentoRegistrador() {
		return numeroDocumentoRegistrador;
	}

	@Column(name="Apellido_Paterno_Registrador",length = 50)
	public String getApellidoPaternoRegistrador() {
		return apellidoPaternoRegistrador;
	}
	
	@Column(name="Apellido_Materno_Registrador",length = 50)
	public String getApellidoMaternoRegistrador() {
		return apellidoMaternoRegistrador;
	}

	@Column(name="Nombres_Registrador",length = 150)
	public String getNombresRegistrador() {
		return nombresRegistrador;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Nacimiento_Registrador", length = 23)
	public Date getFechaNacimientoRegistrador() {
		return fechaNacimientoRegistrador;
	}

	public void setIdRegistrador(String idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public void setIdTipoDocumentoRegistrador(MaestroHisTipoDoc idTipoDocumentoRegistrador) {
		this.idTipoDocumentoRegistrador = idTipoDocumentoRegistrador;
	}

	public void setNumeroDocumentoRegistrador(String numeroDocumentoRegistrador) {
		this.numeroDocumentoRegistrador = numeroDocumentoRegistrador;
	}

	public void setApellidoPaternoRegistrador(String apellidoPaternoRegistrador) {
		this.apellidoPaternoRegistrador = apellidoPaternoRegistrador;
	}

	public void setApellidoMaternoRegistrador(String apellidoMaternoRegistrador) {
		this.apellidoMaternoRegistrador = apellidoMaternoRegistrador;
	}

	public void setNombresRegistrador(String nombresRegistrador) {
		this.nombresRegistrador = nombresRegistrador;
	}

	public void setFechaNacimientoRegistrador(Date fechaNacimientoRegistrador) {
		this.fechaNacimientoRegistrador = fechaNacimientoRegistrador;
	}
	
	
	
}
