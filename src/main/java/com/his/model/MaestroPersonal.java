package com.his.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="MAESTRO_PERSONAL")
public class MaestroPersonal implements Serializable{

	private String idPersonal;
	private MaestroHisTipoDoc idTipoDocumentoPersonal;
	private String numeroDocumentoPersonal;
	private String apellidoPaternoPersonal;
	private String apellidoMaternoPersonal;
	private String nombresPersonal;
	private Date fechaNacimientoPersonal;
	private MaestroHisCondicionContrato idCondicion;///datos desiguales bloc de notas
	private MaestroHisProfesion idProfesion;
	private MaestroHisColegio idColegio;
	private String numeroColegiatura;
	private MaestroHisEstablecimiento idEstablecimiento;
	private Date fechaAlta;
	private Date fechaBaja;
	
	public MaestroPersonal(String idPersonal, MaestroHisTipoDoc idTipoDocumentoPersonal, String numeroDocumentoPersonal,
			String apellidoPaternoPersonal, String apellidoMaternoPersonal, String nombresPersonal,
			Date fechaNacimientoPersonal, MaestroHisCondicionContrato idCondicion, MaestroHisProfesion idProfesion, MaestroHisColegio idColegio,
			String numeroColegiatura, MaestroHisEstablecimiento idEstablecimiento, Date fechaAlta, Date fechaBaja) {
		super();
		this.idPersonal = idPersonal;
		this.idTipoDocumentoPersonal = idTipoDocumentoPersonal;
		this.numeroDocumentoPersonal = numeroDocumentoPersonal;
		this.apellidoPaternoPersonal = apellidoPaternoPersonal;
		this.apellidoMaternoPersonal = apellidoMaternoPersonal;
		this.nombresPersonal = nombresPersonal;
		this.fechaNacimientoPersonal = fechaNacimientoPersonal;
		this.idCondicion = idCondicion;
		this.idProfesion = idProfesion;
		this.idColegio = idColegio;
		this.numeroColegiatura = numeroColegiatura;
		this.idEstablecimiento = idEstablecimiento;
		this.fechaAlta = fechaAlta;
		this.fechaBaja = fechaBaja;
	}
	
	public MaestroPersonal() {}

	@Id
	@Column(name="Id_Personal",length = 50)
	public String getIdPersonal() {
		return idPersonal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Tipo_Documento_Personal")
	//@Column(name="Id_Tipo_Documento_Personal")
	public MaestroHisTipoDoc getIdTipoDocumentoPersonal() {
		return idTipoDocumentoPersonal;
	}

	@Column(name="Numero_Documento_Personal",length = 15)
	public String getNumeroDocumentoPersonal() {
		return numeroDocumentoPersonal;
	}

	@Column(name="Apellido_Paterno_Personal",length = 50)
	public String getApellidoPaternoPersonal() {
		return apellidoPaternoPersonal;
	}

	@Column(name="Apellido_Materno_Personal",length = 50)
	public String getApellidoMaternoPersonal() {
		return apellidoMaternoPersonal;
	}

	@Column(name="Nombres_Personal",length = 150)
	public String getNombresPersonal() {
		return nombresPersonal;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Nacimiento_Personal", length = 23)
	public Date getFechaNacimientoPersonal() {
		return fechaNacimientoPersonal;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Condicion")
	//@Column(name="Id_Condicion",length = 2)
	public MaestroHisCondicionContrato getIdCondicion() {
		return idCondicion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Profesion")
	//@Column(name="Id_Profesion",length = 2)
	public MaestroHisProfesion getIdProfesion() {
		return idProfesion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Colegio")
	//@Column(name="Id_Colegio",length = 2)
	public MaestroHisColegio getIdColegio() {
		return idColegio;
	}

	@Column(name="Numero_Colegiatura",length = 20)
	public String getNumeroColegiatura() {
		return numeroColegiatura;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Establecimiento")
	//@Column(name="Id_Establecimiento")
	public MaestroHisEstablecimiento getIdEstablecimiento() {
		return idEstablecimiento;
	}

	@Column(name = "Fecha_Alta", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaAlta() {
		return fechaAlta;
	}

	@Column(name = "Fecha_Baja", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setIdPersonal(String idPersonal) {
		this.idPersonal = idPersonal;
	}

	public void setIdTipoDocumentoPersonal(MaestroHisTipoDoc idTipoDocumentoPersonal) {
		this.idTipoDocumentoPersonal = idTipoDocumentoPersonal;
	}

	public void setNumeroDocumentoPersonal(String numeroDocumentoPersonal) {
		this.numeroDocumentoPersonal = numeroDocumentoPersonal;
	}

	public void setApellidoPaternoPersonal(String apellidoPaternoPersonal) {
		this.apellidoPaternoPersonal = apellidoPaternoPersonal;
	}

	public void setApellidoMaternoPersonal(String apellidoMaternoPersonal) {
		this.apellidoMaternoPersonal = apellidoMaternoPersonal;
	}

	public void setNombresPersonal(String nombresPersonal) {
		this.nombresPersonal = nombresPersonal;
	}

	public void setFechaNacimientoPersonal(Date fechaNacimientoPersonal) {
		this.fechaNacimientoPersonal = fechaNacimientoPersonal;
	}

	public void setIdCondicion(MaestroHisCondicionContrato idCondicion) {
		this.idCondicion = idCondicion;
	}

	public void setIdProfesion(MaestroHisProfesion idProfesion) {
		this.idProfesion = idProfesion;
	}

	public void setIdColegio(MaestroHisColegio idColegio) {
		this.idColegio = idColegio;
	}

	public void setNumeroColegiatura(String numeroColegiatura) {
		this.numeroColegiatura = numeroColegiatura;
	}

	public void setIdEstablecimiento(MaestroHisEstablecimiento idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
	
	
	
	
}

