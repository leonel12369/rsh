package com.his.model;

/*import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;*/

//@Entity
//@Table(name="T_CONSOLIDADO_NUEVA_TRAMA_HISMINSA")
public class TConsolidadoNuevaTramaHisMinsa /* implements Serializable*/{
	
/*	private String idCita;
	private String anio;
	private String mes;
	private String dia;
	private Date fechaAtencion;
	private String anioMes;
	private String lote;
	private int numPag;
	private int numReg;
	private String idUps;
	private String descripcionUps;
	private int idEstablecimiento;
	private int codigoSector;
	private String descripcionSector;
	private int codigoDisa;
	private String descripcionDisa;
	private String codigoRed;
	private String  descripcionRed;
	private String codigoMicroRed;
	private String  descripcionMicroRed;
	private String codigoUnico; 
	private String  nombreEstablecimiento;
	private String  ubigueoEstablecimiento;
	private String departamentoEstablecimiento;
	private String provinciaEstablecimiento;
	private String distritoEstablecimiento;
	private String idPaciente;
	private int tipoDocPaciente;
	private String abrevTipoDocPaciente;
	private String numeroDocumentoPaciente;
	private String apellidoPaternoPaciente;
	private String apellidoMaternoPaciente;
	private String nombresPaciente;
	private Date fechaNacimientoPaciente;
	private String genero;
	private String historiaClinica;
	private String fichaFamiliar;
	private String idEtnia;
	private String descripcionEtnia;
	private String idFinanciador;
	private String descripcionFinanciador;
	private String idPais;
	private String descripcionPais;
	private String idPersonal;
	private int tipoDocPersonal;
	private String abrevTipoDocPersonal;
	private String numeroDocumentoPersonal;
	private String apellidoPaternoPersonal;
	private String apellidoMaternoPersonal;
	private String nombresPersonal;
	private Date fechaNacimientoPersonal;
	private int idCondicion;
	private String descripcionCondicion;
	private String idProfesion;
	private String descripcionProfesion;
	private String idColegio;
	private String descripcionColegio;
	private String numeroColegiatura;
	private String idRegistrador;
	private int tipoDocRegistrador;
	private String abrevTipoDocRegistrador;
	private String numeroDocumentoRegistrador;
	private String apellidoPaternoRegistrador;
	private String apellidoMaternoRegistrador;
	private String nombresRegistrador;
	private Date fechaNacimientoRegistrador;
	private String idCondicionEstablecimiento;
	private String idCondicionServicio;
	private int edadReg;
	private String tipoEdad;
	private int anioActualPaciente;
	private int mesActualPaciente;
	private int diaActualPaciente;
	private String grupoEdad;
	private String idTurno;
	private String fgTipo;
	private String codigoItem;
	private String descripcionItem;
	private String tipoDiagnostico;
	private String valorLab;
	private int  idCorrelativoItem;
	private double peso;
	private double talla;
	private double hemoglobina;
	private double pac;
	private double pc;
	private int idOtraCondicion;
	private String descripcionOtraCondicion;
	private Date fechaUltimaRegla;
	private Date fechaSolicitudHb;
	private Date fechaResultadoHb;
	private Date fechaRegistro;
	private Date fechaModificacion;
	
	public TConsolidadoNuevaTramaHisMinsa(String idCita, String anio, String mes, String dia, Date fechaAtencion,
			String anioMes, String lote, int numPag, int numReg, String idUps, String descripcionUps,
			int idEstablecimiento, int codigoSector, String descripcionSector, int codigoDisa, String descripcionDisa,
			String codigoRed, String descripcionRed, String codigoMicroRed, String descripcionMicroRed,
			String codigoUnico, String nombreEstablecimiento, String ubigueoEstablecimiento,
			String departamentoEstablecimiento, String provinciaEstablecimiento, String distritoEstablecimiento,
			String idPaciente, int tipoDocPaciente, String abrevTipoDocPaciente, String numeroDocumentoPaciente,
			String apellidoPaternoPaciente, String apellidoMaternoPaciente, String nombresPaciente,
			Date fechaNacimientoPaciente, String genero, String historiaClinica, String fichaFamiliar, String idEtnia,
			String descripcionEtnia, String idFinanciador, String descripcionFinanciador, String idPais,
			String descripcionPais, String idPersonal, int tipoDocPersonal, String abrevTipoDocPersonal,
			String numeroDocumentoPersonal, String apellidoPaternoPersonal, String apellidoMaternoPersonal,
			String nombresPersonal, Date fechaNacimientoPersonal, int idCondicion, String descripcionCondicion,
			String idProfesion, String descripcionProfesion, String idColegio, String descripcionColegio,
			String numeroColegiatura, String idRegistrador, int tipoDocRegistrador, String abrevTipoDocRegistrador,
			String numeroDocumentoRegistrador, String apellidoPaternoRegistrador, String apellidoMaternoRegistrador,
			String nombresRegistrador, Date fechaNacimientoRegistrador, String idCondicionEstablecimiento,
			String idCondicionServicio, int edadReg, String tipoEdad, int anioActualPaciente, int mesActualPaciente,
			int diaActualPaciente, String grupoEdad, String idTurno, String fgTipo, String codigoItem,
			String descripcionItem, String tipoDiagnostico, String valorLab, int idCorrelativoItem, double peso,
			double talla, double hemoglobina, double pac, double pc, int idOtraCondicion,
			String descripcionOtraCondicion, Date fechaUltimaRegla, Date fechaSolicitudHb, Date fechaResultadoHb,
			Date fechaRegistro, Date fechaModificacion) {
		super();
		this.idCita = idCita;
		this.anio = anio;
		this.mes = mes;
		this.dia = dia;
		this.fechaAtencion = fechaAtencion;
		this.anioMes = anioMes;
		this.lote = lote;
		this.numPag = numPag;
		this.numReg = numReg;
		this.idUps = idUps;
		this.descripcionUps = descripcionUps;
		this.idEstablecimiento = idEstablecimiento;
		this.codigoSector = codigoSector;
		this.descripcionSector = descripcionSector;
		this.codigoDisa = codigoDisa;
		this.descripcionDisa = descripcionDisa;
		this.codigoRed = codigoRed;
		this.descripcionRed = descripcionRed;
		this.codigoMicroRed = codigoMicroRed;
		this.descripcionMicroRed = descripcionMicroRed;
		this.codigoUnico = codigoUnico;
		this.nombreEstablecimiento = nombreEstablecimiento;
		this.ubigueoEstablecimiento = ubigueoEstablecimiento;
		this.departamentoEstablecimiento = departamentoEstablecimiento;
		this.provinciaEstablecimiento = provinciaEstablecimiento;
		this.distritoEstablecimiento = distritoEstablecimiento;
		this.idPaciente = idPaciente;
		this.tipoDocPaciente = tipoDocPaciente;
		this.abrevTipoDocPaciente = abrevTipoDocPaciente;
		this.numeroDocumentoPaciente = numeroDocumentoPaciente;
		this.apellidoPaternoPaciente = apellidoPaternoPaciente;
		this.apellidoMaternoPaciente = apellidoMaternoPaciente;
		this.nombresPaciente = nombresPaciente;
		this.fechaNacimientoPaciente = fechaNacimientoPaciente;
		this.genero = genero;
		this.historiaClinica = historiaClinica;
		this.fichaFamiliar = fichaFamiliar;
		this.idEtnia = idEtnia;
		this.descripcionEtnia = descripcionEtnia;
		this.idFinanciador = idFinanciador;
		this.descripcionFinanciador = descripcionFinanciador;
		this.idPais = idPais;
		this.descripcionPais = descripcionPais;
		this.idPersonal = idPersonal;
		this.tipoDocPersonal = tipoDocPersonal;
		this.abrevTipoDocPersonal = abrevTipoDocPersonal;
		this.numeroDocumentoPersonal = numeroDocumentoPersonal;
		this.apellidoPaternoPersonal = apellidoPaternoPersonal;
		this.apellidoMaternoPersonal = apellidoMaternoPersonal;
		this.nombresPersonal = nombresPersonal;
		this.fechaNacimientoPersonal = fechaNacimientoPersonal;
		this.idCondicion = idCondicion;
		this.descripcionCondicion = descripcionCondicion;
		this.idProfesion = idProfesion;
		this.descripcionProfesion = descripcionProfesion;
		this.idColegio = idColegio;
		this.descripcionColegio = descripcionColegio;
		this.numeroColegiatura = numeroColegiatura;
		this.idRegistrador = idRegistrador;
		this.tipoDocRegistrador = tipoDocRegistrador;
		this.abrevTipoDocRegistrador = abrevTipoDocRegistrador;
		this.numeroDocumentoRegistrador = numeroDocumentoRegistrador;
		this.apellidoPaternoRegistrador = apellidoPaternoRegistrador;
		this.apellidoMaternoRegistrador = apellidoMaternoRegistrador;
		this.nombresRegistrador = nombresRegistrador;
		this.fechaNacimientoRegistrador = fechaNacimientoRegistrador;
		this.idCondicionEstablecimiento = idCondicionEstablecimiento;
		this.idCondicionServicio = idCondicionServicio;
		this.edadReg = edadReg;
		this.tipoEdad = tipoEdad;
		this.anioActualPaciente = anioActualPaciente;
		this.mesActualPaciente = mesActualPaciente;
		this.diaActualPaciente = diaActualPaciente;
		this.grupoEdad = grupoEdad;
		this.idTurno = idTurno;
		this.fgTipo = fgTipo;
		this.codigoItem = codigoItem;
		this.descripcionItem = descripcionItem;
		this.tipoDiagnostico = tipoDiagnostico;
		this.valorLab = valorLab;
		this.idCorrelativoItem = idCorrelativoItem;
		this.peso = peso;
		this.talla = talla;
		this.hemoglobina = hemoglobina;
		this.pac = pac;
		this.pc = pc;
		this.idOtraCondicion = idOtraCondicion;
		this.descripcionOtraCondicion = descripcionOtraCondicion;
		this.fechaUltimaRegla = fechaUltimaRegla;
		this.fechaSolicitudHb = fechaSolicitudHb;
		this.fechaResultadoHb = fechaResultadoHb;
		this.fechaRegistro = fechaRegistro;
		this.fechaModificacion = fechaModificacion;
	}
	
	public TConsolidadoNuevaTramaHisMinsa() {}

	@Id
	@Column(name="Id_Cita",length = 50)
	public String getIdCita() {
		return idCita;
	}
	
	@Column(name="Anio",length = 4)
	public String getAnio() {
		return anio;
	}
	
	@Column(name="Mes",length = 2)
	public String getMes() {
		return mes;
	}

	@Column(name="Dia",length = 2)
	public String getDia() {
		return dia;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Atencion", length = 23)
	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	@Column(name="Anio_Mes",length = 6)
	public String getAnioMes() {
		return anioMes;
	}
	
	@Column(name="Lote",length = 3)
	public String getLote() {
		return lote;
	}

	@Column(name="Num_Pag")
	public int getNumPag() {
		return numPag;
	}
	
	@Column(name="Num_Reg")
	public int getNumReg() {
		return numReg;
	}

	@Column(name="Id_Ups",length = 6)
	public String getIdUps() {
		return idUps;
	}

	@Column(name="Descripcion_Ups",length = 100)
	public String getDescripcionUps() {
		return descripcionUps;
	}

	@Column(name="Id_Establecimiento")
	public int getIdEstablecimiento() {
		return idEstablecimiento;
	}

	@Column(name="Codigo_Sector")
	public int getCodigoSector() {
		return codigoSector;
	}

	@Column(name="Descripcion_Sector",length = 50)
	public String getDescripcionSector() {
		return descripcionSector;
	}

	@Column(name="Codigo_Disa")
	public int getCodigoDisa() {
		return codigoDisa;
	}

	@Column(name="Descripcion_Disa",length = 80)
	public String getDescripcionDisa() {
		return descripcionDisa;
	}

	@Column(name="Codigo_Red",length = 2)
	public String getCodigoRed() {
		return codigoRed;
	}

	@Column(name="Descripcion_Red",length = 70)
	public String getDescripcionRed() {
		return descripcionRed;
	}

	@Column(name="Codigo_MicroRed",length = 2)
	public String getCodigoMicroRed() {
		return codigoMicroRed;
	}
	
	@Column(name="Descripcion_MicroRed",length = 70)
	public String getDescripcionMicroRed() {
		return descripcionMicroRed;
	}

	@Column(name="Codigo_Unico",length = 9)
	public String getCodigoUnico() {
		return codigoUnico;
	}

	@Column(name="Nombre_Establecimiento",length = 100)
	public String getNombreEstablecimiento() {
		return nombreEstablecimiento;
	}

	@Column(name="Ubigueo_Establecimiento",length = 6)
	public String getUbigueoEstablecimiento() {
		return ubigueoEstablecimiento;
	}

	@Column(name="Departamento_Establecimiento",length = 150)
	public String getDepartamentoEstablecimiento() {
		return departamentoEstablecimiento;
	}

	@Column(name="Provincia_Establecimiento",length = 150)
	public String getProvinciaEstablecimiento() {
		return provinciaEstablecimiento;
	}
	
	@Column(name="Distrito_Establecimiento",length = 150)
	public String getDistritoEstablecimiento() {
		return distritoEstablecimiento;
	}
	
	@Column(name="Id_Paciente",length = 50)
	public String getIdPaciente() {
		return idPaciente;
	}

	@Column(name="Tipo_Doc_Paciente")
	public int getTipoDocPaciente() {
		return tipoDocPaciente;
	}
	
	@Column(name="Abrev_Tipo_Doc_Paciente",length = 20)
	public String getAbrevTipoDocPaciente() {
		return abrevTipoDocPaciente;
	}

	@Column(name="Numero_Documento_Paciente",length = 15)
	public String getNumeroDocumentoPaciente() {
		return numeroDocumentoPaciente;
	}

	@Column(name="Apellido_Paterno_Paciente",length = 50)
	public String getApellidoPaternoPaciente() {
		return apellidoPaternoPaciente;
	}

	@Column(name="Apellido_Materno_Paciente",length = 50)
	public String getApellidoMaternoPaciente() {
		return apellidoMaternoPaciente;
	}

	@Column(name="Nombres_Paciente",length = 150)
	public String getNombresPaciente() {
		return nombresPaciente;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Nacimiento_Paciente", length = 23)
	public Date getFechaNacimientoPaciente() {
		return fechaNacimientoPaciente;
	}

	@Column(name="Genero",length = 1)
	public String getGenero() {
		return genero;
	}

	@Column(name="Historia_Clinica",length = 15)
	public String getHistoriaClinica() {
		return historiaClinica;
	}

	@Column(name="Ficha_Familiar",length = 50)
	public String getFichaFamiliar() {
		return fichaFamiliar;
	}

	@Column(name="Id_Etnia",length = 3)
	public String getIdEtnia() {
		return idEtnia;
	}

	@Column(name="Descripcion_Etnia",length = 100)
	public String getDescripcionEtnia() {
		return descripcionEtnia;
	}

	@Column(name="Id_Financiador",length = 2)
	public String getIdFinanciador() {
		return idFinanciador;
	}

	@Column(name="Descripcion_Financiador",length = 100)
	public String getDescripcionFinanciador() {
		return descripcionFinanciador;
	}

	@Column(name="Id_Pais",length = 3)
	public String getIdPais() {
		return idPais;
	}

	@Column(name="Descripcion_Pais",length = 300)
	public String getDescripcionPais() {
		return descripcionPais;
	}

	@Column(name="Id_Personal",length = 50)
	public String getIdPersonal() {
		return idPersonal;
	}

	@Column(name="Tipo_Doc_Personal")
	public int getTipoDocPersonal() {
		return tipoDocPersonal;
	}

	@Column(name="Abrev_Tipo_Doc_Personal",length = 20)
	public String getAbrevTipoDocPersonal() {
		return abrevTipoDocPersonal;
	}

	@Column(name="Numero_Documento_Personal",length = 15)
	public String getNumeroDocumentoPersonal() {
		return numeroDocumentoPersonal;
	}

	@Column(name="Apellido_Paterno_Personal",length = 50)
	public String getApellidoPaternoPersonal() {
		return apellidoPaternoPersonal;
	}

	@Column(name="Apellido_Materno_Personal",length = 50)
	public String getApellidoMaternoPersonal() {
		return apellidoMaternoPersonal;
	}

	@Column(name="Nombres_Personal",length = 150)
	public String getNombresPersonal() {
		return nombresPersonal;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Nacimiento_Personal", length = 23)
	public Date getFechaNacimientoPersonal() {
		return fechaNacimientoPersonal;
	}

	@Column(name="Id_Condicion")
	public int getIdCondicion() {
		return idCondicion;
	}

	@Column(name="Descripcion_Condicion",length = 500)
	public String getDescripcionCondicion() {
		return descripcionCondicion;
	}

	@Column(name="Id_Profesion",length = 2)
	public String getIdProfesion() {
		return idProfesion;
	}

	@Column(name="Descripcion_Profesion",length = 200)
	public String getDescripcionProfesion() {
		return descripcionProfesion;
	}

	@Column(name="Id_Colegio",length = 2)
	public String getIdColegio() {
		return idColegio;
	}

	@Column(name="Descripcion_Colegio",length = 800)
	public String getDescripcionColegio() {
		return descripcionColegio;
	}

	@Column(name="Numero_Colegiatura",length = 20)
	public String getNumeroColegiatura() {
		return numeroColegiatura;
	}

	@Column(name="Id_Registrador",length = 50)
	public String getIdRegistrador() {
		return idRegistrador;
	}

	@Column(name="Tipo_Doc_Registrador")
	public int getTipoDocRegistrador() {
		return tipoDocRegistrador;
	}

	@Column(name="Abrev_Tipo_Doc_Registrador",length = 20)
	public String getAbrevTipoDocRegistrador() {
		return abrevTipoDocRegistrador;
	}

	@Column(name="Numero_Documento_Registrador",length = 15)
	public String getNumeroDocumentoRegistrador() {
		return numeroDocumentoRegistrador;
	}

	@Column(name="Apellido_Paterno_Registrador",length = 50)
	public String getApellidoPaternoRegistrador() {
		return apellidoPaternoRegistrador;
	}

	@Column(name="Apellido_Materno_Registrador",length = 50)
	public String getApellidoMaternoRegistrador() {
		return apellidoMaternoRegistrador;
	}

	@Column(name="Nombres_Registrador",length = 150)
	public String getNombresRegistrador() {
		return nombresRegistrador;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Nacimiento_Registrador", length = 23)
	public Date getFechaNacimientoRegistrador() {
		return fechaNacimientoRegistrador;
	}

	@Column(name="Id_Condicion_Establecimiento",length = 1)
	public String getIdCondicionEstablecimiento() {
		return idCondicionEstablecimiento;
	}

	@Column(name="Id_Condicion_Servicio",length = 1)
	public String getIdCondicionServicio() {
		return idCondicionServicio;
	}

	@Column(name="Edad_Reg")
	public int getEdadReg() {
		return edadReg;
	}

	@Column(name="Tipo_Edad",length = 1)
	public String getTipoEdad() {
		return tipoEdad;
	}

	@Column(name="Anio_Actual_Paciente")
	public int getAnioActualPaciente() {
		return anioActualPaciente;
	}

	@Column(name="Mes_Actual_Paciente")
	public int getMesActualPaciente() {
		return mesActualPaciente;
	}

	@Column(name="Dia_Actual_Paciente")
	public int getDiaActualPaciente() {
		return diaActualPaciente;
	}

	@Column(name="Grupo_Edad",length = 13)
	public String getGrupoEdad() {
		return grupoEdad;
	}

	@Column(name="Id_Turno",length = 1)
	public String getIdTurno() {
		return idTurno;
	}

	@Column(name="Fg_Tipo",length = 2)
	public String getFgTipo() {
		return fgTipo;
	}

	@Column(name="Codigo_Item",length = 15)
	public String getCodigoItem() {
		return codigoItem;
	}

	@Column(name="Descripcion_Item",length = 300)
	public String getDescripcionItem() {
		return descripcionItem;
	}

	@Column(name="Tipo_Diagnostico",length = 1)
	public String getTipoDiagnostico() {
		return tipoDiagnostico;
	}

	@Column(name="Valor_Lab",length = 3)
	public String getValorLab() {
		return valorLab;
	}

	@Column(name="Id_Correlativo_Item")
	public int getIdCorrelativoItem() {
		return idCorrelativoItem;
	}
	@Column(name="Peso", precision = 6, scale = 3)
	public double getPeso() {
		return peso;
	}

	@Column(name="Talla", precision = 5, scale = 2)
	public double getTalla() {
		return talla;
	}

	@Column(name="Hemoglobina", precision = 5, scale = 2)
	public double getHemoglobina() {
		return hemoglobina;
	}

	@Column(name="Pac", precision = 5, scale = 2)
	public double getPac() {
		return pac;
	}

	@Column(name="Pc", precision = 5, scale = 2)
	public double getPc() {
		return pc;
	}

	@Column(name="Id_Otra_Condicion")
	public int getIdOtraCondicion() {
		return idOtraCondicion;
	}

	@Column(name="Descripcion_Otra_Condicion",length = 300)
	public String getDescripcionOtraCondicion() {
		return descripcionOtraCondicion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Ultima_Regla", length = 23)
	public Date getFechaUltimaRegla() {
		return fechaUltimaRegla;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Solicitud_Hb", length = 23)
	public Date getFechaSolicitudHb() {
		return fechaSolicitudHb;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Resultado_Hb", length = 23)
	public Date getFechaResultadoHb() {
		return fechaResultadoHb;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Registro", length = 23)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Modificacion", length = 23)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setIdCita(String idCita) {
		this.idCita = idCita;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setNumPag(int numPag) {
		this.numPag = numPag;
	}

	public void setNumReg(int numReg) {
		this.numReg = numReg;
	}

	public void setIdUps(String idUps) {
		this.idUps = idUps;
	}

	public void setDescripcionUps(String descripcionUps) {
		this.descripcionUps = descripcionUps;
	}

	public void setIdEstablecimiento(int idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public void setCodigoSector(int codigoSector) {
		this.codigoSector = codigoSector;
	}

	public void setDescripcionSector(String descripcionSector) {
		this.descripcionSector = descripcionSector;
	}

	public void setCodigoDisa(int codigoDisa) {
		this.codigoDisa = codigoDisa;
	}

	public void setDescripcionDisa(String descripcionDisa) {
		this.descripcionDisa = descripcionDisa;
	}

	public void setCodigoRed(String codigoRed) {
		this.codigoRed = codigoRed;
	}

	public void setDescripcionRed(String descripcionRed) {
		this.descripcionRed = descripcionRed;
	}

	public void setCodigoMicroRed(String codigoMicroRed) {
		this.codigoMicroRed = codigoMicroRed;
	}

	public void setDescripcionMicroRed(String descripcionMicroRed) {
		this.descripcionMicroRed = descripcionMicroRed;
	}

	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}

	public void setNombreEstablecimiento(String nombreEstablecimiento) {
		this.nombreEstablecimiento = nombreEstablecimiento;
	}

	public void setUbigueoEstablecimiento(String ubigueoEstablecimiento) {
		this.ubigueoEstablecimiento = ubigueoEstablecimiento;
	}

	public void setDepartamentoEstablecimiento(String departamentoEstablecimiento) {
		this.departamentoEstablecimiento = departamentoEstablecimiento;
	}

	public void setProvinciaEstablecimiento(String provinciaEstablecimiento) {
		this.provinciaEstablecimiento = provinciaEstablecimiento;
	}

	public void setDistritoEstablecimiento(String distritoEstablecimiento) {
		this.distritoEstablecimiento = distritoEstablecimiento;
	}

	public void setIdPaciente(String idPaciente) {
		this.idPaciente = idPaciente;
	}

	public void setTipoDocPaciente(int tipoDocPaciente) {
		this.tipoDocPaciente = tipoDocPaciente;
	}

	public void setAbrevTipoDocPaciente(String abrevTipoDocPaciente) {
		this.abrevTipoDocPaciente = abrevTipoDocPaciente;
	}

	public void setNumeroDocumentoPaciente(String numeroDocumentoPaciente) {
		this.numeroDocumentoPaciente = numeroDocumentoPaciente;
	}

	public void setApellidoPaternoPaciente(String apellidoPaternoPaciente) {
		this.apellidoPaternoPaciente = apellidoPaternoPaciente;
	}

	public void setApellidoMaternoPaciente(String apellidoMaternoPaciente) {
		this.apellidoMaternoPaciente = apellidoMaternoPaciente;
	}

	public void setNombresPaciente(String nombresPaciente) {
		this.nombresPaciente = nombresPaciente;
	}

	public void setFechaNacimientoPaciente(Date fechaNacimientoPaciente) {
		this.fechaNacimientoPaciente = fechaNacimientoPaciente;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setHistoriaClinica(String historiaClinica) {
		this.historiaClinica = historiaClinica;
	}

	public void setFichaFamiliar(String fichaFamiliar) {
		this.fichaFamiliar = fichaFamiliar;
	}

	public void setIdEtnia(String idEtnia) {
		this.idEtnia = idEtnia;
	}

	public void setDescripcionEtnia(String descripcionEtnia) {
		this.descripcionEtnia = descripcionEtnia;
	}

	public void setIdFinanciador(String idFinanciador) {
		this.idFinanciador = idFinanciador;
	}

	public void setDescripcionFinanciador(String descripcionFinanciador) {
		this.descripcionFinanciador = descripcionFinanciador;
	}

	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}

	public void setDescripcionPais(String descripcionPais) {
		this.descripcionPais = descripcionPais;
	}

	public void setIdPersonal(String idPersonal) {
		this.idPersonal = idPersonal;
	}

	public void setTipoDocPersonal(int tipoDocPersonal) {
		this.tipoDocPersonal = tipoDocPersonal;
	}

	public void setAbrevTipoDocPersonal(String abrevTipoDocPersonal) {
		this.abrevTipoDocPersonal = abrevTipoDocPersonal;
	}

	public void setNumeroDocumentoPersonal(String numeroDocumentoPersonal) {
		this.numeroDocumentoPersonal = numeroDocumentoPersonal;
	}

	public void setApellidoPaternoPersonal(String apellidoPaternoPersonal) {
		this.apellidoPaternoPersonal = apellidoPaternoPersonal;
	}

	public void setApellidoMaternoPersonal(String apellidoMaternoPersonal) {
		this.apellidoMaternoPersonal = apellidoMaternoPersonal;
	}

	public void setNombresPersonal(String nombresPersonal) {
		this.nombresPersonal = nombresPersonal;
	}

	public void setFechaNacimientoPersonal(Date fechaNacimientoPersonal) {
		this.fechaNacimientoPersonal = fechaNacimientoPersonal;
	}

	public void setIdCondicion(int idCondicion) {
		this.idCondicion = idCondicion;
	}

	public void setDescripcionCondicion(String descripcionCondicion) {
		this.descripcionCondicion = descripcionCondicion;
	}

	public void setIdProfesion(String idProfesion) {
		this.idProfesion = idProfesion;
	}

	public void setDescripcionProfesion(String descripcionProfesion) {
		this.descripcionProfesion = descripcionProfesion;
	}

	public void setIdColegio(String idColegio) {
		this.idColegio = idColegio;
	}

	public void setDescripcionColegio(String descripcionColegio) {
		this.descripcionColegio = descripcionColegio;
	}

	public void setNumeroColegiatura(String numeroColegiatura) {
		this.numeroColegiatura = numeroColegiatura;
	}

	public void setIdRegistrador(String idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public void setTipoDocRegistrador(int tipoDocRegistrador) {
		this.tipoDocRegistrador = tipoDocRegistrador;
	}

	public void setAbrevTipoDocRegistrador(String abrevTipoDocRegistrador) {
		this.abrevTipoDocRegistrador = abrevTipoDocRegistrador;
	}

	public void setNumeroDocumentoRegistrador(String numeroDocumentoRegistrador) {
		this.numeroDocumentoRegistrador = numeroDocumentoRegistrador;
	}

	public void setApellidoPaternoRegistrador(String apellidoPaternoRegistrador) {
		this.apellidoPaternoRegistrador = apellidoPaternoRegistrador;
	}

	public void setApellidoMaternoRegistrador(String apellidoMaternoRegistrador) {
		this.apellidoMaternoRegistrador = apellidoMaternoRegistrador;
	}

	public void setNombresRegistrador(String nombresRegistrador) {
		this.nombresRegistrador = nombresRegistrador;
	}

	public void setFechaNacimientoRegistrador(Date fechaNacimientoRegistrador) {
		this.fechaNacimientoRegistrador = fechaNacimientoRegistrador;
	}

	public void setIdCondicionEstablecimiento(String idCondicionEstablecimiento) {
		this.idCondicionEstablecimiento = idCondicionEstablecimiento;
	}

	public void setIdCondicionServicio(String idCondicionServicio) {
		this.idCondicionServicio = idCondicionServicio;
	}

	public void setEdadReg(int edadReg) {
		this.edadReg = edadReg;
	}

	public void setTipoEdad(String tipoEdad) {
		this.tipoEdad = tipoEdad;
	}

	public void setAnioActualPaciente(int anioActualPaciente) {
		this.anioActualPaciente = anioActualPaciente;
	}

	public void setMesActualPaciente(int mesActualPaciente) {
		this.mesActualPaciente = mesActualPaciente;
	}

	public void setDiaActualPaciente(int diaActualPaciente) {
		this.diaActualPaciente = diaActualPaciente;
	}

	public void setGrupoEdad(String grupoEdad) {
		this.grupoEdad = grupoEdad;
	}

	public void setIdTurno(String idTurno) {
		this.idTurno = idTurno;
	}

	public void setFgTipo(String fgTipo) {
		this.fgTipo = fgTipo;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}

	public void setTipoDiagnostico(String tipoDiagnostico) {
		this.tipoDiagnostico = tipoDiagnostico;
	}

	public void setValorLab(String valorLab) {
		this.valorLab = valorLab;
	}

	public void setIdCorrelativoItem(int idCorrelativoItem) {
		this.idCorrelativoItem = idCorrelativoItem;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public void setTalla(double talla) {
		this.talla = talla;
	}

	public void setHemoglobina(double hemoglobina) {
		this.hemoglobina = hemoglobina;
	}

	public void setPac(double pac) {
		this.pac = pac;
	}

	public void setPc(double pc) {
		this.pc = pc;
	}

	public void setIdOtraCondicion(int idOtraCondicion) {
		this.idOtraCondicion = idOtraCondicion;
	}

	public void setDescripcionOtraCondicion(String descripcionOtraCondicion) {
		this.descripcionOtraCondicion = descripcionOtraCondicion;
	}

	public void setFechaUltimaRegla(Date fechaUltimaRegla) {
		this.fechaUltimaRegla = fechaUltimaRegla;
	}

	public void setFechaSolicitudHb(Date fechaSolicitudHb) {
		this.fechaSolicitudHb = fechaSolicitudHb;
	}

	public void setFechaResultadoHb(Date fechaResultadoHb) {
		this.fechaResultadoHb = fechaResultadoHb;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	*/
}
