package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_ESTABLECIMIENTO")
public class MaestroHisEstablecimiento implements Serializable{

	private int idEstablecimiento;
	private String nombreEstablecimiento;
	private MaestroHisUbigeoIneiReniec ubigueoEstablecimiento;// datos desigual
	private int codigoDisa;
	private String disa;
	private String codigoRed;
	private String red;
	private String codigoMicroRed;
	private String microRed;
	private String codigoUnico;
	private int codigoSector;
	private String descripcionSector;
	private String departamento;
	private String provincia;
	private String distrito;
	
	public MaestroHisEstablecimiento(int idEstablecimiento, String nombreEstablecimiento, MaestroHisUbigeoIneiReniec ubigueoEstablecimiento,
			int codigoDisa, String disa, String codigoRed, String red, String codigoMicroRed, String microRed,
			String codigoUnico, int codigoSector, String descripcionSector, String departamento, String provincia,
			String distrito) {
		super();
		this.idEstablecimiento = idEstablecimiento;
		this.nombreEstablecimiento = nombreEstablecimiento;
		this.ubigueoEstablecimiento = ubigueoEstablecimiento;
		this.codigoDisa = codigoDisa;
		this.disa = disa;
		this.codigoRed = codigoRed;
		this.red = red;
		this.codigoMicroRed = codigoMicroRed;
		this.microRed = microRed;
		this.codigoUnico = codigoUnico;
		this.codigoSector = codigoSector;
		this.descripcionSector = descripcionSector;
		this.departamento = departamento;
		this.provincia = provincia;
		this.distrito = distrito;
	}
	
	public MaestroHisEstablecimiento() {}

	@Id
	@Column(name="Id_Establecimiento",nullable = false)
	public int getIdEstablecimiento() {
		return idEstablecimiento;
	}

	@Column(name="Nombre_Establecimiento", length = 100)
	public String getNombreEstablecimiento() {
		return nombreEstablecimiento;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Ubigueo_Establecimiento")
	//@Column(name="Ubigueo_Establecimiento", length = 6)
	public MaestroHisUbigeoIneiReniec getUbigueoEstablecimiento() {
		return ubigueoEstablecimiento;
	}

	@Column(name="Codigo_Disa")
	public int getCodigoDisa() {
		return codigoDisa;
	}

	@Column(name="Disa", length = 80)
	public String getDisa() {
		return disa;
	}

	@Column(name="Codigo_Red", length = 2)
	public String getCodigoRed() {
		return codigoRed;
	}

	@Column(name="Red", length = 70)
	public String getRed() {
		return red;
	}

	@Column(name="Codigo_MicroRed", length = 2)
	public String getCodigoMicroRed() {
		return codigoMicroRed;
	}

	@Column(name="MicroRed", length = 70)
	public String getMicroRed() {
		return microRed;
	}

	@Column(name="Codigo_Unico", length = 9)
	public String getCodigoUnico() {
		return codigoUnico;
	}

	@Column(name="Codigo_Sector")
	public int getCodigoSector() {
		return codigoSector;
	}

	@Column(name="Descripcion_Sector", length = 50)
	public String getDescripcionSector() {
		return descripcionSector;
	}

	@Column(name="Departamento", length = 150)
	public String getDepartamento() {
		return departamento;
	}

	@Column(name="Provincia", length = 150)
	public String getProvincia() {
		return provincia;
	}

	@Column(name="Distrito", length = 150)
	public String getDistrito() {
		return distrito;
	}

	public void setIdEstablecimiento(int idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public void setNombreEstablecimiento(String nombreEstablecimiento) {
		this.nombreEstablecimiento = nombreEstablecimiento;
	}

	public void setUbigueoEstablecimiento(MaestroHisUbigeoIneiReniec ubigueoEstablecimiento) {
		this.ubigueoEstablecimiento = ubigueoEstablecimiento;
	}

	public void setCodigoDisa(int codigoDisa) {
		this.codigoDisa = codigoDisa;
	}

	public void setDisa(String disa) {
		this.disa = disa;
	}

	public void setCodigoRed(String codigoRed) {
		this.codigoRed = codigoRed;
	}

	public void setRed(String red) {
		this.red = red;
	}

	public void setCodigoMicroRed(String codigoMicroRed) {
		this.codigoMicroRed = codigoMicroRed;
	}

	public void setMicroRed(String microRed) {
		this.microRed = microRed;
	}

	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}

	public void setCodigoSector(int codigoSector) {
		this.codigoSector = codigoSector;
	}

	public void setDescripcionSector(String descripcionSector) {
		this.descripcionSector = descripcionSector;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	
	
	
}
