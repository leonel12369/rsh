package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_PROFESION")
public class MaestroHisProfesion implements Serializable{

	private String idProfesion;
	private String descripcionProfesion;
	private MaestroHisColegio idColegio;
	
	@Id
	@Column(name="Id_Profesion",nullable = false, length = 2)
	public String getIdProfesion() {
		return idProfesion;
	}
	
	public void setIdProfesion(String idProfesion) {
		this.idProfesion = idProfesion;
	}
	
	@Column(name="Descripcion_Profesion", length = 200)
	public String getDescripcionProfesion() {
		return descripcionProfesion;
	}
	
	public void setDescripcionProfesion(String descripcionProfesion) {
		this.descripcionProfesion = descripcionProfesion;
	}
	
	//@Column(name="Id_Colegio", length = 2)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Colegio")
	public MaestroHisColegio getIdColegio() {
		return idColegio;
	}
	
	public void setIdColegio(MaestroHisColegio idColegio) {
		this.idColegio = idColegio;
	}

	public MaestroHisProfesion(String idProfesion, String descripcionProfesion, MaestroHisColegio idColegio) {
		super();
		this.idProfesion = idProfesion;
		this.descripcionProfesion = descripcionProfesion;
		this.idColegio = idColegio;
	}
	
	public MaestroHisProfesion() {}
	
}
