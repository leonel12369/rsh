package com.his.model;

import javax.persistence.Column;

public class Ubigeo {


	private String value;
	
	private String nombre;
	
	public Ubigeo() {}

	public Ubigeo(String value, String nombre) {
		super();
		this.value = value;
		this.nombre = nombre;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	
}


