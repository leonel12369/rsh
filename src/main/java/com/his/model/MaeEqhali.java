package com.his.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mae_eqhali")
public class MaeEqhali implements Serializable{

	private int id;
	private String nombreLote;
	private String descripcionModulo;
	private String grupo ;
	public Boolean estado;
	
	public MaeEqhali(int id, String nombreLote, String descripcionModulo, String grupo,Boolean estado) {
		super();
		this.id = id;
		this.nombreLote = nombreLote;
		this.descripcionModulo = descripcionModulo;
		this.grupo = grupo;
		this.estado = estado;
	}
	
	public MaeEqhali() {}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	@Column(name="nombre_lote",length = 3)
	public String getNombreLote() {
		return nombreLote;
	}

	@Column(name="descripcion_modulo",length = 50)
	public String getDescripcionModulo() {
		return descripcionModulo;
	}
	
	@Column(name="grupo",length = 15)
	public String getGrupo() {
		return grupo;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNombreLote(String nombreLote) {
		this.nombreLote = nombreLote;
	}

	public void setDescripcionModulo(String descripcionModulo) {
		this.descripcionModulo = descripcionModulo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	
	@Column(name = "estado")
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
}
