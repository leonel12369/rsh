package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBM_ANIOS_MES_ARCHIVO_PLANO_TMP")
public class TbmAniosMesArchivoPlanoTmp implements Serializable {//revisar autoincrement

	private int iId;
	private String vSector;
	private int iAnnio;
	private int iMes;
	private int iFGEstado;
	
	public TbmAniosMesArchivoPlanoTmp(int iId, String vSector, int iAnnio, int iMes, int iFGEstado) {
		super();
		this.iId = iId;
		this.vSector = vSector;
		this.iAnnio = iAnnio;
		this.iMes = iMes;
		this.iFGEstado = iFGEstado;
	}
	
	
	public TbmAniosMesArchivoPlanoTmp() {}

	@Id
	@Column(name="I_ID",nullable = false)
	public int getiId() {
		return iId;
	}

	@Column(name="V_SECTOR", length = 8)
	public String getvSector() {
		return vSector;
	}

	@Column(name="I_ANNIO")
	public int getiAnnio() {
		return iAnnio;
	}

	@Column(name="I_MES")
	public int getiMes() {
		return iMes;
	}

	@Column(name="I_FG_ESTADO")
	public int getiFGEstado() {
		return iFGEstado;
	}


	public void setiId(int iId) {
		this.iId = iId;
	}


	public void setvSector(String vSector) {
		this.vSector = vSector;
	}


	public void setiAnnio(int iAnnio) {
		this.iAnnio = iAnnio;
	}


	public void setiMes(int iMes) {
		this.iMes = iMes;
	}


	public void setiFGEstado(int iFGEstado) {
		this.iFGEstado = iFGEstado;
	}
	
	
	
}
