package com.his.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="mae_estrategiaSanitaria")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class MaeEstrategiaSanitaria implements Serializable {

	private int idEstrategia;
	private String descripcionEstrategia;
	private String programa;
	public Boolean estado;
	
	public MaeEstrategiaSanitaria(int idEstrategia, String descripcionEstrategia, String programa,Boolean estado) {
		super();
		this.idEstrategia = idEstrategia;
		this.descripcionEstrategia = descripcionEstrategia;
		this.estado = estado;
		this.programa = programa;
	}
	
	public MaeEstrategiaSanitaria() {}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name="idEstrategia",nullable = false)
	public int getIdEstrategia() {
		return idEstrategia;
	}

	@Column(name="descripcionEstrategia", length = 500)
	public String getDescripcionEstrategia() {
		return descripcionEstrategia;
	}

	@Column(name="programa", length = 500)
	public String getPrograma() {
		return programa;
	}

	public void setIdEstrategia(int idEstrategia) {
		this.idEstrategia = idEstrategia;
	}

	public void setDescripcionEstrategia(String descripcionEstrategia) {
		this.descripcionEstrategia = descripcionEstrategia;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}
	
	@Column(name = "estado")
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
}
