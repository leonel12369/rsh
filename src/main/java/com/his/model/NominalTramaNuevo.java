package com.his.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="NOMINAL_TRAMA_NUEVO")
public class NominalTramaNuevo  implements Serializable{
	
	private MaeCitas idCita;
	private String anio;
	private String mes;
	private String dia;
	private Date fechaAtencion;
	private String lote;
	private int numPag;
	private int numReg;
	private MaestroHisUps  idUps;////-------------------------->datos desiguales bloc de notas
	private MaestroHisEstablecimiento idEstablecimiento;
	private MaestroPaciente idPaciente;
	private MaestroPersonal idPersonal;
	private MaestroRegistrador idRegistrador;
	private MaestroHisFinanciador idFinanciador;
	private String idCondicionEstablecimiento;
	private String idCondicionServicio;
	private int edadReg;
	private String tipoEdad;
	private int anioActualPaciente;
	private int mesActualPaciente;
	private int diaActualPaciente;
	private String idTurno;
	private MaestroHisCieCpms  codigoItem;///-------------------------->datos desiguales bloc de notas
	private String tipoDiagnostico;
	private String valorLab;
	private int idCorrelativoItem;
	private Float peso;
	private Float talla;
	private Float hemoglobina;
	private Float pac;
	private Float pc;
	private MaestroHisOtraCondicion idOtraCondicion;
	private Date fechaUltimaRegla;
	private Date fechaSolicitudHb;
	private Date fechaResultadoHb;
	private Date fechaRegistro;
	private Date fechaModificacion;
	private MaestroHisPais idPais;
	private int id;
	
	public NominalTramaNuevo(MaeCitas idCita, String anio, String mes, String dia, Date fechaAtencion, String lote,
			int numPag, int numReg, MaestroHisUps idUps, MaestroHisEstablecimiento idEstablecimiento, MaestroPaciente idPaciente, MaestroPersonal idPersonal,
			MaestroRegistrador idRegistrador, MaestroHisFinanciador idFinanciador, String idCondicionEstablecimiento, String idCondicionServicio,
			int edadReg, String tipoEdad, int anioActualPaciente, int mesActualPaciente, int diaActualPaciente,
			String idTurno, MaestroHisCieCpms codigoItem, String tipoDiagnostico, String valorLab, int idCorrelativoItem,
			Float peso, Float talla, Float hemoglobina, Float pac, Float pc, MaestroHisOtraCondicion idOtraCondicion,
			Date fechaUltimaRegla, Date fechaSolicitudHb, Date fechaResultadoHb, Date fechaRegistro,
			Date fechaModificacion, MaestroHisPais idPais,int id) {
		super();
		this.idCita = idCita;
		this.anio = anio;
		this.mes = mes;
		this.dia = dia;
		this.fechaAtencion = fechaAtencion;
		this.lote = lote;
		this.numPag = numPag;
		this.numReg = numReg;
		this.idUps = idUps;
		this.idEstablecimiento = idEstablecimiento;
		this.idPaciente = idPaciente;
		this.idPersonal = idPersonal;
		this.idRegistrador = idRegistrador;
		this.idFinanciador = idFinanciador;
		this.idCondicionEstablecimiento = idCondicionEstablecimiento;
		this.idCondicionServicio = idCondicionServicio;
		this.edadReg = edadReg;
		this.tipoEdad = tipoEdad;
		this.anioActualPaciente = anioActualPaciente;
		this.mesActualPaciente = mesActualPaciente;
		this.diaActualPaciente = diaActualPaciente;
		this.idTurno = idTurno;
		this.codigoItem = codigoItem;
		this.tipoDiagnostico = tipoDiagnostico;
		this.valorLab = valorLab;
		this.idCorrelativoItem = idCorrelativoItem;
		this.peso = peso;
		this.talla = talla;
		this.hemoglobina = hemoglobina;
		this.pac = pac;
		this.pc = pc;
		this.idOtraCondicion = idOtraCondicion;
		this.fechaUltimaRegla = fechaUltimaRegla;
		this.fechaSolicitudHb = fechaSolicitudHb;
		this.fechaResultadoHb = fechaResultadoHb;
		this.fechaRegistro = fechaRegistro;
		this.fechaModificacion = fechaModificacion;
		this.idPais = idPais;
		this.id=id;
	}
	
	public NominalTramaNuevo() {}


	
	@Id
	@Column(name="id",nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Cita")
	//@Column(name="Id_Cita",length = 50)
	public MaeCitas getIdCita() {
		return idCita;
	}
	@Column(name="Anio",length = 4)
	public String getAnio() {
		return anio;
	}

	@Column(name="Mes",length = 2)
	public String getMes() {
		return mes;
	}

	@Column(name="Dia",length = 2)
	public String getDia() {
		return dia;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Atencion", length = 23)
	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	@Column(name="Lote",length = 3)
	public String getLote() {
		return lote;
	}

	@Column(name="Num_Pag")
	public int getNumPag() {
		return numPag;
	}

	@Column(name="Num_Reg")
	public int getNumReg() {
		return numReg;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Ups")
	//@Column(name="Id_Ups",length = 6)
	public MaestroHisUps getIdUps() {
		return idUps;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Establecimiento")
	//@Column(name="Id_Establecimiento")
	public MaestroHisEstablecimiento getIdEstablecimiento() {
		return idEstablecimiento;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Paciente")
	//@Column(name="Id_Paciente",length = 50)
	public MaestroPaciente getIdPaciente() {
		return idPaciente;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Personal")
	//@Column(name="Id_Personal",length = 50)
	public MaestroPersonal getIdPersonal() {
		return idPersonal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Registrador")
	//@Column(name="Id_Registrador",length = 50)
	public MaestroRegistrador getIdRegistrador() {
		return idRegistrador;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Financiador")
	//@Column(name="Id_Financiador",length = 2)
	public MaestroHisFinanciador getIdFinanciador() {
		return idFinanciador;
	}

	@Column(name="Id_Condicion_Establecimiento",length = 1)
	public String getIdCondicionEstablecimiento() {
		return idCondicionEstablecimiento;
	}

	@Column(name="Id_Condicion_Servicio",length = 1)
	public String getIdCondicionServicio() {
		return idCondicionServicio;
	}

	@Column(name="Edad_Reg")
	public int getEdadReg() {
		return edadReg;
	}

	@Column(name="Tipo_Edad",length = 1)
	public String getTipoEdad() {
		return tipoEdad;
	}

	@Column(name="Anio_Actual_Paciente")
	public int getAnioActualPaciente() {
		return anioActualPaciente;
	}
	
	@Column(name="Mes_Actual_Paciente")
	public int getMesActualPaciente() {
		return mesActualPaciente;
	}
	
	@Column(name="Dia_Actual_Paciente")
	public int getDiaActualPaciente() {
		return diaActualPaciente;
	}

	@Column(name="Id_Turno",length = 1)
	public String getIdTurno() {
		return idTurno;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Codigo_Item")
	//@Column(name="Codigo_Item",length = 15)
	public MaestroHisCieCpms getCodigoItem() {
		return codigoItem;
	}

	@Column(name="Tipo_Diagnostico",length = 1)
	public String getTipoDiagnostico() {
		return tipoDiagnostico;
	}

	@Column(name="Valor_Lab",length = 3)
	public String getValorLab() {
		return valorLab;
	}

	@Column(name="Id_Correlativo_Item")
	public int getIdCorrelativoItem() {
		return idCorrelativoItem;
	}

	@Column(name="Peso", precision = 6, scale = 3)
	public Float getPeso() {
		return peso;
	}

	@Column(name="Talla", precision = 5, scale = 2)
	public Float getTalla() {
		return talla;
	}

	@Column(name="Hemoglobina", precision = 5, scale = 2)
	public Float getHemoglobina() {
		return hemoglobina;
	}

	@Column(name="Pac", precision = 5, scale = 2,nullable = true)
	public Float getPac() {
		return pac;
	}
	
	@Column(name="Pc", precision = 5, scale = 2)
	public Float getPc() {
		return pc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Otra_Condicion")
	//@Column(name="Id_Otra_Condicion")
	public MaestroHisOtraCondicion getIdOtraCondicion() {
		return idOtraCondicion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Ultima_Regla", length = 23)
	public Date getFechaUltimaRegla() {
		return fechaUltimaRegla;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Solicitud_Hb", length = 23)
	public Date getFechaSolicitudHb() {
		return fechaSolicitudHb;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Resultado_Hb", length = 23)
	public Date getFechaResultadoHb() {
		return fechaResultadoHb;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Registro", length = 23)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Fecha_Modificacion", length = 23)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Pais")
	//@Column(name="Id_Pais",length = 3)
	public MaestroHisPais getIdPais() {
		return idPais;
	}

	public void setIdCita(MaeCitas idCita) {
		this.idCita = idCita;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setNumPag(int numPag) {
		this.numPag = numPag;
	}

	public void setNumReg(int numReg) {
		this.numReg = numReg;
	}

	public void setIdUps(MaestroHisUps idUps) {
		this.idUps = idUps;
	}

	public void setIdEstablecimiento(MaestroHisEstablecimiento idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public void setIdPaciente(MaestroPaciente idPaciente) {
		this.idPaciente = idPaciente;
	}

	public void setIdPersonal(MaestroPersonal idPersonal) {
		this.idPersonal = idPersonal;
	}

	public void setIdRegistrador(MaestroRegistrador idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public void setIdFinanciador(MaestroHisFinanciador idFinanciador) {
		this.idFinanciador = idFinanciador;
	}

	public void setIdCondicionEstablecimiento(String idCondicionEstablecimiento) {
		this.idCondicionEstablecimiento = idCondicionEstablecimiento;
	}

	public void setIdCondicionServicio(String idCondicionServicio) {
		this.idCondicionServicio = idCondicionServicio;
	}

	public void setEdadReg(int edadReg) {
		this.edadReg = edadReg;
	}

	public void setTipoEdad(String tipoEdad) {
		this.tipoEdad = tipoEdad;
	}

	public void setAnioActualPaciente(int anioActualPaciente) {
		this.anioActualPaciente = anioActualPaciente;
	}

	public void setMesActualPaciente(int mesActualPaciente) {
		this.mesActualPaciente = mesActualPaciente;
	}

	public void setDiaActualPaciente(int diaActualPaciente) {
		this.diaActualPaciente = diaActualPaciente;
	}

	public void setIdTurno(String idTurno) {
		this.idTurno = idTurno;
	}

	public void setCodigoItem(MaestroHisCieCpms codigoItem) {
		this.codigoItem = codigoItem;
	}

	public void setTipoDiagnostico(String tipoDiagnostico) {
		this.tipoDiagnostico = tipoDiagnostico;
	}

	public void setValorLab(String valorLab) {
		this.valorLab = valorLab;
	}

	public void setIdCorrelativoItem(int idCorrelativoItem) {
		this.idCorrelativoItem = idCorrelativoItem;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

	public void setTalla(Float talla) {
		this.talla = talla;
	}

	public void setHemoglobina(Float hemoglobina) {
		this.hemoglobina = hemoglobina;
	}

	public void setPac(Float pac) {
		this.pac = pac;
	}

	public void setPc(Float pc) {
		this.pc = pc;
	}

	public void setIdOtraCondicion(MaestroHisOtraCondicion idOtraCondicion) {
		this.idOtraCondicion = idOtraCondicion;
	}

	public void setFechaUltimaRegla(Date fechaUltimaRegla) {
		this.fechaUltimaRegla = fechaUltimaRegla;
	}

	public void setFechaSolicitudHb(Date fechaSolicitudHb) {
		this.fechaSolicitudHb = fechaSolicitudHb;
	}

	public void setFechaResultadoHb(Date fechaResultadoHb) {
		this.fechaResultadoHb = fechaResultadoHb;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public void setIdPais(MaestroHisPais idPais) {
		this.idPais = idPais;
	}
	
	
}
