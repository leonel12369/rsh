package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_ETNIA")
public class MaestroHisEtnia  implements Serializable{

	private String idEtnia;
	
	private String descripcionEtnia;

	public MaestroHisEtnia() {}
	
	public MaestroHisEtnia(String idEtnia, String descripcionEtnia) {
		super();
		this.idEtnia = idEtnia;
		this.descripcionEtnia = descripcionEtnia;
	}

	@Id
	@Column(name="Id_Etnia",nullable = false, length = 2 )
	public String getIdEtnia() {
		return idEtnia;
	}

	public void setIdEtnia(String idEtnia) {
		this.idEtnia = idEtnia;
	}

	@Column(name="Descripcion_Etnia", length = 100)
	public String getDescripcionEtnia() {
		return descripcionEtnia;
	}

	public void setDescripcionEtnia(String descripcionEtnia) {
		this.descripcionEtnia = descripcionEtnia;
	}
	
	
}
