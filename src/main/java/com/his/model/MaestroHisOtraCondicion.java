package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_OTRA_CONDICION")
public class MaestroHisOtraCondicion implements Serializable{

	private int idOtraCondicion;
	private String descripcionOtraCondicion;
	
	
	public MaestroHisOtraCondicion(int idOtraCondicion, String descripcionOtraCondicion) {
		super();
		this.idOtraCondicion = idOtraCondicion;
		this.descripcionOtraCondicion = descripcionOtraCondicion;
	}
	
	public MaestroHisOtraCondicion() {}

	
	@Id
	@Column(name="Id_Otra_Condicion",nullable = false)
	public int getIdOtraCondicion() {
		return idOtraCondicion;
	}

	@Column(name="Descripcion_Otra_Condicion", length = 300)
	
	public String getDescripcionOtraCondicion() {
		return descripcionOtraCondicion;
	}

	public void setIdOtraCondicion(int idOtraCondicion) {
		this.idOtraCondicion = idOtraCondicion;
	}

	public void setDescripcionOtraCondicion(String descripcionOtraCondicion) {
		this.descripcionOtraCondicion = descripcionOtraCondicion;
	}
	
	
	
}
