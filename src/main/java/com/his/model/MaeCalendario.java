package com.his.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="mae_calendario")
public class MaeCalendario implements Serializable{
	
	private int id;
	private String title; 
	private String descripcion;
	private Date start; 
	private Date end;
	private String color ;
	
	
	public MaeCalendario(int id, String title, String descripcion, Date start, Date end,
			String color) {
		super();
		this.id = id;
		this.title = title;
		this.descripcion = descripcion;
		this.start = start;
		this.end = end;
		this.color = color;
	}

	public MaeCalendario() {}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	@Column(name="titulo",length = 50)
	public String getTitle() {
		return title;
	}

	@Column(name="descripcion",length = 500)
	public String getDescripcion() {
		return descripcion;
	}

	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(timezone = "GMT-05:00",pattern="yyyy-MM-dd HH:mm")
	//@JsonFormat(timezone = "GMT-05:00",pattern="dd/MM/yyyy HH:mm") 
	@Column(name = "f_comienzo", length = 23)
	public Date getStart() {
		return start;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(timezone = "GMT-05:00", pattern="yyyy-MM-dd HH:mm")
	//@JsonFormat(timezone = "GMT-05:00",pattern="dd/MM/yyyy HH:mm") 
	@Column(name = "f_final", length = 23)
	public Date getEnd() {
		return end;
	}

	@Column(name="color",length = 10)
	public String getColor() {
		return color;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	
	
}



