package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_UPS")
public class MaestroHisUps implements Serializable {// serializable sirve para la conversion del estado de un objeto en un flujo de bytes y eso poder alamcenar en la bd  
													//Facilita el alamcenamoentpo u elñ envio de objeto
	
	private String idUps;
	
	private String descripcionUps;
	
	public MaestroHisUps() {}

	public MaestroHisUps(String idUps, String descripcionUps) {
		super();
		this.idUps = idUps;
		this.descripcionUps = descripcionUps;
	}

	@Id
	@Column(name="Id_Ups",nullable = false, length = 6)
	public String getIdUps() {
		return idUps;
	}

	public void setIdUps(String idUps) {
		this.idUps = idUps;
	}

	@Column(name="Descripcion_Ups", length = 100)
	public String getDescripcionUps() {
		return descripcionUps;
	}

	public void setDescripcionUps(String descripcionUps) {
		this.descripcionUps = descripcionUps;
	}
	
	
}		
