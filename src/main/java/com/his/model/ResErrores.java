package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="res_errores")
public class ResErrores  implements Serializable{
	
	public int id;
	public MaeCitas idCita;
	public MaeErrores idError;
	public String anio;
	public String mes;
	public Boolean estado;
	
	
	public ResErrores(int id, MaeCitas idCita, MaeErrores idError, String anio, String mes,Boolean estado) {
		super();
		this.id = id;
		this.idCita = idCita;
		this.idError = idError;
		this.anio = anio;
		this.mes = mes;
		this.estado = estado;
	}

	public ResErrores() {}
	
	@Id
	@Column(name="id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCita")
	//@Column(name="idCita",length = 50)
	public MaeCitas getIdCita() {
		return idCita;
	}

	//@Column(name="idError", length = 4)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idError")
	public MaeErrores getIdError() {
		return idError;
	}

	@Column(name="año", length = 4)
	public String getAnio() {
		return anio;
	}

	@Column(name="mes",length = 2)
	public String getMes() {
		return mes;
	}


	public void setId(int id) {
		this.id = id;
	}


	public void setIdCita(MaeCitas idCita) {
		this.idCita = idCita;
	}


	public void setIdError(MaeErrores idError) {
		this.idError = idError;
	}


	public void setAnio(String anio) {
		this.anio = anio;
	}


	public void setMes(String mes) {
		this.mes = mes;
	}

	@Column(name = "estado")
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
}
