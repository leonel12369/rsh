package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_TIPO_DOC")
public class MaestroHisTipoDoc implements Serializable{
	
	private int idTipoDocumento;
	private String abrevTipoDoc;
	private String descripcionTipoDocumento;

	public MaestroHisTipoDoc(int idTipoDocumento, String abrevTipoDoc, String descripcionTipoDocumento) {
		super();
		this.idTipoDocumento = idTipoDocumento;
		this.abrevTipoDoc = abrevTipoDoc;
		this.descripcionTipoDocumento = descripcionTipoDocumento;
	}
	
	public MaestroHisTipoDoc() {}

	@Id
	@Column(name="Id_Tipo_Documento",nullable = false)
	public int getIdTipoDocumento() {
		return idTipoDocumento;
	}

	public void setIdTipoDocumento(int idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	
	@Column(name="Abrev_Tipo_Doc", length = 20)
	public String getAbrevTipoDoc() {
		return abrevTipoDoc;
	}

	public void setAbrevTipoDoc(String abrevTipoDoc) {
		this.abrevTipoDoc = abrevTipoDoc;
	}
	
	
	@Column(name="Descripcion_Tipo_Documento", length = 250)
	public String getDescripcionTipoDocumento() {
		return descripcionTipoDocumento;
	}

	public void setDescripcionTipoDocumento(String descripcionTipoDocumento) {
		this.descripcionTipoDocumento = descripcionTipoDocumento;
	}
	
	
}
