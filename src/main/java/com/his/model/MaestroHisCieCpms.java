package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_CIE_CPMS")
public class MaestroHisCieCpms implements Serializable{

	private String codigoItem;
	private String descripcionItem;
	private String fgTipo;
	private String descripcionTipoItem;
	private int fgEstado;
	
	public MaestroHisCieCpms(String codigoItem, String descripcionItem, String fgTipo, String descripcionTipoItem,
			int fgEstado) {
		super();
		this.codigoItem = codigoItem;
		this.descripcionItem = descripcionItem;
		this.fgTipo = fgTipo;
		this.descripcionTipoItem = descripcionTipoItem;
		this.fgEstado = fgEstado;
	}
	
	public MaestroHisCieCpms() {}

	@Id
	@Column(name="Codigo_Item",nullable = false, length = 15)
	public String getCodigoItem() {
		return codigoItem;
	}

	@Column(name="Descripcion_Item",length = 300)
	public String getDescripcionItem() {
		return descripcionItem;
	}

	@Column(name="Fg_Tipo", length = 2)
	public String getFgTipo() {
		return fgTipo;
	}

	@Column(name="Descripcion_Tipo_Item", length = 100)
	public String getDescripcionTipoItem() {
		return descripcionTipoItem;
	}

	@Column(name="Fg_Estado")
	public int getFgEstado() {
		return fgEstado;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}

	public void setFgTipo(String fgTipo) {
		this.fgTipo = fgTipo;
	}

	public void setDescripcionTipoItem(String descripcionTipoItem) {
		this.descripcionTipoItem = descripcionTipoItem;
	}

	public void setFgEstado(int fgEstado) {
		this.fgEstado = fgEstado;
	}
	
	
	
}
