package com.his.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "mae_rol")
public class MaeRol implements java.io.Serializable {

	private int idRol;
	private MaeUsuario usuario;
	private String nombre;

	public MaeRol() {
	}

	public MaeRol(int idRol) {
		this.idRol = idRol;
	}

	public MaeRol(int idRol, MaeUsuario usuario, String nombre) {
		this.idRol = idRol;
		this.usuario = usuario;
		this.nombre = nombre;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_rol", unique = true, nullable = false)
	public int getIdRol() {
		return this.idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	//@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario")
	public MaeUsuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(MaeUsuario usuario) {
		this.usuario = usuario;
	}

	@Column(name = "nombre", length = 50)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
