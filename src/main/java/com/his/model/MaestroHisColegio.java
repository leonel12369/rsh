package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_COLEGIO")
public class MaestroHisColegio implements Serializable {

	private String idColegio;
	
	private String descripcionColegio;

	public MaestroHisColegio(String idColegio, String descripcionColegio) {
		super();
		this.idColegio = idColegio;
		this.descripcionColegio = descripcionColegio;
	}
	
	public MaestroHisColegio() {}

	@Id
	@Column(name="Id_Colegio",nullable = false, length = 2)
	public String getIdColegio() {
		return idColegio;
	}

	public void setIdColegio(String idColegio) {
		this.idColegio = idColegio;
	}

	@Column(name="Descripcion_Colegio", length = 800)
	public String getDescripcionColegio() {
		return descripcionColegio;
	}

	public void setDescripcionColegio(String descripcionColegio) {
		this.descripcionColegio = descripcionColegio;
	}
	
	
	
}
