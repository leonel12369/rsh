package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_PAIS")
public class MaestroHisPais implements Serializable {

	private String idPais;
	private String descripcionPais;
	
	public MaestroHisPais(String idPais, String descripcionPais) {
		super();
		this.idPais = idPais;
		this.descripcionPais = descripcionPais;
	}
	
	public MaestroHisPais() {}

	@Id
	@Column(name="Id_Pais",nullable = false, length = 3)
	public String getIdPais() {
		return idPais;
	}

	@Column(name="Descripcion_Pais",length = 300)
	public String getDescripcionPais() {
		return descripcionPais;
	}

	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}

	public void setDescripcionPais(String descripcionPais) {
		this.descripcionPais = descripcionPais;
	}
	
	
	
}
