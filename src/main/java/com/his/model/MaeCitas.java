package com.his.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mae_citas")
public class MaeCitas implements Serializable {


	private String idCita;
	private Date fechaAtencion;
	private String lote;
	private int numPag;
	private int numReg;
	private MaestroHisUps  idUps;////-------------------------->datos desiguales bloc de notas
	private MaestroHisEstablecimiento idEstablecimiento;
	private MaestroPaciente idPaciente;
	private MaestroPersonal idPersonal;
	private MaestroRegistrador idRegistrador;
	private MaestroHisFinanciador idFinanciador;
	private String idCondicionEstablecimiento;
	private String idCondicionServicio;
	private int edadReg;
	private String tipoEdad;
	private int anioActualPaciente;
	private int mesActualPaciente;
	private int diaActualPaciente;
	private String idTurno;
	//private MaestroHisCieCpms codigoItem;
	
	public MaeCitas(String idCita, Date fechaAtencion, String lote, int numPag, int numReg, MaestroHisUps idUps,
			MaestroHisEstablecimiento idEstablecimiento, MaestroPaciente idPaciente, MaestroPersonal idPersonal,
			MaestroRegistrador idRegistrador, MaestroHisFinanciador idFinanciador, String idCondicionEstablecimiento,
			String idCondicionServicio, int edadReg, String tipoEdad, int anioActualPaciente, int mesActualPaciente,
			int diaActualPaciente, String idTurno/*,MaestroHisCieCpms codigoItem*/) {
		super();
		this.idCita = idCita;
		this.fechaAtencion = fechaAtencion;
		this.lote = lote;
		this.numPag = numPag;
		this.numReg = numReg;
		this.idUps = idUps;
		this.idEstablecimiento = idEstablecimiento;
		this.idPaciente = idPaciente;
		this.idPersonal = idPersonal;
		this.idRegistrador = idRegistrador;
		this.idFinanciador = idFinanciador;
		this.idCondicionEstablecimiento = idCondicionEstablecimiento;
		this.idCondicionServicio = idCondicionServicio;
		this.edadReg = edadReg;
		this.tipoEdad = tipoEdad;
		this.anioActualPaciente = anioActualPaciente;
		this.mesActualPaciente = mesActualPaciente;
		this.diaActualPaciente = diaActualPaciente;
		this.idTurno = idTurno;
		//this.codigoItem=codigoItem;
	}
	
	public MaeCitas() {}
	

	
	@Id
	@Column(name="Id_Cita",length = 50, nullable = false)
	public String getIdCita() {
		return idCita;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "Fecha_Atencion", length = 23)
	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	@Column(name="Lote",length = 3)
	public String getLote() {
		return lote;
	}

	@Column(name="Num_Pag")
	public int getNumPag() {
		return numPag;
	}

	@Column(name="Num_Reg")
	public int getNumReg() {
		return numReg;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Ups")
	//@Column(name="Id_Ups",length = 6)
	public MaestroHisUps getIdUps() {
		return idUps;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Establecimiento")
	//@Column(name="Id_Establecimiento")
	public MaestroHisEstablecimiento getIdEstablecimiento() {
		return idEstablecimiento;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Paciente")
	//@Column(name="Id_Paciente",length = 50)
	public MaestroPaciente getIdPaciente() {
		return idPaciente;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Personal")
	//@Column(name="Id_Personal",length = 50)
	public MaestroPersonal getIdPersonal() {
		return idPersonal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Registrador")
	//@Column(name="Id_Registrador",length = 50)
	public MaestroRegistrador getIdRegistrador() {
		return idRegistrador;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Id_Financiador")
	//@Column(name="Id_Financiador",length = 2)
	public MaestroHisFinanciador getIdFinanciador() {
		return idFinanciador;
	}

	@Column(name="Id_Condicion_Establecimiento",length = 1)
	public String getIdCondicionEstablecimiento() {
		return idCondicionEstablecimiento;
	}

	@Column(name="Id_Condicion_Servicio",length = 1)
	public String getIdCondicionServicio() {
		return idCondicionServicio;
	}

	@Column(name="Edad_Reg")
	public int getEdadReg() {
		return edadReg;
	}

	@Column(name="Tipo_Edad",length = 1)
	public String getTipoEdad() {
		return tipoEdad;
	}

	@Column(name="Anio_Actual_Paciente")
	public int getAnioActualPaciente() {
		return anioActualPaciente;
	}
	
	@Column(name="Mes_Actual_Paciente")
	public int getMesActualPaciente() {
		return mesActualPaciente;
	}
	
	@Column(name="Dia_Actual_Paciente")
	public int getDiaActualPaciente() {
		return diaActualPaciente;
	}

	@Column(name="Id_Turno",length = 1)
	public String getIdTurno() {
		return idTurno;
	}

	public void setIdCita(String idCita) {
		this.idCita = idCita;
	}


	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setNumPag(int numPag) {
		this.numPag = numPag;
	}

	public void setNumReg(int numReg) {
		this.numReg = numReg;
	}

	public void setIdUps(MaestroHisUps idUps) {
		this.idUps = idUps;
	}

	public void setIdEstablecimiento(MaestroHisEstablecimiento idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public void setIdPaciente(MaestroPaciente idPaciente) {
		this.idPaciente = idPaciente;
	}

	public void setIdPersonal(MaestroPersonal idPersonal) {
		this.idPersonal = idPersonal;
	}

	public void setIdRegistrador(MaestroRegistrador idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public void setIdFinanciador(MaestroHisFinanciador idFinanciador) {
		this.idFinanciador = idFinanciador;
	}

	public void setIdCondicionEstablecimiento(String idCondicionEstablecimiento) {
		this.idCondicionEstablecimiento = idCondicionEstablecimiento;
	}

	public void setIdCondicionServicio(String idCondicionServicio) {
		this.idCondicionServicio = idCondicionServicio;
	}

	public void setEdadReg(int edadReg) {
		this.edadReg = edadReg;
	}

	public void setTipoEdad(String tipoEdad) {
		this.tipoEdad = tipoEdad;
	}

	public void setAnioActualPaciente(int anioActualPaciente) {
		this.anioActualPaciente = anioActualPaciente;
	}

	public void setMesActualPaciente(int mesActualPaciente) {
		this.mesActualPaciente = mesActualPaciente;
	}

	public void setDiaActualPaciente(int diaActualPaciente) {
		this.diaActualPaciente = diaActualPaciente;
	}

	public void setIdTurno(String idTurno) {
		this.idTurno = idTurno;
	}

/*	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Codigo_Item")
	//@Column(name="Id_Establecimiento")
	public MaestroHisCieCpms getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(MaestroHisCieCpms codigoItem) {
		this.codigoItem = codigoItem;
	}
*/
	
	
	

	

	

	

	
	
	
}
