package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_CONDICION_CONTRATO")
public class MaestroHisCondicionContrato  implements Serializable{

	public String idCondicion;
	
	public String descripcion_Condicion;

	public MaestroHisCondicionContrato(String idCondicion, String descripcion_Condicion) {
		super();
		this.idCondicion = idCondicion;
		this.descripcion_Condicion = descripcion_Condicion;
	}
	
	public MaestroHisCondicionContrato() {}

	@Id
	@Column(name="Id_Condicion",nullable = false, length = 2)
	public String getIdCondicion() {
		return idCondicion;
	}

	public void setIdCondicion(String idCondicion) {
		this.idCondicion = idCondicion;
	}

	@Column(name="Descripcion_Condicion", length = 500)
	public String getDescripcion_Condicion() {
		return descripcion_Condicion;
	}

	public void setDescripcion_Condicion(String descripcion_Condicion) {
		this.descripcion_Condicion = descripcion_Condicion;
	}
	
	
	
}
