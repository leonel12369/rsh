package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_FINANCIADOR")
public class MaestroHisFinanciador  implements Serializable{
	
	private String idFinanciador;
	
	private String descripcionFinanciador;
	
	
	public MaestroHisFinanciador() {}

	@Id
	@Column(name="Id_Financiador",nullable = false, length = 2)
	public String getIdFinanciador() {
		return idFinanciador;
	}


	public void setIdFinanciador(String idFinanciador) {
		this.idFinanciador = idFinanciador;
	}

	@Column(name="Descripcion_Financiador", length = 100)
	public String getDescripcionFinanciador() {
		return descripcionFinanciador;
	}


	public void setDescripcionFinanciador(String descripcionFinanciador) {
		this.descripcionFinanciador = descripcionFinanciador;
	}


	public MaestroHisFinanciador(String idFinanciador, String descripcionFinanciador) {
		super();
		this.idFinanciador = idFinanciador;
		this.descripcionFinanciador = descripcionFinanciador;
	}
	
	

}

