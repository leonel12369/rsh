package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name="mae_errores")
public class MaeErrores  implements Serializable{

	public String idError;
	public MaeEstrategiaSanitaria idEstrategia;
	public String descripcionCorta;
	public String descripcionLarga;
	public String sintaxis;
	public String condicion;
	public Boolean estado;
	
	public MaeErrores(String idError, MaeEstrategiaSanitaria idEstrategia, String descripcionCorta, String descripcionLarga,
			String sintaxis,String condicion,Boolean estado) {
		super();
		this.idError = idError;
		this.idEstrategia = idEstrategia;
		this.descripcionCorta = descripcionCorta;
		this.descripcionLarga = descripcionLarga;
		this.sintaxis = sintaxis;
		this.condicion = condicion;
		this.estado = estado;
	}
	
	public MaeErrores() {}

	@Id
	@Column(name="idError",nullable = false, length = 4)//corregido---------------------
	public String getIdError() {
		return idError;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idEstrategia")
	//@Column(name="idEstrategia", length = 4)
	public MaeEstrategiaSanitaria getIdEstrategia() {
		return idEstrategia;
	}

	@Column(name="descripcionCorta", length = 500)
	public String getDescripcionCorta() {
		return descripcionCorta;
	}

	@Column(name="descripcionLarga", length = 500)
	public String getDescripcionLarga() {
		return descripcionLarga;
	}

	@Column(name="sintaxis", length =  500)
	public String getSintaxis() {
		return sintaxis;
	}

	public void setIdError(String idError) {
		this.idError = idError;
	}

	public void setIdEstrategia(MaeEstrategiaSanitaria idEstrategia) {
		this.idEstrategia = idEstrategia;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}

	public void setSintaxis(String sintaxis) {
		this.sintaxis = sintaxis;
	}

	
	@Column(name="condicion", length = 5)
	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	
	
	@Column(name = "estado")
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
	
}
