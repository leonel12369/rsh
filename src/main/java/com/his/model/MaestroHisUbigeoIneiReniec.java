package com.his.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MAESTRO_HIS_UBIGEO_INEI_RENIEC")
public class MaestroHisUbigeoIneiReniec implements Serializable{

	
	private String idUbigeoInei;
	private String idUbigeoReniec;
	private String departamento;
	private String provincia;
	private String distrito;
	private String codDepInei;
	private String codProvInei;
	private String codDistInei;
	private String codDepReniec;
	private String codProvReniec;
	private String codDistReniec;
	
	public MaestroHisUbigeoIneiReniec(String idUbigeoInei, String idUbigeoReniec, String departamento, String provincia,
			String distrito, String codDepInei, String codProvInei, String codDistInei, String codDepReniec,
			String codProvReniec, String codDistReniec) {
		super();
		this.idUbigeoInei = idUbigeoInei;
		this.idUbigeoReniec = idUbigeoReniec;
		this.departamento = departamento;
		this.provincia = provincia;
		this.distrito = distrito;
		this.codDepInei = codDepInei;
		this.codProvInei = codProvInei;
		this.codDistInei = codDistInei;
		this.codDepReniec = codDepReniec;
		this.codProvReniec = codProvReniec;
		this.codDistReniec = codDistReniec;
	}
	
	public MaestroHisUbigeoIneiReniec() {}

	@Id
	@Column(name="IdUbigeoInei",nullable = false, length = 6)
	public String getIdUbigeoInei() {
		return idUbigeoInei;
	}

	@Column(name="IdUbigeoReniec",nullable = false, length = 6)
	public String getIdUbigeoReniec() {
		return idUbigeoReniec;
	}

	@Column(name="Departamento",nullable = false, length = 100)
	public String getDepartamento() {
		return departamento;
	}

	@Column(name="Provincia",nullable = false, length = 100)
	public String getProvincia() {
		return provincia;
	}

	@Column(name="Distrito",nullable = false, length = 100)
	public String getDistrito() {
		return distrito;
	}

	@Column(name="CodDepInei",length = 2)
	public String getCodDepInei() {
		return codDepInei;
	}

	@Column(name="CodProvInei",length = 2)
	public String getCodProvInei() {
		return codProvInei;
	}

	@Column(name="CodDistInei",length = 2)
	public String getCodDistInei() {
		return codDistInei;
	}

	@Column(name="CodDepReniec",length = 2)
	public String getCodDepReniec() {
		return codDepReniec;
	}

	@Column(name="CodProvReniec",length = 2)
	public String getCodProvReniec() {
		return codProvReniec;
	}

	@Column(name="CodDistReniec",length = 2)
	public String getCodDistReniec() {
		return codDistReniec;
	}

	public void setIdUbigeoInei(String idUbigeoInei) {
		this.idUbigeoInei = idUbigeoInei;
	}

	public void setIdUbigeoReniec(String idUbigeoReniec) {
		this.idUbigeoReniec = idUbigeoReniec;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public void setCodDepInei(String codDepInei) {
		this.codDepInei = codDepInei;
	}

	public void setCodProvInei(String codProvInei) {
		this.codProvInei = codProvInei;
	}

	public void setCodDistInei(String codDistInei) {
		this.codDistInei = codDistInei;
	}

	public void setCodDepReniec(String codDepReniec) {
		this.codDepReniec = codDepReniec;
	}

	public void setCodProvReniec(String codProvReniec) {
		this.codProvReniec = codProvReniec;
	}

	public void setCodDistReniec(String codDistReniec) {
		this.codDistReniec = codDistReniec;
	}
	
	
	
}
