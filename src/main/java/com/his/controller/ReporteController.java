package com.his.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.List;

import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.his.model.ResErrores;
import com.his.reporte.CSVService;
import com.his.reporte.ExcelService;
import com.his.service.ErroresService;

@Controller
@RequestMapping("/reporte")
public class ReporteController {

	@Autowired
	private ExcelService excelService;
	
	@Autowired
	private ErroresService erroresService;
	
	@Autowired
	private CSVService CSVService;
	
	//---------------------------------------- excel-----------------------------------------------
	 @RequestMapping(value = "/excelEstablecimiento", method = RequestMethod.GET)
	 public ResponseEntity<InputStreamResource> exportExcel(
				@RequestParam(value="establecimiento")int establecimiento,
				@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="idPrograma")int idPrograma,
				@RequestParam(value="estado")Boolean estado,
				@RequestParam(value="tipoRegistrador")int tipoRegistrador){
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltrados(estado,tipoRegistrador, idPrograma, establecimiento, anio, mes);
		 ByteArrayInputStream stream =excelService.exportExcelFiltros(lista);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Content-Disposition", "attachment; filename=Establecimiento.xls");
		 return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	 }
	 
	 
	 @RequestMapping(value = "/excelUser", method = RequestMethod.GET)
	 public ResponseEntity<InputStreamResource> exportExcelRegistrador(
				@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="dni")String dni,
				@RequestParam(value="idPrograma")int idPrograma,
				@RequestParam(value="estado")Boolean estado,
				@RequestParam(value="tipoRegistrador")int tipoRegistrador){
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltradosUser(estado,tipoRegistrador, idPrograma, anio, mes, dni);
		 ByteArrayInputStream stream =excelService.exportExcelFiltros(lista);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Content-Disposition", "attachment; filename=Registrador.xls");
		 return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	 }
	 
	 
	 @RequestMapping(value = "/excelPersonal", method = RequestMethod.GET)
	 public ResponseEntity<InputStreamResource> exportExcelPersonal(
				@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="dni")String dni,
				@RequestParam(value="estado")Boolean estado,
				@RequestParam(value="idPrograma")int idPrograma){
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltradosPersonal(estado,idPrograma, anio, mes, dni);
		 ByteArrayInputStream stream =excelService.exportExcelFiltros(lista);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Content-Disposition", "attachment; filename=Personal.xls");
		 return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	 }
	 
	 @RequestMapping(value = "/excelPaciente", method = RequestMethod.GET)
	 public ResponseEntity<InputStreamResource> exportExcelPaciente(
				@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="dni")String dni,
				@RequestParam(value="estado")Boolean estado){
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltradosPaciente(estado, anio, mes, dni);
		 ByteArrayInputStream stream =excelService.exportExcelFiltros(lista);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add("Content-Disposition", "attachment; filename=Paciente.xls");
		 return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	 }
	 
	 
	//---------------------------------------- CSV-----------------------------------------------
	 /*@GetMapping("/csvEstablecimiento")
	 public void exportCSVEstablecimiento(HttpServletResponse response,
			 @RequestParam(value="establecimiento")int establecimiento,
			 @RequestParam(value="anio")String anio,
			 @RequestParam(value="mes")String mes,
			 @RequestParam(value="idPrograma")int idPrograma,
			 @RequestParam(value="tipoRegistrador")int tipoRegistrador ) throws IOException {
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltrados(tipoRegistrador, idPrograma, establecimiento, anio, mes);
		 
		 response.setContentType("text/csv");
		 String filename="establecimiento.csv";
		 String headerKey="Content-Disposition";
		 String headerValue="attachment; filename="+ filename;
		 response.setHeader(headerKey,headerValue);
		 
		 ICsvBeanWriter csvWriter= new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		 String [] csvHeader= {"N#","Nombre del Personal","Apellido Paterno del Personal","Apellido Materno del Personal","Fecha Atencion","Lote","Num Pag","Num Reg","ERROR descripcion corta","ERROR descripcion completa","Programa de estrategia","Descripcion de estrategia","Deber ser "};
		 String [] nameMapping= {"id","idCita"};
		 csvWriter.writeHeader(csvHeader);
		 
		 for(ResErrores i: lista) {
			 csvWriter.write(i, nameMapping);
		 }
		 csvWriter.close();
	 }*/
	 
	 @GetMapping("/csvEstablecimiento.csv")
	 public void exportCSVEstablecimiento(HttpServletResponse response,
			 @RequestParam(value="establecimiento")int establecimiento,
			 @RequestParam(value="anio")String anio,
			 @RequestParam(value="mes")String mes,
			 @RequestParam(value="idPrograma")int idPrograma,
			 @RequestParam(value="estado")Boolean estado,
			 @RequestParam(value="tipoRegistrador")int tipoRegistrador ) throws IOException {
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltrados(estado,tipoRegistrador, idPrograma, establecimiento, anio, mes);
		 
		 response.setContentType("text/csv");
	     response.setHeader("Content-Disposition", "attachment; file=employee.csv");
	     
	     CSVService.downloadCsv(response.getWriter(), lista);
	 }
	 
	 @GetMapping("/csvRegistrador.csv")
	 public void exportCSVRegistrador(HttpServletResponse response,
			 	@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="dni")String dni,
				@RequestParam(value="idPrograma")int idPrograma,
				@RequestParam(value="estado")Boolean estado,
				@RequestParam(value="tipoRegistrador")int tipoRegistrador ) throws IOException {
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltradosUser(estado,tipoRegistrador, idPrograma, anio, mes, dni);
		 
		 response.setContentType("text/csv");
	     response.setHeader("Content-Disposition", "attachment; file=employee.csv");
	     
	     CSVService.downloadCsv(response.getWriter(), lista);
	 }
	 
	 @GetMapping("/csvPersonal.csv")
	 public void exportCSVPersonal(HttpServletResponse response,
			 	@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="dni")String dni,
				@RequestParam(value="estado")Boolean estado,
				@RequestParam(value="idPrograma")int idPrograma) throws IOException {
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltradosPersonal(estado,idPrograma, anio, mes, dni);
		 
		 response.setContentType("text/csv");
	     response.setHeader("Content-Disposition", "attachment; file=employee.csv");
	     
	     CSVService.downloadCsv(response.getWriter(), lista);
	 }
	 
	 @GetMapping("/csvPaciente.csv")
	 public void exportCSVPaciente(HttpServletResponse response,
			 	@RequestParam(value="anio")String anio,
				@RequestParam(value="mes")String mes,
				@RequestParam(value="dni")String dni,
				@RequestParam(value="estado")Boolean estado) throws IOException {
		 
		 List<ResErrores> lista=erroresService.listaErroresFiltradosPaciente(estado, anio, mes, dni);
		 
		 response.setContentType("text/csv");
	     response.setHeader("Content-Disposition", "attachment; file=employee.csv");
	     
	     CSVService.downloadCsv(response.getWriter(), lista);
	 }
}
