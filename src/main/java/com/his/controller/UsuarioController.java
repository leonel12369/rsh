package com.his.controller;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.persistence.PrePersist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.his.model.MaeRol;
import com.his.model.MaeUsuario;
import com.his.paginador.PageRender;
import com.his.service.UsuarioService;




@Controller
@RequestMapping("/usuario")
@SessionAttributes("usuario")
public class UsuarioController {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/crear")
	public String crear(Model model) {
		model.addAttribute("titulo","Crear Usuario");
		return "views/usuario/creacionUsuario";
	}
	

	@RequestMapping(value="/guardar", method=RequestMethod.POST,consumes={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String guardar(@RequestBody Map<String, Object> json) {
		System.out.println(json);
		if(json.get("idUsuario")==null) {
			String nombre= (String)json.get("nombre");
			String apellidoPaterno= (String) json.get("apellidoPaterno");
			String apellidoMaterno= (String) json.get("apellidoMaterno");
			String nombreUsuario= (String) json.get("nombreUsuario");
			String contrasenia= (String) json.get("contrasenia");
			List<String>listRol= (List<String>) json.get("listRol");
			
			MaeUsuario usuario=new MaeUsuario();
			usuario.setNombre(nombre);
			usuario.setApellidoPaterno(apellidoPaterno);
			usuario.setApellidoMaterno(apellidoMaterno);
			usuario.setNombreUsuario(nombreUsuario);
			contrasenia=passwordEncoder.encode(contrasenia);
			usuario.setContrasenia(contrasenia);
			//usuario.setIdUsuario(5);
			usuarioService.save(usuario);
			
			for(int i=0;i<listRol.size();i++) {
				MaeRol rol=new MaeRol();
				rol.setNombre(listRol.get(i));
				rol.setUsuario(usuario);
				usuarioService.saveRol(rol);
			}
		}
		else {
			int idUsuario= (int) json.get("idUsuario");
			
			String nombre= (String)json.get("nombre");
			String apellidoPaterno= (String) json.get("apellidoPaterno");
			String apellidoMaterno= (String) json.get("apellidoMaterno");
			String nombreUsuario= (String) json.get("nombreUsuario");
			String contrasenia= (String) json.get("contrasenia");
			List<String>listRol= (List<String>) json.get("listRol");
			
			
			MaeUsuario usuario= usuarioService.findUsuario(idUsuario);
			usuario.setNombre(nombre);
			usuario.setApellidoPaterno(apellidoPaterno);
			usuario.setApellidoMaterno(apellidoMaterno);
			usuario.setNombreUsuario(nombreUsuario);
			if(contrasenia.length()>0) {
				contrasenia=passwordEncoder.encode(contrasenia);
				usuario.setContrasenia(contrasenia);
			}
			usuarioService.save(usuario);
			
			
			List<String> lista_borrar=usuarioService.listaStringRol(idUsuario);
			List<String> lista_guardar= new ArrayList<String>();
			
			for(int i=0;i<listRol.size();i++) {
				lista_guardar.add(listRol.get(i));
			}
			System.out.println("lista_rol: "+lista_guardar);
			/*------------------------------------INTERSECCION DE LISTAS-------------------------------------------------*/
			List<String> interseccion=new ArrayList<String>();
			for(int i=0;i<lista_borrar.size();i++) {
				if(lista_guardar.contains(lista_borrar.get(i))) {
					interseccion.add(lista_borrar.get(i));
				}
			}
			System.out.println("-----------------------------------------------");
			System.out.println("lista_borrar: "+lista_borrar);
			System.out.println("lista_guardar: "+lista_guardar);
			System.out.println("interseccion: "+interseccion);
			System.out.println("------------------------------------------------");
			/*-------------------------------Eliminar datos de lista original-------------------------------*/
			List<String> lista_original2=lista_borrar;
			for(int i=0;i<lista_original2.size();i++) {
				for(int j=0;j<interseccion.size();j++) {
					if(interseccion.get(j).equals(lista_original2.get(i))) {
						lista_borrar.remove(i);
					}
				}
			}
			System.out.println("lista_borrar:"+lista_borrar);
			/*-------------------------------Eliminar datos de lista front------------------------*/
			List<String> lista_frond2=lista_guardar;
			for(int i=0;i<lista_frond2.size();i++) {
				for(int j=0;j<interseccion.size();j++) {
					if(interseccion.get(j).equals(lista_frond2.get(i))) {
						lista_guardar.remove(i);
					}
				}
			}
			System.out.println("lista_guardar :"+lista_guardar);
			/*---------------------------------------Editar Rol-----------------------------------------*/
			if(lista_borrar.size()>0) {
				for(int i=0;i<lista_borrar.size();i++) {
					System.out.println("borrar: "+lista_borrar.get(i));
					//usuarioService.saveRol(rol);
					usuarioService.deleteRolEspecifico(lista_borrar.get(i), idUsuario);
				}
			}
			if(lista_guardar.size()>0) {
				for(int i=0;i<lista_guardar.size();i++) {
					System.out.println("guardar: "+lista_guardar.get(i));
					MaeRol rol=new MaeRol();
					rol.setUsuario(usuario);
					rol.setNombre(lista_guardar.get(i));
					usuarioService.saveRol(rol);
				}
			}
			
			
		}
		return "null";
	}
	
	@RequestMapping(value="/verificar", method=RequestMethod.GET)
	public @ResponseBody int validarUserName(@RequestParam(value="username")String username) {
		System.out.println(username);
		int existencia =usuarioService.findUsername(username);
		System.out.println(existencia);
		if(existencia==0) {
			return 0;
		}
		else {
			return 1;
		}
	}
	
	
	@GetMapping("/editar/{id}")
	public String editar(@PathVariable(value="id")int id, Model model) {
		model.addAttribute("titulo","Editar Usuario");
		model.addAttribute("usuario",usuarioService.findUsuario(id));
		model.addAttribute("rol",usuarioService.findRolUsuario(id));
		return "views/usuario/editarUsuario";
	}
	
	
	@GetMapping("/listar")
	public String listarPrueba(Model model) {
		model.addAttribute("titulo","Listar Usuario");
		model.addAttribute("adminAndUser", usuarioService.adminAndUser());
		model.addAttribute("admin", usuarioService.adminOrUser("ROLE_ADMIN"));
		model.addAttribute("user", usuarioService.adminOrUser("ROLE_USER"));
		return "views/usuario/listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id")int id,RedirectAttributes flash) {
		if(id>0) {
			System.out.println(id);
			usuarioService.deleteRol(id);
			usuarioService.deleteUsuario(id);
			//flash.addFlashAttribute("success", "PRODUCTO ELIMINADO");
		}
		return "redirect:/usuario/listar";
	}
}
