package com.his.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.his.model.MaeCalendario;
import com.his.model.MaeEqhali;
import com.his.model.Ubigeo;
import com.his.service.CalendarioService;

@Controller
//@RequestMapping("/calendario")
public class CalendarioController {

	@Autowired
	private CalendarioService calendarioService; 
	
	@GetMapping({"/","/calendario/index"})
	public String index(Model model) {
		Date fecha = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(fecha);
		model.addAttribute("eventos",calendarioService.buscar_mes_anio(calendar.get(Calendar.MONTH) + 1,calendar.get(Calendar.YEAR)));
		//System.out.println(calendar.get(Calendar.MONTH) + 1+" "+ calendar.get(Calendar.YEAR));
		return "views/calendario/calendario";
	}

	@GetMapping("/calendario/calendarioActualizar")
	public String calendario_actualizar(Model model,
			@RequestParam(value="anio")int anio,
			@RequestParam(value="mes")int mes) {
		
		String fecha="";
		if(mes<10) {
			fecha=anio+"-"+"0"+mes+"-"+"01";
		}
		else {
			fecha=anio+"-"+mes+"-"+"01";
		}
		System.out.println(fecha);
		model.addAttribute("fecha",fecha);
		model.addAttribute("eventoss",calendarioService.buscar_mes_anio(mes, anio));
		return "views/calendario/calendario_fragment";
	}
	
	@PostMapping(value="/calendario/calendarioGuardar",consumes={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Map<String, String>   guardar_calendario(@RequestBody Map<String, Object>  json) {
		String id_string=(String)json.get("id");
		int id=Integer.parseInt(id_string);
		String titulo=(String)json.get("titulo");
		String f_inicial=(String)json.get("f_inicial");
		String f_final=(String)json.get("f_final");
		String descripcion=(String)json.get("descripcion");
		
		System.out.println("id: "+id+"titulo: "+titulo+"f_inicial: "+f_inicial +"f_final: "+f_final+"descripcion: "+descripcion);
		HashMap<String, String> map = new HashMap<>();
		SimpleDateFormat formato=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date inicial_f=new Date(),final_f=new Date();
		try {
			inicial_f = formato.parse(f_inicial);
			final_f=formato.parse(f_final);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(id>0) {
			MaeCalendario nuevo = calendarioService.buscar(id);
			nuevo.setTitle(titulo);
			nuevo.setStart(inicial_f);
			nuevo.setEnd(final_f);
			nuevo.setDescripcion(descripcion);
			calendarioService.agregar(nuevo);
			map.put("1","Se edito correctamente");	
			System.out.println("editar");
			//System.out.println("id: "+nuevo.getId()+" titulo: "+nuevo.getTitle()+" f_inicial: "+nuevo.getStart() +" f_final: "+nuevo.getEnd()+" descripcion: "+nuevo.getDescripcion());
		}
		else {
			MaeCalendario calendario=new MaeCalendario();
			calendario.setTitle(titulo);
			calendario.setStart(inicial_f);
			calendario.setEnd(final_f);
			calendario.setDescripcion(descripcion);
			calendarioService.agregar(calendario);
			map.put("2","Se guardo correctamente");	
			System.out.println("guardar");
			//System.out.println("id: "+calendario.getId()+" titulo: "+calendario.getTitle()+" f_inicial: "+calendario.getStart() +" f_final: "+calendario.getEnd()+" descripcion: "+calendario.getDescripcion());
		}
		/*if(objCalendario.getId()>0) {
			
		}
		else {
			//calendarioService.agregar(objCalendario);
			System.out.println("guardado");
		}*/
		
		return map;
	}
	
	@GetMapping("/calendario/editarCalendario/{id}")
	public String editar_calendario(@PathVariable(value="id") int id, Model model) {
		model.addAttribute("objCalendario", calendarioService.buscar(id));
		return "views/calendario/modal";
	}
	
	@GetMapping("/calendario/eliminarCalendario/{id}")
	public String eliminar_calendario(@PathVariable("id")Integer id,RedirectAttributes flash) {
		MaeCalendario objeto=calendarioService.buscar(id);
		if(objeto!=null) {
			calendarioService.eliminar(objeto);
			System.out.println("eliminado");
		} 
		else {
			System.out.println("problemas al eliminar");
		}
		return "redirect:/calendario/index";
	}
}

