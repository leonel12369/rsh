package com.his.controller;

import java.security.Principal;

import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer.RedirectionEndpointConfig;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

	@RequestMapping(value = {"/login"}, method = RequestMethod.GET)
	public String login(@RequestParam(value="error",required=false)String error,
			@RequestParam(value="logout",required=false)String logout, Model model, Principal auth, RedirectAttributes flash,Authentication authentication ) {
		
		//Object principal = SecurityContextHolder.getContext().getAuthentication().getName();
		//System.out.println(auth);
		if(auth != null) {
			flash.addFlashAttribute("info", "Ya has iniciado sesion");
			return "redirect:/calendario/index";
		}
		
		if(error != null) {
			//System.out.println(error);
			model.addAttribute("error", "La contraseña o usuario esta mal escrito");
		}
		
		if(logout != null) {
			model.addAttribute("success", "Cerró sesion correctamente");
		}
			
		return "views/login/login_a";
	}
}
