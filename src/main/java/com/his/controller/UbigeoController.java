package com.his.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.his.model.Ubigeo;
import com.his.service.UbigeoService;

@Controller
@RequestMapping("/ubigeo")
public class UbigeoController {
	
	@Autowired
	private UbigeoService ubigeoService;

	@RequestMapping(value="/cargaProvincia", method=RequestMethod.POST,  consumes={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<Ubigeo> cargaProvincia(@RequestBody Map<String, Object>  json)throws  IOException{
		String departamento=(String)json.get("nombre");
		//System.out.println(departamento);
		return ubigeoService.provincia(departamento);
	} 
	
	@RequestMapping(value="/cargaDistrito", method=RequestMethod.POST, consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Ubigeo> cargaDistrito(@RequestBody Map<String,Object> json){
		//System.out.println(json);
		String departamento=(String)json.get("departamento");
		String provincia=(String)json.get("provincia");
		//System.out.println(departamento+provincia);
		return ubigeoService.distrito(departamento, provincia);
		//return null;
	}
	
	
	@RequestMapping(value="/red", method=RequestMethod.POST, consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Ubigeo> red(@RequestBody Map<String,Object> json){
		String codDisaString=(String)json.get("codDisa");
		int codDisa=Integer.parseInt(codDisaString);
		return ubigeoService.red(codDisa);
	}
	
	
	@RequestMapping(value="/microRed", method=RequestMethod.POST, consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Ubigeo> microRed(@RequestBody Map<String,Object> json){
		
		String codRed=(String)json.get("codRed");
		String codDisaString =(String)json.get("codDisa");
		int codDisa=Integer.parseInt(codDisaString);
		return ubigeoService.microRed(codDisa, codRed);
	}
	
	@RequestMapping(value="/establecimiento", method=RequestMethod.POST, consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Ubigeo> establecimiento(@RequestBody Map<String,Object> json){
		String codRed=(String)json.get("red");
		String codMicroRed=(String)json.get("microRed");
		String codDisaString=(String)json.get("codDisa");
		int codDisa=Integer.parseInt(codDisaString);
		return ubigeoService.establecimiento(codDisa, codRed, codMicroRed);
	}
}
