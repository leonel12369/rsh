package com.his.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.his.dao.MaestroDao;
import com.his.model.MaeEqhali;
import com.his.model.MaeErrores;
import com.his.model.MaeEstrategiaSanitaria;
import com.his.service.MaestroService;






@Controller
@RequestMapping("/maestro")
public class MaestroController {

	@Autowired
	private MaestroService maestroService;
	 
	@GetMapping("/eqhali")
	public String maestro_eqhali(Model model,Map<String,Object> objEQhali) {
		model.addAttribute("eqhali",maestroService.listarEqhali());
		MaeEqhali eqhali= new MaeEqhali();
		objEQhali.put("objEqhali", eqhali); 
		model.addAttribute("aparecer","0");
		return "views/maestro/eqhali/listar";
	}
	
	
	@PostMapping("/eqhaliGuardar")
	public String guardar_eqhali(MaeEqhali objEqhali) {

		if(objEqhali.getId()>0) {
			MaeEqhali nuevo = maestroService.buscar_maeEqhali(objEqhali.getId());
			nuevo.setNombreLote(objEqhali.getNombreLote());
			nuevo.setDescripcionModulo(objEqhali.getDescripcionModulo());
			nuevo.setGrupo(objEqhali.getGrupo());
			maestroService.save_maeEqhali(nuevo);
			System.out.println("editado correctamente");
		}
		else {
			maestroService.save_maeEqhali(objEqhali);
			System.out.println("guardado");
		}
		
		return "redirect:/maestro/eqhali";
	}
	
	@GetMapping("/editarEqhali/{id}")
	public String editar_eqhali(@PathVariable(value="id") int id, Model model) {
		model.addAttribute("objEqhali", maestroService.buscar_maeEqhali(id));
		return "views/maestro/eqhali/modal";
	}
	
	@GetMapping("/eliminarEqhali/{id}")
	public String eliminar_eqhali(@PathVariable("id")Integer id,RedirectAttributes flash) {
		MaeEqhali objeto=maestroService.buscar_maeEqhali(id);
		if(objeto!=null) {
			objeto.setEstado(false);
			maestroService.eliminar_maeEqhali(objeto);
			System.out.println("eliminado");
		} 
		else {
			System.out.println("problemas al eliminar");
		}
		return "redirect:/maestro/eqhali";
	}
	
	
	@GetMapping("/estrategia")
	public String maestro_estrategia(Model model,Map<String,Object> objEstrategia) {
		model.addAttribute("estrategia",maestroService.listarEstrategia());
		MaeEstrategiaSanitaria estrategia= new MaeEstrategiaSanitaria();
		objEstrategia.put("objEstrategia", estrategia); 
		return "views/maestro/estrategia/listar";
	}
	
	
	@PostMapping("/estrategiaGuardar")
	public String guardar_estrategia(MaeEstrategiaSanitaria objEstrategia) {
		if(objEstrategia.getIdEstrategia()>0) {
			MaeEstrategiaSanitaria nuevo = maestroService.buscar_estrategia(objEstrategia.getIdEstrategia());
			nuevo.setDescripcionEstrategia(objEstrategia.getDescripcionEstrategia());
			nuevo.setPrograma(objEstrategia.getPrograma());;
			maestroService.save_estrategia(objEstrategia);
			System.out.println("editado correctamente");
		}
		else {
			System.out.println("id: "+objEstrategia.getIdEstrategia());
			maestroService.save_estrategia(objEstrategia);
			System.out.println("guardado");
		}
		
		return "redirect:/maestro/estrategia";
	}
	
	
	@GetMapping("/editarEstrategia/{id}")
	public String editar_estrategia(@PathVariable(value="id") int id, Model model) {
		model.addAttribute("objEstrategia", maestroService.buscar_estrategia(id));
		return "views/maestro/estrategia/modal";
	}
	
	@GetMapping("/eliminarEstrategia/{id}")
	public String eliminar_estrategia(@PathVariable("id")Integer id,RedirectAttributes flash) {
		MaeEstrategiaSanitaria objeto=maestroService.buscar_estrategia(id);
		if(objeto!=null) {
			objeto.setEstado(false);
			maestroService.eliminar_estrategia(objeto);
			System.out.println("eliminado");
		} 
		else {
			System.out.println("problemas al eliminar");
		}
		return "redirect:/maestro/estrategia";
	}
	
	
	@GetMapping("/error")
	public String maestro_error(Model model,Map<String,Object> objError)  {
		model.addAttribute("error",maestroService.listarError());
		
		//List<MaeEstrategiaSanitaria> lista= maestroService.listarEstrategia();

		model.addAttribute("combo",maestroService.listarEstrategia());
		MaeErrores error= new MaeErrores();
		objError.put("objError", error); 
		return "views/maestro/error/listar";
	}
	
	
	@PostMapping("/errorGuardar")
	public String guardar_error(MaeErrores objError) {
		System.out.println("idError:"+objError.getIdError());
		if(!objError.getIdError().equals("")) {
			MaeErrores nuevo = maestroService.buscar_maeError(objError.getIdError());
			
			nuevo.setIdEstrategia(objError.getIdEstrategia());
			nuevo.setDescripcionCorta(objError.getDescripcionCorta());
			nuevo.setDescripcionLarga(objError.getDescripcionLarga());
			nuevo.setSintaxis(objError.getSintaxis());
			nuevo.setCondicion(objError.getCondicion());
			maestroService.editar_maeError(objError);
			System.out.println("editado correctamente");
			System.out.println(objError.getIdEstrategia());
			System.out.println(objError.getIdEstrategia().getIdEstrategia());
		}
		else {
			String id= maestroService.idError(objError.getIdEstrategia().getIdEstrategia());
			System.out.println("id: "+id);
			int id_aumentado=Integer.parseInt(id)+1;
			System.out.println("id_aumentado"+id_aumentado);
			String id_aumentado_string= String.valueOf(id_aumentado);
			if(id_aumentado_string.length()<2) {
				id_aumentado_string="0"+id_aumentado_string;
			}
			String idError=objError.getIdEstrategia().getIdEstrategia()+id_aumentado_string;
			System.out.println("idError:"+idError);
			objError.setIdError(idError);
			maestroService.save_maeError(objError);
			System.out.println("guardado");
		}
		
		return "redirect:/maestro/error";
	}
	
	@GetMapping("/editarError/{id}")
	public String editar_error(@PathVariable(value="id") String id, Model model) {
		model.addAttribute("objError", maestroService.buscar_maeError(id));
		return "views/maestro/error/modal";
	}
	
	@GetMapping("/eliminarError/{id}")
	public String eliminar_error(@PathVariable("id")String id,RedirectAttributes flash) {
		MaeErrores objeto=maestroService.buscar_maeError(id);
		if(objeto!=null) {
			objeto.setEstado(false);
			maestroService.eliminar_maeError(objeto);
			System.out.println("eliminado");
		} 
		else {
			System.out.println("problemas al eliminar");
		}
		return "redirect:/maestro/error";
	}
	
}
