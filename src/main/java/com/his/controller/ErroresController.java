package com.his.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.his.model.MaestroHisUbigeoIneiReniec;
import com.his.model.ResErrores;
import com.his.model.Ubigeo;
import com.his.paginador.PageRender;
import com.his.service.CitaService;
import com.his.service.ErroresService;
import com.his.service.GenericService;
import com.his.service.ProgramaService;
import com.his.service.UbigeoService;


@Controller
@RequestMapping("/errores")
public class ErroresController {
	
	@Autowired
	private ErroresService erroresService;
	
	@Autowired
	private UbigeoService ubigeoService;
	
	@Autowired
	private CitaService citaService;
	
	@Autowired
	private ProgramaService programaService;
	
	@Autowired
	private GenericService genericService;
	
	@GetMapping("/listar")
	public String principal(Model model,@RequestParam(name="page",defaultValue="0") int page) {
		model.addAttribute("titulo","HIS-principal");
		//List<Ubigeo> departamento =ubigeoService.departamento();
		//--List<Ubigeo> disa =ubigeoService.disa();	
		//model.addAttribute("departamento",departamento);
		//--model.addAttribute("disa",disa);
		model.addAttribute("programa",programaService.listar());
		//String url="/errores/listar";
		//Pageable pagerequest=PageRequest.of(page, 5);
		//Page<ResErrores> lista=erroresService.listarTodo(pagerequest);
		//PageRender<ResErrores> pageRender_lista=new PageRender<>(url,lista);
		//model.addAttribute("lista",lista.getContent());
		//model.addAttribute("page", pageRender_lista);
		
		model.addAttribute("microred",ubigeoService.microRed(5, "01"));
		
		
		return "views/c_error/listar";
	}
	
	
	@GetMapping("/resultados")
	public String resultados(Model model,
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(value="establecimiento")int establecimiento,
			@RequestParam(value="anio")String anio,
			@RequestParam(value="mes")String mes,
			@RequestParam(value="idPrograma")int idPrograma,
			@RequestParam(value="tipoRegistrador")int tipoRegistrador,
			@RequestParam(value="estado")Boolean estado) {//estado
		System.out.println(estado);
		
		String url="/errores/resultados";
		url=url+"?establecimiento="+establecimiento+"&anio="+anio+"&mes="+mes+"&idPrograma="+idPrograma+"&tipoRegistrador="+tipoRegistrador+"&estado="+estado+"&page=";
		Pageable pagerequest=PageRequest.of(page, 15);
		Page<ResErrores> lista=erroresService.erroresFiltrados(estado,tipoRegistrador,idPrograma,establecimiento, anio, mes,pagerequest);
		PageRender<ResErrores> pageRender_lista=new PageRender<>(url,lista);
		model.addAttribute("lista",lista.getContent());
		model.addAttribute("page", pageRender_lista);
		model.addAttribute("resultados", "Establecimiento");
		model.addAttribute("visible", "0");
		String nombre_mes= devuelve_mes(mes);
		model.addAttribute("tiempo", ", Mes: "+nombre_mes+""+", Año: "+anio);
		
		/*System.out.println(url);
		System.out.println(lista.getContent().size());*/
		
		return "views/c_error/resultados";
	}

	@GetMapping("/detalle/{id}")
	public String detalle(Model model,@PathVariable("id") String idCita,@RequestParam(value="n")String url) {
		model.addAttribute("detalle",erroresService.listaErrores(idCita));
		model.addAttribute("detalle_cita",citaService.busquedaCita(idCita));
		System.out.println(url);
		if(url.equals("listar")) {
			model.addAttribute("visible",false);
		}
		else {
			model.addAttribute("visible",true);
		}
		
		return "views/c_error/detalle";
	}
	
	@GetMapping("/errorXuser")
	public String errorXUser(Model model) {
		model.addAttribute("programa",programaService.listar());
		return "views/c_error/b_error_x_user";
	}

	@GetMapping("/resultadosUser")
	public String resultadosXuser(Model model,
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(value="dni")String dni,
			@RequestParam(value="anio")String anio,
			@RequestParam(value="mes")String mes,
			@RequestParam(value="idPrograma")int idPrograma,
			@RequestParam(value="estado")Boolean estado,
			@RequestParam(value="tipoRegistrador")int tipoRegistrador) {
		
		String url="/errores/resultadosUser";
		url=url+"?dni="+dni+"&anio="+anio+"&mes="+mes+"&idPrograma="+idPrograma+"&tipoRegistrador="+tipoRegistrador+"&estado="+estado+"&page=";
		Pageable pagerequest=PageRequest.of(page, 15);
		Page<ResErrores> lista=erroresService.erroresFiltradosUser(estado,tipoRegistrador,idPrograma,anio, mes, dni, pagerequest);
		PageRender<ResErrores> pageRender_lista=new PageRender<>(url,lista);
		model.addAttribute("lista",lista.getContent());
		model.addAttribute("page", pageRender_lista);
		/*System.out.println(url);
		System.out.println(lista.getContent().size());*/
		
		model.addAttribute("resultados", "Registrador");
		model.addAttribute("visible", "1");
		model.addAttribute("dato_nombre", genericService.nombres_resultados("registrador", dni));
		String nombre_mes= devuelve_mes(mes);
		model.addAttribute("tiempo", ", Mes: "+nombre_mes+""+", Año: "+anio);
		return "views/c_error/resultados";
	}
	
	
	@GetMapping("/errorXpersonal")
	public String errorXpersonal(Model model) {
		model.addAttribute("programa",programaService.listar());
		return "views/c_error/b_error_x_personal";
	}
	
	
	@GetMapping("/resultadosPersonal")
	public String resultadosXpersonal(Model model,
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(value="dni")String dni,
			@RequestParam(value="anio")String anio,
			@RequestParam(value="mes")String mes,
			@RequestParam(value="estado")Boolean estado,
			@RequestParam(value="idPrograma")int idPrograma) {
		
		String url="/errores/resultadosPersonal";
		url=url+"?dni="+dni+"&anio="+anio+"&mes="+mes+"&idPrograma="+idPrograma+"&estado="+estado+"&page=";
		Pageable pagerequest=PageRequest.of(page, 15);
		Page<ResErrores> lista=erroresService.erroresFiltradosPersonal(estado,idPrograma, anio, mes, dni, pagerequest);
								//erroresService.errores(idPrograma,anio, mes, dni, pagerequest);
		PageRender<ResErrores> pageRender_lista=new PageRender<>(url,lista);
		model.addAttribute("lista",lista.getContent());
		model.addAttribute("page", pageRender_lista);
		/*System.out.println(url);
		System.out.println(lista.getContent().size());*/
		
		model.addAttribute("resultados", "Personal");
		model.addAttribute("visible", "1");
		model.addAttribute("dato_nombre", genericService.nombres_resultados("personal", dni));
		String nombre_mes= devuelve_mes(mes);
		model.addAttribute("tiempo", ", Mes: "+nombre_mes+""+", Año: "+anio);
		return "views/c_error/resultados";
	}
	
	@RequestMapping(value="/actualizar/{id}", method=RequestMethod.POST,  consumes={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Map<String, String> actualizar(@PathVariable("id") Integer idResErrores,
			@RequestBody Map<String,Object> json){
		
		Boolean estado=(Boolean)json.get("estado");
		HashMap<String, String> map = new HashMap<>();
		ResErrores resError= erroresService.buscar(idResErrores);
		if(resError!=null) {
			resError.setEstado(estado);
			int resultado=erroresService.editar(resError);
			if(resultado==1) {
				map.put("1","Se edito correctamente");	
			}
			else {
				map.put("2","Error al editar");	
			}
		}
		else {
			map.put("3","Este id no existe en la BD");	
		}
		return map;
	} 
	
	@GetMapping("/errorXpaciente")
	public String errorXpaciente(Model model) {
		return "views/c_error/b_error_x_paciente";
	}
	
	@GetMapping("/resultadosPaciente")
	public String resultadosXpaciente(Model model,
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(value="dni")String dni,
			@RequestParam(value="anio")String anio,
			@RequestParam(value="mes")String mes,
			@RequestParam(value="estado")Boolean estado) {
		
		String url="/errores/resultadosPaciente";
		url=url+"?dni="+dni+"&anio="+anio+"&mes="+mes+"&estado="+estado+"&page=";
		Pageable pagerequest=PageRequest.of(page, 15);
		Page<ResErrores> lista=erroresService.erroresFiltradosPaciente(estado, anio, mes, dni, pagerequest);
								//erroresService.errores(idPrograma,anio, mes, dni, pagerequest);
		PageRender<ResErrores> pageRender_lista=new PageRender<>(url,lista);
		model.addAttribute("lista",lista.getContent());
		model.addAttribute("page", pageRender_lista);
		/*System.out.println(url);*/
		System.out.println(lista.getContent().size());
		
		model.addAttribute("resultados", "Paciente");
		model.addAttribute("visible", "1");
		model.addAttribute("dato_nombre", genericService.nombres_resultados("paciente", dni));
		String nombre_mes= devuelve_mes(mes);
		model.addAttribute("tiempo", ", Mes: "+nombre_mes+""+", Año: "+anio);
		return "views/c_error/resultados";
	}
	
	public String devuelve_mes(String mes) {
		String nombre="";
		switch(mes) {
		case "1":
			nombre="Enero";
			break;
		case "2":
			nombre="Febrero";
			break;
		
		case "3":
			nombre="Marzo";
			break;
		
		case "4":
			nombre="Abril";
			break;
		
		case "5":
			nombre="Mayo";
			break;
		
		case "6":
			nombre="Junio";
			break;
		
		case "7":
			nombre="Julio";
			break;
		
		case "8":
			nombre="Agosto";
			break;
		
		case "9":
			nombre="Setiembre";
			break;
		
		case "10":
			nombre="Octubre";
			break;
			
		case "11":
			nombre="Noviembre";
			break;
		
		case "12":
			nombre="Diciembre";
			break;
		}

		return nombre;
	} 
}

