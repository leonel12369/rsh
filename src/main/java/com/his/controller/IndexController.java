package com.his.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.his.dao.ProgramaDao;
import com.his.model.Grafico;
import com.his.model.MaeErrores;
import com.his.model.MaestroHisUbigeoIneiReniec;
import com.his.model.MaestroPersonal;
import com.his.model.NominalTramaNuevo;
import com.his.model.ResErrores;
import com.his.model.Ubigeo;
import com.his.paginador.PageRender;
import com.his.service.ErroresService;
import com.his.service.GraficoService;
import com.his.service.UbigeoService;


@Controller
public class IndexController {
	
	/*@Autowired
	private ErroresService erroresService;
	
	@Autowired
	private UbigeoService ubigeoService;*/
	
	@Autowired
	private GraficoService graficoService;
	
	@Autowired
	private ProgramaDao programaDao;
	
	
	@GetMapping({"/general"})
	public String principal(Model model) {
		model.addAttribute("titulo","HIS-principal");
		
		//List<ResErrores> l=erroresService.listaErrores("348672823");
		//System.out.println(l.size());
		/*Pageable pagerequest=PageRequest.of(0, 5);
		Page<ResErrores> lista=erroresService.erroresFiltrados(3596, "2020", "1", pagerequest);
		PageRender<ResErrores> pageRender_lista=new PageRender<>("",lista);
		System.out.println(lista.getContent().size());
		System.out.println(pageRender_lista.getTotalPaginas());
		//model.addAttribute("lista",lista.getContent());
		//model.addAttribute("page", pageRender_lista);*/
		//---------------------------------------------GRAFICO ESPEFICIFICO---------------------------------
		List<Object[]> lista_error_microRed=graficoService.errorMicroRed("0", 0,0, 0);
		
		List<String> ListaStringCantidad=agregarListaResul(0,lista_error_microRed);
		List<String> ListaStringCodMicroRed=agregarListaResul(1,lista_error_microRed);
		List<String> ListaStringNombre=agregarListaResul(2,lista_error_microRed);
		
		model.addAttribute("ListaStringCodMicroRed",ListaStringCodMicroRed);
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringNombre",ListaStringNombre);
		
	
		//--------------------------------------------GRAFICO GENERAL-----------------------------------
		List<Grafico> lista_mes_error=graficoService.mesError();
		List<Grafico> errores_comunes=graficoService.erroresComunes();

		List<String> ListaStringMesError=agregarListaMes(lista_mes_error);
		List<Integer> ListaIntegerMesError=agregarListaCantidad(lista_mes_error);
		List<String> ListaStringErrorComun=agregarListaNombre(errores_comunes);
		List<Integer> ListaIntegerErrorComun=agregarListaCantidad(errores_comunes);
		
		
		model.addAttribute("ListaStringMesError",ListaStringMesError);
		model.addAttribute("ListaIntegerMesError",ListaIntegerMesError);
		model.addAttribute("ListaStringErrorComun",ListaStringErrorComun);
		model.addAttribute("ListaIntegerErrorComun",ListaIntegerErrorComun);
		
		//-----------------------------------------------------select ---------------------
		model.addAttribute("programa",programaDao.listar());
		
		return "views/principal/index";
	}
	
	@GetMapping("/actualizar")
	public String actualizarEstablecimiento(Model model,@RequestParam(name="idMicroRed") String idMicroRed,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") int mes,
			@RequestParam(name="idPrograma") int idPrograma,
			@RequestParam(name="fmes") int fmes) {
		System.out.println("idMicroRed: "+idMicroRed);
		
		model.addAttribute("ListaStringNombreEstablecimiento",graficoService.errorEstablecimiento(idMicroRed,anio,mes,fmes,idPrograma));
		
		return "views/principal/cantidadEstablecimiento";
	}
	
	
	@GetMapping("/graficosMicrored")
	public String especificoMicrored(Model model,@RequestParam(name="idMicroRed") String idMicroRed,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") int mes,
			@RequestParam(name="idPrograma") int idPrograma,
			@RequestParam(name="fmes") int fmes) {
		
		System.out.println("idMicroRed: "+idMicroRed);
		List<Object[]> lista_error_Cmes_microRed=graficoService.cantidadErrorMicrored(idMicroRed,anio,mes,fmes,idPrograma );
		List<Object[]> lista_error_comun_microRed=graficoService.errorComunMicrored(idMicroRed,anio,mes,fmes,idPrograma );
		
		List<String> listaErrorMesMicroRed=agregarListaResultMes(0,lista_error_Cmes_microRed);
		List<String> listaErrorCantidadMicroRed=agregarListaResul(1,lista_error_Cmes_microRed);
		List<String> listaDescrErrorComunMicroRed=agregarListaResul(0,lista_error_comun_microRed);
		List<String> listaCantidadErrorComunMicroRed=agregarListaResul(1,lista_error_comun_microRed);
		
		model.addAttribute("listaErrorMesMicroRed",listaErrorMesMicroRed);
		model.addAttribute("listaErrorCantidadMicroRed",listaErrorCantidadMicroRed);
		model.addAttribute("listaDescrErrorComunMicroRed",listaDescrErrorComunMicroRed);
		model.addAttribute("listaCantidadErrorComunMicroRed",listaCantidadErrorComunMicroRed);
		
		return "views/principal/especificoMicrored";
	}
	
	@GetMapping("/graficosBar")
	public String actualizarGraficosFiltros(Model model,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") int mes,
			@RequestParam(name="idPrograma") int idPrograma,
			@RequestParam(name="fmes") int fmes) {
		System.out.println(anio+" "+mes+" "+idPrograma);
		List<Object[]> lista_error_microRed=graficoService.errorMicroRed(anio,mes,fmes,idPrograma );
		
		List<String> ListaStringCantidad=agregarListaResul(0,lista_error_microRed);
		List<String> ListaStringCodMicroRed=agregarListaResul(1,lista_error_microRed);
		List<String> ListaStringNombre=agregarListaResul(2,lista_error_microRed);
		
		model.addAttribute("ListaStringCodMicroRed",ListaStringCodMicroRed);
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringNombre",ListaStringNombre);
		
		return "views/principal/graficos_microred";
	}
	
	@GetMapping("/creditos")
	public String creditos(){
		return "views/creditos/creditos";
	}
	
	//-------------------------------------metodo para iterar List<object>--------------------------
	private List<String> agregarListaResul(int i,List<Object[]> lista){
		List<String> lista_retorno=new ArrayList<String>();
		for (Object[] a : lista) {
		    //System.out.println(" "+ a[0]+ " "+ a[1]+ " "+ a[2]);
			String variable=a[i]+"";
			lista_retorno.add(variable);
		}
		return lista_retorno;
	}
	
	//-------------------------------------metodo para iterar List<object> en mes--------------------------
		private List<String> agregarListaResultMes(int i,List<Object[]> lista){
			List<String> lista_retorno=new ArrayList<String>();
			for (Object[] a : lista) {
				String variable=a[i]+"";
				switch(variable) {
				case "1":
					lista_retorno.add("Enero");
					break;
				case "2":
					lista_retorno.add("Febrero");
					break;
				case "3":
					lista_retorno.add("Marzo");
					break;
				case "4":
					lista_retorno.add("Abril");
					break;
				case "5":
					lista_retorno.add("Mayo");
					break;
				case "6":
					lista_retorno.add("Junio");
					break;
				case "7":
					lista_retorno.add("Julio");
					break;
				case "8":
					lista_retorno.add("Agosto");
					break;
				case "9":
					lista_retorno.add("Setiembre");
					break;
				case "10":
					lista_retorno.add("Octubre");
					break;
				case "11":
					lista_retorno.add("Noviembre");
					break;
				case "12":
					lista_retorno.add("Diciembre");
					break;
				}
				
			}
			return lista_retorno;
		}
	
	//-------------------------------------metodo para iterar graficos segun mes--------------------------
	private List<String> agregarListaMes(List<Grafico> lista){
		List<String> listaString=new ArrayList<String>();
		for(Grafico i:lista) {
			String x=i.getNombre();
			switch(x) {
			case "1":
				listaString.add("Enero");
				break;
			case "2":
				listaString.add("Febrero");
				break;
			case "3":
				listaString.add("Marzo");
				break;
			case "4":
				listaString.add("Abril");
				break;
			case "5":
				listaString.add("Mayo");
				break;
			case "6":
				listaString.add("Junio");
				break;
			case "7":
				listaString.add("Julio");
				break;
			case "8":
				listaString.add("Agosto");
				break;
			case "9":
				listaString.add("Setiembre");
				break;
			case "10":
				listaString.add("Octubre");
				break;
			case "11":
				listaString.add("Noviembre");
				break;
			case "12":
				listaString.add("Diciembre");
				break;
			}
			//listaString.add(i.getNombre());
		}
		//Collections.reverse(listaString);
		return listaString;
	}
	
	//-------------------------------------metodo para iterar graficos segun cantidad--------------------------
	private List<Integer> agregarListaCantidad(List<Grafico> lista){
		List<Integer> listaCantidad=new ArrayList<Integer>();
		for(Grafico i:lista) {
			listaCantidad.add(i.getCantidad());
		}
		//Collections.reverse(listaCantidad);
		return listaCantidad;
	}
	
	//-------------------------------------metodo para iterar graficos segun general--------------------------
	private List<String> agregarListaNombre(List<Grafico> lista){
		List<String> listaString=new ArrayList<String>();
		for(Grafico i:lista) {
			listaString.add(i.getNombre());
		}
		//Collections.reverse(listaString);
		return listaString;
	}
	
	
	
}

