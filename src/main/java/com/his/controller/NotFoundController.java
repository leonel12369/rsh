package com.his.controller;

import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NotFoundController implements ErrorController{


	@RequestMapping("/error")
    public String render404(HttpServletRequest request,Model model,Map<String, ?> map) {
        // Add model attributes
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		Object trace = map.get("trace");
		if (status != null) {
	        Integer statusCode = Integer.valueOf(status.toString());

	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
	        	 return "views/error/error_404";
	        }
	        else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
	        	model.addAttribute("mensaje",trace);
	            return "views/error/error_500";
	        }
	    }
		Integer statusCode = Integer.valueOf(status.toString());
		model.addAttribute("tipo_error",statusCode);
		model.addAttribute("mensaje",trace);
        return "views/error/otros";
    }

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return null;
	}
}
