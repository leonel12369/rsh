package com.his.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.his.service.GenericService;
import com.his.service.GraficoService;

@Controller
@RequestMapping("/avance")
public class AvanceController {
	
	@Autowired
	private GenericService genericService;
	
	@Autowired
	private GraficoService graficoService;
	
	@GetMapping("/microred")
	public String avance(Model model) {
		model.addAttribute("avance",genericService.avance_microred("2020",1,12));//"2020","1","12" pro defecto
		model.addAttribute("total_microred",genericService.total_microred("2020",1,12));//"2020","1","12"
		//model.addAttribute("no_avance",genericService.no_avance("2020", 1, 12));//"2020","1","12"
		model.addAttribute("no_avance",genericService.no_avance_microred("2020",1,12));
		return "views/avance/microrred";
	}
	
	@GetMapping("/actualizarTabla")
	public String actualizarTablaMicrored(Model model,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") Integer  mes,
			@RequestParam(name="fmes") Integer  fmes) {
		System.out.println(anio+" "+mes+" "+fmes);
		model.addAttribute("no_avance",genericService.no_avance_microred(anio, mes, fmes));
		model.addAttribute("avance",genericService.avance_microred(anio,mes,fmes));//"2020","1","12"
		model.addAttribute("total_microred",genericService.total_microred(anio,mes,fmes));//"2020","1","12"
		//model.addAttribute("no_avance",genericService.no_avance(anio,mes,fmes));//"2020","1","12"
		
		return "views/avance/tabla_microred";
	}
	
	
	@GetMapping("/detalleMicrored/{id}")
	public String Avance_detalle(Model model,@PathVariable("id") String id,
			@RequestParam(name="red") String red,
			@RequestParam(name="anio",required=false) String anio,
			@RequestParam(name="mes",required=false) Integer  mes,
			@RequestParam(name="fmes",required=false) Integer  fmes) {
		
		if(anio==null) {
			anio="2020";
		}
		if(mes==null) {
			mes=1;
		}
		if(fmes==null) {
			fmes=12;
		}
		List<Object[]> lista_avance_microRed=graficoService.avanceGraficoDetalleEstablecimiento(id, anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista_avance_microRed);
		List<String> ListaStringIdEstablecimiento=agregarListaResul(1,lista_avance_microRed);
		List<String> ListaStringEstablecimiento=agregarListaResul(2,lista_avance_microRed);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringIdEstablecimiento",ListaStringIdEstablecimiento);
		model.addAttribute("ListaStringEstablecimiento",ListaStringEstablecimiento);
		
		model.addAttribute("cantidadCorregida",graficoService.cantidadAvance_RestanteMicrored(id,true,anio, mes, fmes));
		model.addAttribute("cantidadRestante",graficoService.cantidadAvance_RestanteMicrored(id,false, anio, mes, fmes));
		
		model.addAttribute("informacionMicrored",genericService.datos_microred(id));
		
		model.addAttribute("detalle_cantidad_estableicmientos",genericService.detalleMicroRed_Establecimientos(id, anio, mes, fmes));
		model.addAttribute("detalle_no_avance_estableicmientos",genericService.avance_est_detMicrocred(id, anio, mes, fmes));
		//System.out.println(genericService.datos_microred(id));
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+mes+", Mes final: "+fmes);
		model.addAttribute("anio",anio);
		model.addAttribute("mesI",mes);
		model.addAttribute("mesF",fmes);
		
		model.addAttribute("anterior",red);
		return "views/avance/detalle_microrred";
	}

	@GetMapping("/graficoDetalleMicrored/{id}")
	public String graficoDetalleMicrored(Model model,@PathVariable("id") String id,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") Integer  mes,
			@RequestParam(name="fmes") Integer  fmes) {
		
		List<Object[]> lista_avance_microRed=graficoService.avanceGraficoDetalleEstablecimiento(id, anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista_avance_microRed);
		List<String> ListaStringIdEstablecimiento=agregarListaResul(1,lista_avance_microRed);
		List<String> ListaStringEstablecimiento=agregarListaResul(2,lista_avance_microRed);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringIdEstablecimiento",ListaStringIdEstablecimiento);
		model.addAttribute("ListaStringEstablecimiento",ListaStringEstablecimiento);
		
		model.addAttribute("cantidadCorregida",graficoService.cantidadAvance_RestanteMicrored(id, true, anio, mes, fmes));
		model.addAttribute("cantidadRestante",graficoService.cantidadAvance_RestanteMicrored(id, false, anio, mes, fmes));
		
		model.addAttribute("informacionMicrored",genericService.datos_microred(id));
		
		model.addAttribute("detalle_cantidad_estableicmientos",genericService.detalleMicroRed_Establecimientos(id, anio, mes, fmes));
		model.addAttribute("detalle_no_avance_estableicmientos",genericService.avance_est_detMicrocred(id, anio, mes, fmes));
		//System.out.println(genericService.datos_microred(id));
		
		String nombre_mes_inicial= devuelve_mes(mes);
		String nombre_mes_final= devuelve_mes(fmes);
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+nombre_mes_inicial+", Mes final: "+nombre_mes_final);
		return "views/avance/detalle_microred_actualizar";
	}
	
	
	
	@GetMapping("/detalleEstablecimiento/{id}")
	public String Avance_detalle_establecimiento(Model model,@PathVariable("id") String id,
			@RequestParam(name="red") String red,
			@RequestParam(name="microred") String microred,
			@RequestParam(name="anioE",required=false) String anio,
			@RequestParam(name="mesE",required=false) Integer mes,
			@RequestParam(name="fmesE",required=false) Integer  fmes) {
			
		System.out.println(anio+" "+mes+" "+fmes);
		
		model.addAttribute("cantidadCorregidaEstablecimiento",graficoService.cantidadAvance_RestanteEstablecimiento(id, true, anio, mes, fmes));
		model.addAttribute("cantidadRestanteEstablecimiento",graficoService.cantidadAvance_RestanteEstablecimiento(id, false, anio, mes, fmes));
		model.addAttribute("avancePersonal",genericService.avance_personal(id, anio, mes, fmes));
		//model.addAttribute("rankin",genericService.ranking(id, true));
		model.addAttribute("informacionEstablecimiento",genericService.datos_Establecimiento(id));
		
		List<Object[]> lista=genericService.ranking(id, true, anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista);
		List<String> ListaStringErrores=agregarListaResul(1,lista);
		//List<String> ListaStringPersonal=agregarListaResul(2,lista);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringErrores",ListaStringErrores);
		//model.addAttribute("ListaStringPersonal",ListaStringPersonal);
		model.addAttribute("noAvancePersonal",genericService.no_avance_personal(id, anio, mes, fmes,0));
		model.addAttribute("terminaron",genericService.no_avance_personal(id, anio, mes, fmes,1));
		
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+mes+", Mes final: "+fmes);
		
		model.addAttribute("anio",anio);
		model.addAttribute("mesI",mes);
		model.addAttribute("mesF",fmes);
		
		model.addAttribute("red",red);
		model.addAttribute("microred",microred);
		return "views/avance/detalleEstablecimientoPrueba";
	}
	
	
	
	@GetMapping("/detalleGraficoEstablecimiento/{id}")
	public String detalle_grafico_establecimiento(Model model,@PathVariable("id") String id,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") Integer  mes,
			@RequestParam(name="fmes") Integer  fmes) {
		
		model.addAttribute("cantidadCorregidaEstablecimiento",graficoService.cantidadAvance_RestanteEstablecimiento(id, true, anio, mes, fmes));
		model.addAttribute("cantidadRestanteEstablecimiento",graficoService.cantidadAvance_RestanteEstablecimiento(id, false, anio, mes, fmes));
		//---model.addAttribute("avancePersonal",genericService.avance_personal(id, anio, mes, fmes));
		//model.addAttribute("rankin",genericService.ranking(id, true));
		model.addAttribute("informacionEstablecimiento",genericService.datos_Establecimiento(id));
		
		List<Object[]> lista=genericService.ranking(id, true, anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista);
		List<String> ListaStringErrores=agregarListaResul(1,lista);
		//List<String> ListaStringPersonal=agregarListaResul(2,lista);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringErrores",ListaStringErrores);
		//model.addAttribute("ListaStringPersonal",ListaStringPersonal);
		
		String nombre_mes_inicial= devuelve_mes(mes);
		String nombre_mes_final= devuelve_mes(fmes);
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+nombre_mes_inicial+", Mes final: "+nombre_mes_final);
		return "views/avance/detalle_establecimiento_actualizar";
	}
	
	@GetMapping("/detalleGraficoEstablecimientoPersonal/{id}")
	public String detalle_grafico_establecimiento_personal(Model model,@PathVariable("id") String id,
			@RequestParam(name="anio") String anio,
			@RequestParam(name="mes") Integer  mes,
			@RequestParam(name="fmes") Integer  fmes) {
		
		
		model.addAttribute("avancePersonal",genericService.avance_personal(id, anio, mes, fmes));
		model.addAttribute("noAvancePersonal",genericService.no_avance_personal(id, anio, mes, fmes,0));
		model.addAttribute("terminaron",genericService.no_avance_personal(id, anio, mes, fmes,1));
		
		return "views/avance/detalle_establecimiento_personal";
	}
	private List<String> agregarListaResul(int i,List<Object[]> lista){
		List<String> lista_retorno=new ArrayList<String>();
		for (Object[] a : lista) {
		    //System.out.println(" "+ a[0]+ " "+ a[1]+ " "+ a[2]);
			String variable=a[i]+"";
			lista_retorno.add(variable);
		}
		return lista_retorno;
	}
	
	
	
	
	
	@GetMapping("/detalleRedes")
	public String Avance_detalle_redes(Model model) {
		
		List<Object[]> lista_avance_Red=graficoService.avanceGraficoDetalleREd("1", "2020", 1, 12);
		List<String> ListaStringCantidad=agregarListaResul(0,lista_avance_Red);
		List<String> ListaStringIdEstablecimiento=agregarListaResul(1,lista_avance_Red);
		List<String> ListaStringEstablecimiento=agregarListaResul(2,lista_avance_Red);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringIdEstablecimiento",ListaStringIdEstablecimiento);
		model.addAttribute("ListaStringEstablecimiento",ListaStringEstablecimiento);
		
		model.addAttribute("cantidadCorregida",graficoService.cantidadAvance_RestanteRed("", true, "2020", 1, 12));
		model.addAttribute("cantidadRestante",graficoService.cantidadAvance_RestanteRed("", false, "2020", 1, 12));
		
		model.addAttribute("informacionRed",genericService.datos_Redes());
		
		model.addAttribute("avanzando",genericService.avance_redes("2020", 1, 12));
		model.addAttribute("no_avance",genericService.no_avance_redes("2020", 1, 12, 0));
		model.addAttribute("terminado",genericService.no_avance_redes("2020", 1, 12, 1));
		
		model.addAttribute("periodo"," Año: 2020, Mes inicial: Enero, Mes final: Diciembre");
		//System.out.println(genericService.datos_microred(id));
		return "views/avance/red/red";//-------------------------------
	}

	@GetMapping("/detalleRedes/actualizar")
	public String Avance_detalle_redes_actualizar(Model model,
			@RequestParam(name="anio",required=false) String anio,
			@RequestParam(name="mes",required=false) Integer  mes,
			@RequestParam(name="fmes",required=false) Integer  fmes) {
		
		List<Object[]> lista_avance_Red=graficoService.avanceGraficoDetalleREd("1", anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista_avance_Red);
		List<String> ListaStringIdEstablecimiento=agregarListaResul(1,lista_avance_Red);
		List<String> ListaStringEstablecimiento=agregarListaResul(2,lista_avance_Red);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringIdEstablecimiento",ListaStringIdEstablecimiento);
		model.addAttribute("ListaStringEstablecimiento",ListaStringEstablecimiento);
		
		model.addAttribute("cantidadCorregida",graficoService.cantidadAvance_RestanteRed("", true, anio, mes, fmes));
		model.addAttribute("cantidadRestante",graficoService.cantidadAvance_RestanteRed("", false, anio, mes, fmes));
		
		model.addAttribute("informacionRed",genericService.datos_Redes());
		
		model.addAttribute("avanzando",genericService.avance_redes(anio, mes, fmes));
		model.addAttribute("no_avance",genericService.no_avance_redes(anio, mes, fmes, 0));
		model.addAttribute("terminado",genericService.no_avance_redes(anio, mes, fmes, 1));
		//System.out.println(genericService.datos_microred(id));
		String nombre_mes_inicial= devuelve_mes(mes);
		String nombre_mes_final= devuelve_mes(fmes);
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+nombre_mes_inicial+", Mes final: "+nombre_mes_final);
		return "views/avance/red/fragment_actualizable";//fragment
	}
	
	
	
	@GetMapping("/detalleMicroredes/{id}")
	public String Avance_detalle_microredes(Model model,@PathVariable("id") String id,
			@RequestParam(name="anio",required=false) String anio,
			@RequestParam(name="mes",required=false) Integer  mes,
			@RequestParam(name="fmes",required=false) Integer  fmes) {
		if(anio==null) {
			anio="2020";
		}
		if(mes==null) {
			mes=1;
		}
		if(fmes==null) {
			fmes=12;
		}
		List<Object[]> lista_avance_microRed=graficoService.avanceGraficoDetalleMicroRed(id, anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista_avance_microRed);
		List<String> ListaStringIdEstablecimiento=agregarListaResul(1,lista_avance_microRed);
		List<String> ListaStringEstablecimiento=agregarListaResul(2,lista_avance_microRed);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringIdEstablecimiento",ListaStringIdEstablecimiento);
		model.addAttribute("ListaStringEstablecimiento",ListaStringEstablecimiento);
		
		model.addAttribute("cantidadCorregida",graficoService.cantidadAvance_RestanteMicrored2(id, true, anio, mes, fmes));
		model.addAttribute("cantidadRestante",graficoService.cantidadAvance_RestanteMicrored2(id, false, anio, mes, fmes));
		
		model.addAttribute("informacionMicrored",genericService.datos_microredes(id));
		
		model.addAttribute("avanzando",genericService.avance_microRedes(id, anio, mes, fmes));
		model.addAttribute("no_avance",genericService.no_avance_microRedes(id, anio, mes, fmes, 0));
		model.addAttribute("terminado",genericService.no_avance_microRedes(id, anio, mes, fmes, 1));
		//System.out.println(genericService.datos_microred(id));
		
		model.addAttribute("anio",anio);
		model.addAttribute("mesI",mes);
		model.addAttribute("mesF",fmes);
		
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+mes+", Mes final: "+fmes);
		return "views/avance/microred/microred";//-----------------------
	}
	
	
	@GetMapping("/detalleMicroredes/actualizar/{id}")
	public String Avance_detalle_microredes_actualizar(Model model,@PathVariable("id") String id,
			@RequestParam(name="anio",required=false) String anio,
			@RequestParam(name="mes",required=false) Integer  mes,
			@RequestParam(name="fmes",required=false) Integer  fmes) {
		List<Object[]> lista_avance_microRed=graficoService.avanceGraficoDetalleMicroRed(id, anio, mes, fmes);
		List<String> ListaStringCantidad=agregarListaResul(0,lista_avance_microRed);
		List<String> ListaStringIdEstablecimiento=agregarListaResul(1,lista_avance_microRed);
		List<String> ListaStringEstablecimiento=agregarListaResul(2,lista_avance_microRed);
		
		model.addAttribute("ListaStringCantidad",ListaStringCantidad);
		model.addAttribute("ListaStringIdEstablecimiento",ListaStringIdEstablecimiento);
		model.addAttribute("ListaStringEstablecimiento",ListaStringEstablecimiento);
		
		model.addAttribute("cantidadCorregida",graficoService.cantidadAvance_RestanteMicrored2(id, true, anio, mes, fmes));
		model.addAttribute("cantidadRestante",graficoService.cantidadAvance_RestanteMicrored2(id, false, anio, mes, fmes));
		
		model.addAttribute("informacionMicrored",genericService.datos_microredes(id));
		
		model.addAttribute("avanzando",genericService.avance_microRedes(id, anio, mes, fmes));
		model.addAttribute("no_avance",genericService.no_avance_microRedes(id, anio, mes, fmes, 0));
		model.addAttribute("terminado",genericService.no_avance_microRedes(id, anio, mes, fmes, 1));
		//System.out.println(genericService.datos_microred(id));
		
		String nombre_mes_inicial= devuelve_mes(mes);
		String nombre_mes_final= devuelve_mes(fmes);
		model.addAttribute("periodo"," Año: "+anio+", Mes inicial: "+nombre_mes_inicial+", Mes final: "+nombre_mes_final);
		return "views/avance/microred/fragment_actualizable";//fragment
	}
	
	
	public String devuelve_mes(int mes) {
		String nombre="";
		switch(mes) {
		case 1:
			nombre="Enero";
			break;
		case 2:
			nombre="Febrero";
			break;
		
		case 3:
			nombre="Marzo";
			break;
		
		case 4:
			nombre="Abril";
			break;
		
		case 5:
			nombre="Mayo";
			break;
		
		case 6:
			nombre="Junio";
			break;
		
		case 7:
			nombre="Julio";
			break;
		
		case 8:
			nombre="Agosto";
			break;
		
		case 9:
			nombre="Setiembre";
			break;
		
		case 10:
			nombre="Octubre";
			break;
			
		case 11:
			nombre="Noviembre";
			break;
		
		case 12:
			nombre="Diciembre";
			break;
		}

		return nombre;
	} 
	
}
