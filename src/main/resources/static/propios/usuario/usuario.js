/**
 * 
 */


function rol_admin(){
	var check1 = document.getElementById("rol1");
	 if (check1.checked == true) {
		 var check1 = $("#rol1").val(true);
		 console.log(document.getElementById("rol1").value);
    } else {
   	 var check1 = $("#rol1").val(false);
   		console.log(document.getElementById("rol1").value);
    }
};

function rol_user(){
	var check2 = document.getElementById("rol2");
	 if (check2.checked == true) {
		 var check1 = $("#rol2").val(true);
		 console.log(document.getElementById("rol2").value);
   } else {
  	 var check1 = $("#rol2").val(false);
  		console.log(document.getElementById("rol2").value);
   }
};

function guardarUsuario(){
	//console.log("sdf");
	var nombre= document.getElementById("nombre").value;
	var apellidoPaterno=document.getElementById("apellidoPaterno").value;
	var apellidoMaterno=document.getElementById("apellidoMaterno").value;
	var nombreUsuario=document.getElementById("nombreUsuario").value;
	var label = document.getElementById('labelNombreUsuario'); 
	var contrasenia=document.getElementById("contrasenia").value;
	var check1 = document.getElementById("rol1").value;
	var check2 = document.getElementById("rol2").value;
	
	if(nombre==""){
		alert("El campo nombre esta vacio"); 
    	return false;
	};
	if(apellidoPaterno==""){
		alert("El campo apellido Paterno esta vacio"); 
    	return false;
	};
	if(apellidoMaterno==""){
		alert("El campo apellido Materno esta vacio"); 
    	return false;
	};
	if(nombreUsuario==""){
		alert("El campo nombre Usuario esta vacio"); 
    	return false;
	};
	if(nombreUsuario.length<4){
		alert("El campo nombre Usuario debe tener mas de 3 letras"); 
    	return false;
	};
	if(contrasenia==""){
		alert("El campo contrasenia esta vacio"); 
    	return false;
	};
	
	if(check1=="false" && check2=="false" ){
		alert("Al menos el usuario debe tener algun rol"); 
    	return false;
	}
	console.log("---")
 	if(label.style.color==='red'){
 		alert("Debe cambiar el usuario"); 
 		return false;
 	}
	/////////////////////////////////
	console.log("---")
	var listRol=[];
	if(check1=="true" && check2=="true" ){
		listRol.push("ROLE_ADMIN");
		listRol.push("ROLE_USER");
	}
	else{
		if(check1=="true"){
			listRol.push("ROLE_ADMIN");
		}
		else{
			listRol.push("ROLE_USER");
		}	
	}
	var token = $("input[name='_csrf']").val();
	console.log(token);
	var header = "X-CSRF-TOKEN";
	console.log(header)
	var jsonUsuario={
			nombre:nombre,
			apellidoPaterno:apellidoPaterno,
			apellidoMaterno:apellidoMaterno,
			nombreUsuario:nombreUsuario,
			contrasenia:contrasenia,
			listRol:listRol
	}
	
	$.ajax({
		contentType: "application/json",
		url:"/usuario/guardar",
		beforeSend: function(xhr) {
            xhr.setRequestHeader(header, token)
          },
		datatype:"application/json",
		type:'POST',
		data:JSON.stringify(jsonUsuario),
		dataType:'json',
		success:function(data){
			var mensaje = document.getElementById('mensaje_success');  
			mensaje.style.display = 'block';
			setTimeout(function(){
				mensaje.style.display = 'none';
				window.location.href="/usuario/listar/"
				}, 2000);
			console.log("success");
			
		},
		error:function(){
			console.log("error");
		}
	})
}